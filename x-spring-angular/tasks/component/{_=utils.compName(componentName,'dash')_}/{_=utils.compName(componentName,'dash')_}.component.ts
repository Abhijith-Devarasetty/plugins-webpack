import { Component,  OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-<%=utils.compName(componentName,"dash")%>',
  templateUrl: './<%=utils.compName(componentName,"dash")%>.component.html',
  styleUrls: ['./<%=utils.compName(componentName,"dash")%>.component.css'],
})

export class <%=componentName%>Component implements OnInit {
<%tabLayoutData.forEach(t => {-%>
  <%=utils.pascalCase(t.tabName)%>: any;
<%})-%>

  constructor( private formBuilder: FormBuilder ) {
<%let len%>
<%tabLayoutData.forEach(t => {-%>
  this.<%=utils.pascalCase(t.tabName)%> = this.formBuilder.group({
<%t.sections.forEach(s => {-%>
    <%=utils.pascalCase(s.sectionName)%> : this.formBuilder.group({
<%len = s.elements.size-%>
<%s.elements.forEach(e => {-%>
<%len = len - 1;-%>
<%if(e.type === "Checkbox" && e.options !== undefined){-%>
<%e.options.forEach(o =>{-%>
      <%=o%>: [false],
<%})-%>
<%}else{-%>
      <%=e.name%>: ['' <%=utils.getValidators(e)%>]<%if(len!==0){%>,<%}%>
<%}-%>
<%})-%>
    }),
<%})-%>
  });
<%})-%>
  }

  ngOnInit() { }

<%-utils.getActions(tabLayoutData)%>

}
