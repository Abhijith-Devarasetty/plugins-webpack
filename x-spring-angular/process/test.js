const _ = require("lodash");

_.mixin({ 'pascalCase': _.flow(_.camelCase, _.upperFirst) });
  console.log(_.pascalCase('abhi Friend Test'));
  console.log(_.pascalCase('abhiFriend ').length);