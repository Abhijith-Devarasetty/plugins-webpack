const { processData } = require("./processdata.js");
let jsonata = require("jsonata");
const {processYaml} = require("./swaggerParser.js");

module.exports.run = (inputData, lookup, tasks , utils ) => {
  
  let temp = {
    applications:[
      inputData
    ]
  }

  console.log("input data",JSON.stringify(inputData));
  let processedData = processData(temp,utils.getRef);
  console.log("processed data",JSON.stringify(processedData));


  tasks["entity"] = [];
  processedData.project.resources.entities.forEach(e => {
    let temp = processedData.project.entities.filter(f =>  f.name === e.entity )[0];
    // console.log("!!!!!!!!!!!!!!!",temp);
    temp["path"] = e.path;
    temp.taskId = "entity";
    tasks["entity"].push(temp);
  });
  // console.log("$$$$$$$$$$$$",tasks["entity"] );


  tasks["component"] = [];
  processedData.project.resources.components.forEach(e => {
    let temp = processedData.project.components.filter(f =>  f.name === e.entity )[0];
    // console.log("!!!!!!!!!!!!!!!",temp);
    temp["path"] = e.path;
    temp.taskId = "component";
    tasks["component"].push(temp);
  });
  // console.log("$$$$$$$$$$$$",tasks["component"] );

  console.log("tasks model", tasks);
  return { tasks, data: processedData};
};
