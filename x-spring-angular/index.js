const _ = require("lodash");
const path = require("path");
let jsonata = require("jsonata");
const { isEmpty } = require("lodash");
// const test = require("./utils.js");
// const {getRestCall} = require("./utils.js");

_.mixin({ pascalcase: _.flow(_.camelCase, _.upperFirst) });

function pascalCase(input) {
  return _.pascalcase(input);
}

function getDirectory(data) {
  let p = data.project.application.package.replace(/\./g, "//");
  return path.normalize(p);
}

function getPackageName(data) {
  return data.project.application.package;
}

function getEntityName(data) {
  return data.project.entities[0].name;
}

function getCapEntityName(data) {
  let entityName = data.project.entities[0].name;
  return pascalCase(entityName);
}

function repoPrimary(schema, entity) {
  if (jsonata("$count(*[primaryKey ='true'])>1").evaluate(schema)) {
    return pascalCase(entity) + "Id";
  }
  if (jsonata("$count(*[primaryKey ='true'])<1").evaluate(schema)) {
    return "Long";
  }
  for (let i = 0; i < schema.length; i++) {
    if (schema[i].primaryKey === "true") {
      switch (getDataType(schema[i])) {
        case "int":
          return "Integer";
        case "long":
          return "Long";
        case "Long":
          return "Long";
        default:
          return "Long";
      }
    }
  }
}

function getImports(attributes) {
  let set = new Set();
  let result = "";
  attributes.forEach((ele) => {
    if (!_.isEmpty(ele.actualDataType)) {
      set.add("import " + ele.actualDataType + ";");
    }
    if (ele.dataType == "clob") {
      set.add("import java.sql.Clob;");
    }
  });
  for (let temp of set) {
    result =
      result +
      temp +
      `
`;
  }
  return result;
}

function getPrimaryDataType(attributes) {
  for (let i = 0; i < attributes.length; i++) {
    if (attributes[i].primaryKey === "true") {
      return getDataType(attributes[i]);
    }
  }
}

function primaryKey(schema) {
  for (let i = 0; i < schema.length; i++) {
    if (schema[i].meta.pk === "true") {
      switch (getDataType(schema[i])) {
        case "int":
          return "Integer";
        case "long":
          return "Long";
        case "Long":
          return "Long";
        default:
          return "Long";
      }
    }
  }
}

function getDataType(ele) {
  switch (ele.dataType) {
    case "int":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "int";
      }
    case "integer":
      return "int";
    case "decimal":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "float";
      }
    case "string":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "String";
      }
    case "boolean":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "boolean";
      }
    case "date":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "Date";
      }
    case "time":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "Timestamp";
      }
    case "clob":
      return "Clob";
    case "blob":
      return "byte[]";
    default:
      return ele.dataType;
  }
}

function getPrimaryDatType(schema) {
  schema.forEach((ele) => {
    if (ele.meta.pk == "true") {
      switch (ele.dataType) {
        case "int":
          return "Integer";
      }
    }
  });
}

function getRepository(data) {
  switch (data.project.application.database.dbName) {
    case "mongo":
      return "MongoRepository";
    default:
      return "JpaRepository";
  }
}

function checkEureka(data) {
  if (data.project.application.discovery != undefined) {
    switch (Object.keys(data.project.application.discovery)[0]) {
      case "eureka":
        return `
        <dependency>
          <groupId>org.springframework.cloud</groupId>
          <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>`;
    }
  }
}

function checkJpa(data) {
  if (data.project.application.database.dbName != "mongo") {
    return `
  <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
  </dependency>`;
  }
}

function getDataBase(data) {
  switch (data.project.application.database.dbName) {
    case "h2":
      return `
      <dependency>
			    <groupId>com.h2database</groupId>
			    <artifactId>h2</artifactId>
			    <scope>runtime</scope>
		  </dependency>`;

    case "mysql":
      return `
      <dependency>
			    <groupId>mysql</groupId>
			    <artifactId>mysql-connector-java</artifactId>
			    <scope>runtime</scope>
      </dependency>`;

    case "mongo":
      return `
      <dependency>
			    <groupId>org.springframework.boot</groupId>
			    <artifactId>spring-boot-starter-data-mongodb</artifactId>
      </dependency>`;

    case "postgresql":
      return `
      <dependency>
			    <groupId>org.postgresql</groupId>
			    <artifactId>postgresql</artifactId>
			    <scope>runtime</scope>
		  </dependency>`;
  }
}

function getDataBaseProperties(data) {
  let path = data.project.application.database.url;
  switch (data.project.application.database.dbName) {
    case "h2":
      return `
spring.h2.console.enabled=true
spring.database.platform=h2
spring.database.url=${path}`;

    case "mysql":
      return `
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.url=${path}
spring.datasource.username= 
spring.datasource.password=
spring.jpa.show-sql=true
spring.jpa.hibernate.ddl-auto=update
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5Dialect`;

    case "mongo":
      return `spring.data.mongo.uri=${path}`;

    case "postgresql":
      return `
spring.jpa.database=POSTGRESQL
spring.datasource.platform=postgres
spring.datasource.url=${path}
spring.datasource.username= 
spring.datasource.password= 
spring.jpa.show-sql=true
spring.jpa.generate-ddl=true
spring.jpa.hibernate.ddl-auto=update
spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true`;
  }
}

function checkTracing(data) {
  if (data.project.application.tracing !== undefined) {
    switch (Object.keys(data.project.application.tracing)[0]) {
      case "zipkin":
        return `
    <dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-sleuth</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-zipkin</artifactId>
		</dependency>`;
    }
  }
}

function getTracing(data) {
  let result = "";
  if (data.project.application.tracing != undefined) {
    let trace = Object.keys(data.project.application.tracing);
    switch (trace[0]) {
      case "zipkin":
        result = `    
    @Bean
    public Sampler defaultSampler() {
      return Sampler.ALWAYS_SAMPLE;
    }`;
        break;

      default:
        break;
    }
  }
  return result;
}

function getRestCall(act) {
  let http = new RegExp(/(http|grpc)/g, "g");
  let met = new RegExp(/(GET|POST|PUT|DELETE)/g, "g");
  let str = new RegExp(/:(\/\S*)/g, "g");
  let protocol = _.capitalize(_.lowerCase(act.method.match(met)[0]));
  return {
    http: act.method.match(http)[0],
    met: protocol,
    str: act.method.match(str)[0].slice(1),
  };
}

function actionName(act) {
  if (act.method_name !== undefined) {
    return act.method_name;
  } else {
    let str = new RegExp(/:(\/\S*)/g, "g");
    let methLen = str.length;
    return act.method.match(str)[0].slice(1).substring(1, methLen);
  }
}

function getRequestBody(action) {
  req = action.request;
  if (req !== undefined) {
    // console.log("type of",typeof(req));
    if (typeof req === "object") {
      return "@RequestBody " + _.capitalize(req.entity) + " " + req.entity;
    } else {
      if (req != "") {
        return "@RequestBody " + pascalCase(req) + " " + _.toLower(req);
      } else {
        return "";
      }
    }
  } else {
    return "";
  }
}

function getReqParams(action) {
  if (action.req_params !== undefined) {
    params1 = action.req_params;
    str = "";
    params1.forEach((p) => {
      str =
        str +
        "@RequestParam " +
        getActionDataType(p.split(":")[1].trim()) +
        " " +
        p.split(":")[0];
      // console.log("in for each of reqParam",str);
    });
    return str;
  } else {
    return "";
  }
}

function getPathParams(action) {
  if (action.path_params !== undefined) {
    params = action.path_params;
    str = "";
    params.forEach((p) => {
      str =
        str +
        "@PathVariable " +
        getActionDataType(p.split(":")[1].trim()) +
        " " +
        p.split(":")[0];
      // console.log("path",p.split(":")[1]);
    });
    return str;
  } else {
    return "";
  }
}

function getFinalParams(action) {
  let str = (getReqParams(action) + getPathParams(action)).split("@");
  str = getRequestBody(action) + str.join(" , @");
  if (str.substr(0, 2) == " ,") {
    str = str.substring(2);
  }
  return str;
}

function getActionDataType(type) {
  switch (type) {
    case "int":
      return "int";
    case "integer":
      return "int";
    case "string":
      return "String";
    default:
      return type;
  }
}

function composite(attributes) {
  if (jsonata("$count(*[primaryKey ='true'])>1").evaluate(attributes)) {
    return "composite";
  } else {
    return "";
  }
}

function compositeKeys(attributes) {
  let arr = [];
  attributes.forEach((e) => {
    if (e.primaryKey === "true") {
      arr.push(e);
    }
  });
  return arr;
}

function getSerializ(schema) {
  if (jsonata("$count(*[primaryKey ='true'])>1").evaluate(schema)) {
    return "implements Serializable";
  } else {
    return "";
  }
}

function getRelation(data, entityName) {
  rel = jsonata(`project.entities[name = "${entityName}"].relations`).evaluate(
    data
  );
  if (!isEmpty(rel)) {
    let result = "";
    rel.forEach((r) => {
      let relData = {
        rhsEntity: r.rhs.split("|")[0].substring(5),
        rhsAttr: r.rhs.split("|")[1].substring(6),
        lhsEntity: r.lhs.split("|")[0].substring(5),
        lhsAttr: r.lhs.split("|")[1].substring(6),
      };
      if (r.relationship === "OneToMany") {
        if (relData.lhsEntity === entityName) {
          result =
            result +
            `
	  @OneToMany(mappedBy="${entityName}")
    private List<${pascalCase(relData.rhsEntity)}> ${relData.rhsEntity};
`;
        }
        if (relData.rhsEntity === entityName) {
          result =
            result +
            `
    @ManyToOne
    @JoinColumn(name="${getJoinColumn(r, relData)}", nullable=false)
    private ${pascalCase(relData.lhsEntity)} ${relData.lhsEntity};
`;
        }
      } else if (r.relationship === "ManyToMany") {
        if (relData.lhsEntity === entityName) {
          result =
            result +
            `
    @ManyToMany
    @JoinTable(name="${getMergeTable(
      r
    )}",joinColumns=@JoinColumn(name="${getJoinColumn(r, relData)}"),
    inverseJoinColumns=@JoinColumn(name="${getInverseJoinColumns(r, relData)}"))
    private List<${pascalCase(relData.rhsEntity)}> ${
              relData.rhsEntity
            }s=new ArrayList<>();
`;
        }
        if (relData.rhsEntity === entityName) {
          result =
            result +
            `
    @ManyToMany(mappedBy="${relData.rhsEntity}s")
    private List<${pascalCase(relData.lhsEntity)}> ${
              relData.lhsEntity
            }=new ArrayList<>();
`;
        }
      } else if (r.relationship === "OneToOne") {
        if (relData.lhsEntity === entityName) {
          if (r.meta.targetEntity === undefined) {
            result =
              result +
              `
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "${getJoinColumn(
      r,
      relData
    )}", referencedColumnName = "${relData.rhsAttr}")
    private ${pascalCase(relData.rhsEntity)} ${relData.rhsEntity};
`;
          } else {
            if (r.meta.targetEntity === entityName) {
              result =
                result +
                `
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "${getJoinColumn(
      r,
      relData
    )}", referencedColumnName = "${relData.rhsAttr}")
    private ${pascalCase(relData.rhsEntity)} ${relData.rhsEntity};
`;
            } else {
              result =
                result +
                `
    @OneToOne(mappedBy = "${relData.lhsEntity}")
    private ${pascalCase(relData.rhsEntity)} ${relData.rhsEntity};
`;
            }
          }
        }
        if (relData.rhsEntity === entityName) {
          if (r.meta.targetEntity === undefined) {
            result =
              result +
              `
    @OneToOne(mappedBy = "${relData.rhsEntity}")
    private ${pascalCase(relData.lhsEntity)} ${relData.lhsEntity};
`;
          } else {
            if (r.meta.targetClass === entityName) {
              result =
                result +
                `
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "${getJoinColumn(
      r,
      relData
    )}", referencedColumnName = "${relData.lhsAttr}")
    private ${pascalCase(relData.lhsEntity)} ${relData.lhsEntity};
`;
            } else {
              result =
                result +
                `
    @OneToOne(mappedBy = "${relData.rhsEntity}")
    private ${pascalCase(relData.lhsEntity)} ${relData.lhsEntity};
`;
            }
          }
        }
      }
    });
    return result;
  }
}

function getJoinColumn(rel, relData) {
  let joinColumn;
  if (!_.isEmpty(rel.meta) && rel.meta.joinColumn != undefined) {
    joinColumn = rel.meta.joinColumn;
  } else {
    if (rel.relationship == "OneToMany") {
      joinColumn = relData.lhsEntity + "_fk";
    } else if (rel.relationship == "ManyToMany") {
      joinColumn = relData.lhsAttr;
    } else if (rel.relationship == "OneToOne") {
      joinColumn = "fk";
    }
  }
  return joinColumn;
}

function getInverseJoinColumns(rel, relData) {
  let inverseJoinColumn;
  if (!_.isEmpty(rel.meta) && rel.meta.inverseJoinColumn != undefined) {
    inverseJoinColumn = rel.meta.inverseJoinColumns;
  } else {
    inverseJoinColumn = relData.rhsAttr;
  }
  return inverseJoinColumn;
}

function getMergeTable(rel) {
  let mergeTable;
  if (!_.isEmpty(rel.meta) && rel.meta.mergeTable != undefined) {
    mergeTable = rel.meta.mergeTable;
  } else {
    mergeTable = "merge_table";
  }
  return mergeTable;
}

function checkKogitoDepen(data) {
  if (
    (jsonata("**.bpmn").evaluate(data) !== undefined) |
    (jsonata("**.dmn").evaluate(data) !== undefined)
  ) {
    return `
    <dependency>
      <groupId>org.kie.kogito</groupId>
      <artifactId>kogito-springboot-starter</artifactId>
    </dependency>
    <dependency>
      <groupId>io.rest-assured</groupId>
      <artifactId>rest-assured</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.kie.kogito</groupId>
      <artifactId>kogito-scenario-simulation</artifactId>
      <scope>test</scope>
    </dependency>
    `;
  }
}

function checkKogitoDepenMang(data) {
  console.log("kogito", jsonata("**.bpmn").evaluate(data));
  if (
    (jsonata("**.bpmn").evaluate(data) !== undefined) |
    (jsonata("**.dmn").evaluate(data) !== undefined)
  ) {
    return `
      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-dependencies</artifactId>
        <version>\${springboot.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      <dependency>
        <groupId>org.kie.kogito</groupId>
        <artifactId>kogito-bom</artifactId>
        <version>\${project.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    `;
  }
}

function getResponseEntity(a,getRef) {
  if (a.response !== "") {
    if(a.response.includes("array")){
      let str = a.response.substring(8,a.response.length-1);
      return `List<${pascalCase(str)}>`;
    }
    a.response = "$ref:" + a.response + "|";
    let obj = getRef(a.response);
    if (obj === undefined) {
      return a.response;
    }
    return pascalCase(obj.entity);
  } else {
    return "void";
  }
}

function CheckAggregateDto(a, getRef) {
  if (a.response !== "") {
    a.response = "$ref:" + a.response + "|";
    let obj;
    try{
      obj = getRef(a.response);
    }catch(e){
      obj = 'asd';
    }
    if (obj === undefined) {
      return `// Business Logic Here ..... `;
    }
    if (obj.taskId === "aggregatedto") {
      return `return ${pascalCase(obj.entity)}Service.get${pascalCase(
        obj.entity
      )}();`;
    } else {
      return `// Business Logic Here ..... `;
    }
  } else {
    return `// Business Logic Here ..... `;
  }
}

function getAttributesData(attributes,entity){
  let str='';
  attributes.forEach(a => {
    str = `${str}${entity}.set${pascalCase(a.name)}(${getElevalue(a.dataType)});
		`;
  });
  return str;
}

function getElevalue(dataType){
  switch(dataType){
    case 'int': return "1";
    case 'string': return '"asd"';
    default: return '"test"';
  }
}

function getUiElement(material,ele,layout){
  if(material === "material"){
    return getMaterialUiElement(ele,layout);
  }
  if(material === "primeNg"){
    return getPrimeNgUiElement(ele,layout);
  }
}


function getMaterialUiElement(ele,layout){
  let grid = 12/layout;
  let options = '';
  let required = '';
  if(ele.validators !== undefined && ele.validators.includes('required')){
    required = `<span class="required"> *</span>`;
  }
  switch(ele.type){
    case 'Input': return `
        <div class="col-md-${grid} element">
          <mat-label id ="${ele.name}">${ele.label}</mat-label>${required}<br />
          <input class="input" formControlName="${ele.name}" matInput />
        </div>`;

    case 'Datepicker': return `
        <div class="col-md-${grid} element">
          <mat-label id ="${ele.name}">${ele.label}</mat-label>${required}<br />
            <input class="input" formControlName="${ele.name}" matInput [matDatepicker]="picker">
            <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
          <mat-datepicker #picker></mat-datepicker>
        </div>`;
    
    case 'Radio':
      options ='';
      if(ele.options !== undefined){
        ele.options.forEach(o => {
          options = options + `
            <mat-radio-button value="${o}">${o}</mat-radio-button>`;
        });
      }
      return `
        <div class="col-md-${grid} element">
          <mat-label id ="${ele.name}">${ele.label}</mat-label>${required}<br />
          <mat-radio-group formControlName="${ele.name}" aria-label="${ele.label}">`+ options +
          `
          </mat-radio-group>
        </div>`;

    case 'Select':
      options = '';
      if(ele.options !== undefined){
        ele.options.forEach(o => {
          options = options + `
              <mat-option value="${o}">${o}</mat-option>`
        });
      }
      return `
        <div class="col-md-${grid} element">
          <mat-label id ="${ele.name}">${ele.label}</mat-label>${required}<br />
          <mat-select class="select" formControlName="${ele.name}" matNativeControl required>`
          + options +
          `
          </mat-select>
        </div>`;

    case 'Checkbox':
      options = '';
      if(ele.options !== undefined){
        ele.options.forEach(o => {
          options = options + `
              <mat-checkbox formControlName="${o}" class="checkbox-option">${o}</mat-checkbox>`;
        })
      }
      return `
        <div class="col-md-${grid} element">
          <mat-label id ="${ele.name}">${ele.label}</mat-label>${required}<br />
          <section class="checkbox">`
          + options +
          `
          </section>
        </div>`;

    case 'Button':
      let action ='';
      if(ele.option_action !== undefined){
        action = `(click)=${ele.option_action}()`;
      }
      return `
        <div class="col-md-${grid} element">
          <button id ="${ele.name}" mat-raised-button ${action} color="primary">${ele.label}</button><br/>
        </div>
        `;
  }
}

function getActions(tabLayoutData){
  let actions = '';
  tabLayoutData.forEach(t => {
    t.sections.forEach(s => {
      s.elements.forEach(e => {
        if(e.type === "Button"){
          if(e.option_action!==undefined){
            actions = actions + `   ${e.option_action}(){
    // Logic here
  }
`;
          }
        }
      })
    })
  })
  return actions;
}

function getSelectedEle(eleType,tabLayoutData){
  var arr = [];
  tabLayoutData.forEach(t=>{
    let temp = [];
    t.sections.forEach(s=>{
      arr = [...s.elements.filter(e => e.type === eleType && e.option_action !== undefined),...arr];
    });
  })
  // console.log(arr);
  return arr;
}

function getValidators(e){
  if(e.validators === undefined){
    return;
  }
  let str ='';
  e.validators.forEach(v => {
    if(v === 'required'){
      str += ' Validators.required';
    } else if(v.includes('minLength')){
      str += ` Validators.${v}`;
    } else if(v.includes('maxLength')){
      str += ` Validators.${v}`;
    } else if(v === 'email'){
      str += ` Validators.email`;
    }
  });
  str = str.split(" ").join(", ");
  str = str.substring(2,str.lenth);
  str = `,[${str}]`;
  return str;
}

function compName(name,flag){
  if(flag === "dash"){
    //return _.camelCase(name.replace(/[A-Z]/g, '-$&').toLowerCase());
    return _.camelCase(name).replace(/[A-Z]/g, '-$&').toLowerCase();
  }else if(flag === "lower"){
    return _.lowerCase(name);
  }else if (flag === "upper"){
    return _.upperCase(name);
  }
}

module.exports = {
  getDirectory,
  getTracing,
  getEntityName,
  getCapEntityName,
  checkEureka,
  getPackageName,
  getDataBase,
  getDataBaseProperties,
  checkTracing,
  getPrimaryDatType,
  getJoinColumn,
  getInverseJoinColumns,
  getMergeTable,
  checkJpa,
  getRepository,
  getDataType,
  getImports,
  getRestCall,
  getPrimaryDataType,
  primaryKey,
  repoPrimary,
  composite,
  compositeKeys,
  getSerializ,
  getFinalParams,
  actionName,
  pascalCase,
  getRelation,
  checkKogitoDepen,
  checkKogitoDepenMang,
  getResponseEntity,
  CheckAggregateDto,
  getAttributesData,
  getUiElement,
  getActions,
  getSelectedEle,
  getValidators,
  compName
};
