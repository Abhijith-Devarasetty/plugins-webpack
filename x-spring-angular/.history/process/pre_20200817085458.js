// module.exports.run = (inputData, lookup, tasks, utils) => {
//   //transform your data
//   //and return Object with tasks and ...params
//   return { tasks };
// };
const { generateFiles } = require("./processdata.js");

let jsonata = require("jsonata");
module.exports.run = (inputData, lookup, tasks) => {
  //let tasksFile = JSON.parse(fs.readFileSync('tasksFile.json', { encoding: "utf8" }));
  console.log("from pre processor" + JSON.stringify(inputData));
  console.log("from pre processor" + JSON.stringify(lookup));
  console.log("from pre processor" + JSON.stringify(tasks));
  let tasksData = jsonata("${**.name : $.[**.name, **.depends]}").evaluate(
    tasks
  );
  let entities = jsonata("**.entities{**.taskId: [ **.entity ]}").evaluate(
    lookup
  );
  let processedTasks = {};
  for (const [key, value] of Object.entries(entities)) {

    let processedTasksT = tasksData[key].map(function (eachEntity) {
      console.log(eachEntity);
      console.log(value);
      console.log(processedTasks[eachEntity]);
      if (processedTasks[eachEntity])
        processedTasks[eachEntity] = processedTasks[eachEntity].concat(value);
      else processedTasks[eachEntity] = value;
    });
  }
  console.log(JSON.stringify(processedTasks));
  tasks = processedTasks;


  //Process Data 
  
function jsonConversion(input) {
  let output = {
    projects: {
      application: processApplications(input.applications),
      entities: processEntities(input),
      components : processComponents(input.applications)
    },
  };
  return output;
}

function processApplications(applications) {
  let application = [];
  applications.forEach((app) => {
    let appName = jsonata("$.appName").evaluate(app);
    let value = {
      name: appName,
      templateId: app.templateId,
      framework: jsonata("$.framework").evaluate(app),
      package: jsonata("**.package_name").evaluate(app),
      port: jsonata("**.port").evaluate(app),
      database: {
        url: jsonata("**.database.**.url").evaluate(app),
        dbName: "",
        username: "",
        password: "",
      },
      tracing: jsonata("**.tracing").evaluate(app),
      logging: jsonata("**.logging").evaluate(app),
      discovery: jsonata("**.discovery").evaluate(app),
      entities: jsonata("$.**.entities.entity").evaluate(app),
      components: []
    };
    application.push({ [appName]: value });
  });

  return application;
}

function processEntities(input) {
  let temp = jsonata("**.entities[entity = 'resturant']").evaluate(input);
  let entity = {
    name: temp.entity,
    isCompositeKey: jsonata("$count(schema.meta[pk ='true'])>1").evaluate(temp),
    attributes: processAttributes(temp.schema),
  };
  return entity;
}

function processFields(comp) {
    let arr =[];
    comp = comp.componentStructure;
    comp.forEach(field =>{
        arr.push({
            "element":field.input_type,
            "label":field.label,
            "name":field.name
        });
    })
    return arr;
}

function processComponents(applications){
    let components = jsonata("**.components").evaluate(applications);
    console.log(components,applications);
    let arr = [];
    components.forEach(comp =>{
        let appName = jsonata("$.componentName").evaluate(comp);
        let value = {
            "name":appName,
            "fields":processFields(comp)
        }
        arr.push({ [appName]:value });
    })
    return arr;
}

function processEnums(input) {}

function processResources(input) {}

function processAttributes(schema) {
  let result = [];
  schema.forEach((ele) => {
    result.push({
      name: ele.attribute,
      dataType: ele.dataType,
      columnName:
        ele.meta.column != undefined ? ele.meta.column : ele.attribute,
      primaryKey: ele.meta.pk ? "true" : "fasle",
    });
  });
  return result;
}
  tasks = processedTasks;
  inputData = jsonConversion(inputData);

  return { tasks, inputData };
};


