// module.exports.run = (inputData, lookup, tasks, utils) => {
//   //transform your data
//   //and return Object with tasks and ...params
//   return { tasks };
// };

let jsonata = require("jsonata");
module.exports.run = (inputData, lookup, tasks) => {
  //let tasksFile = JSON.parse(fs.readFileSync('tasksFile.json', { encoding: "utf8" }));
  console.log("from pre processor" + JSON.stringify(inputData));
  console.log("from pre processor" + JSON.stringify(lookup));
  console.log("from pre processor" + JSON.stringify(tasks));
  let tasksData = jsonata("${**.name : $.[**.name, **.depends]}").evaluate(
    tasks
  );
  let entities = jsonata("**.entities{**.taskId: [ **.entity ]}").evaluate(
    lookup
  );
  let processedTasks = {};
  for (const [key, value] of Object.entries(entities)) {

    let processedTasksT = tasksData[key].map(function (eachEntity) {
      console.log(eachEntity);
      console.log(value);
      console.log(processedTasks[eachEntity]);
      if (processedTasks[eachEntity])
        processedTasks[eachEntity] = processedTasks[eachEntity].concat(value);
      else processedTasks[eachEntity] = value;
    });
  }
  console.log(JSON.stringify(processedTasks));
  tasks = processedTasks;
  return { tasks, inputData };
};
