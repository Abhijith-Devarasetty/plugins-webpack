

function processData(input) {
    let output = {
      projects: {
        application: processApplications(input.applications),
        entities: processEntities(input),
        components : processComponents(input.applications)
      },
    };
    return output;
  }
  
  function processApplications(applications) {
    let application = [];
    applications.forEach((app) => {
      let appName = jsonata("$.appName").evaluate(app);
      let value = {
        name: appName,
        templateId: app.templateId,
        framework: jsonata("$.framework").evaluate(app),
        package: jsonata("**.package_name").evaluate(app),
        port: jsonata("**.port").evaluate(app),
        database: {
          url: jsonata("**.database.**.url").evaluate(app),
          dbName: "",
          username: "",
          password: "",
        },
        tracing: jsonata("**.tracing").evaluate(app),
        logging: jsonata("**.logging").evaluate(app),
        discovery: jsonata("**.discovery").evaluate(app),
        entities: jsonata("$.**.entities.entity").evaluate(app),
        components: []
      };
      application.push({ [appName]: value });
    });
  
    return application;
  }
  
  function processEntities(input) {
    let temp = jsonata("**.entities[entity = 'resturant']").evaluate(input);
    let entity = {
      name: temp.entity,
      isCompositeKey: jsonata("$count(schema.meta[pk ='true'])>1").evaluate(temp),
      attributes: processAttributes(temp.schema),
    };
    return entity;
  }
  
  function processFields(comp) {
      let arr =[];
      comp = comp.componentStructure;
      comp.forEach(field =>{
          arr.push({
              "element":field.input_type,
              "label":field.label,
              "name":field.name
          });
      })
      return arr;
  }
  
  function processComponents(applications){
      let components = jsonata("**.components").evaluate(applications);
      console.log(components,applications);
      let arr = [];
      components.forEach(comp =>{
          let appName = jsonata("$.componentName").evaluate(comp);
          let value = {
              "name":appName,
              "fields":processFields(comp)
          }
          arr.push({ [appName]:value });
      })
      return arr;
  }
  
  function processEnums(input) {}
  
  function processResources(input) {}
  
  function processAttributes(schema) {
    let result = [];
    schema.forEach((ele) => {
      result.push({
        name: ele.attribute,
        dataType: ele.dataType,
        columnName:
          ele.meta.column != undefined ? ele.meta.column : ele.attribute,
        primaryKey: ele.meta.pk ? "true" : "fasle",
      });
    });
    return result;
  }

  module.exports = { processData };