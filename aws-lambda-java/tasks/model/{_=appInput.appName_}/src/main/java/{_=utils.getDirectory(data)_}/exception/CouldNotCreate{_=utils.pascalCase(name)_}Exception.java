package <%=utils.getPackageName(data)%>.exception;

<%let capEntityName = utils.pascalCase(name)-%>
<%let entityName = name-%>
public class CouldNotCreate<%=capEntityName%>Exception extends IllegalStateException {
    public CouldNotCreate<%=capEntityName%>Exception(String message) {
        super(message);
    }
}
