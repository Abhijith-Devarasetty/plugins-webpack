<%let packageName = utils.getPackageName(data)-%>
<%let capEntityName = utils.pascalCase(name)-%>
<%let entityName = name-%>
<%let pk = utils.lowerFirst(utils.getPrimary(attributes))-%>
<%let dashPk = utils.dashName(pk)-%>
<%let capDashPk = utils.toUpper(dashPk)-%>
<%let pascalPk = utils.pascalCase(pk)-%>
package <%=capEntityName%>.dao;

import <%=capEntityName%>.exception.CouldNotCreate<%=capEntityName%>Exception;
import <%=capEntityName%>.exception.<%=capEntityName%>DoesNotExistException;
import <%=capEntityName%>.exception.TableDoesNotExistException;
import <%=capEntityName%>.exception.UnableToDeleteException;
import <%=capEntityName%>.exception.UnableToUpdateException;
import <%=capEntityName%>.model.<%=capEntityName%>;
import <%=capEntityName%>.model.<%=capEntityName%>Page;
import <%=capEntityName%>.model.request.Create<%=capEntityName%>Request;

import software.amazon.awssdk.services.dynamodb.DynamoDbClient;
import software.amazon.awssdk.services.dynamodb.model.AttributeValue;
import software.amazon.awssdk.services.dynamodb.model.ConditionalCheckFailedException;
import software.amazon.awssdk.services.dynamodb.model.DeleteItemRequest;
import software.amazon.awssdk.services.dynamodb.model.DeleteItemResponse;
import software.amazon.awssdk.services.dynamodb.model.GetItemRequest;
import software.amazon.awssdk.services.dynamodb.model.GetItemResponse;
import software.amazon.awssdk.services.dynamodb.model.PutItemRequest;
import software.amazon.awssdk.services.dynamodb.model.ResourceNotFoundException;
import software.amazon.awssdk.services.dynamodb.model.ReturnValue;
import software.amazon.awssdk.services.dynamodb.model.ScanRequest;
import software.amazon.awssdk.services.dynamodb.model.ScanResponse;
import software.amazon.awssdk.services.dynamodb.model.UpdateItemRequest;
import software.amazon.awssdk.services.dynamodb.model.UpdateItemResponse;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class <%=capEntityName%>Dao {

    private static final String UPDATE_EXPRESSION
            = "<%-utils.getUpdateExpres(attributes)%>";
    private static final String <%=capDashPk%> = "<%=pk%>";
<%attributes.forEach(e=>{-%>
<%if(e.primaryKey !== 'true'){-%>
    private static final String <%-utils.capDashName(e.name)%>_WAS_NULL = "<%=e.name%> was null";
<%}-%>
<%})-%>

    private final String tableName;
    private final DynamoDbClient dynamoDb;
    private final int pageSize;

    /**
     * Constructs an <%=capEntityName%>Dao.
     * @param dynamoDb dynamodb client
     * @param tableName name of table to use for <%=entityName%>s
     * @param pageSize size of pages for get<%=capEntityName%>s
     */
    public <%=capEntityName%>Dao(final DynamoDbClient dynamoDb, final String tableName,
                    final int pageSize) {
        this.dynamoDb = dynamoDb;
        this.tableName = tableName;
        this.pageSize = pageSize;
    }

    /**
     * Returns an <%=entityName%> or throws if the <%=entityName%> does not exist.
     * @param <%=entityName%>Id id of <%=entityName%> to get
     * @return the <%=entityName%> if it exists
     * @throws <%=capEntityName%>DoesNotExistException if the <%=entityName%> does not exist
     */
    public <%=capEntityName%> get<%=capEntityName%>(final String <%=pk%>) {
        try {
            return Optional.ofNullable(
                    dynamoDb.getItem(GetItemRequest.builder()
                            .tableName(tableName)
                            .key(Collections.singletonMap(<%=capDashPk%>,
                                    AttributeValue.builder().s(<%=pk%>).build()))
                            .build()))
                    .map(GetItemResponse::item)
                    .map(this::convert)
                    .orElseThrow(() -> new <%=capEntityName%>DoesNotExistException("<%=capEntityName%> "
                            + <%=pk%> + " does not exist"));
        } catch (ResourceNotFoundException e) {
            throw new TableDoesNotExistException("<%=capEntityName%> table " + tableName + " does not exist");
        }
    }

    /**
     * Gets a page of <%=entityName%>s, at most pageSize long.
     * @param exclusiveStart<%=pascalPk%> the exclusive start id for the next page.
     * @return a page of <%=entityName%>s.
     * @throws TableDoesNotExistException if the <%=entityName%> table does not exist
     */
    public <%=capEntityName%>Page get<%=capEntityName%>s(final String exclusiveStart<%=pascalPk%>) {
        final ScanResponse result;

        try {
            ScanRequest.Builder scanBuilder = ScanRequest.builder()
                    .tableName(tableName)
                    .limit(pageSize);
            if (!isNullOrEmpty(exclusiveStart<%=pascalPk%>)) {
                scanBuilder.exclusiveStartKey(Collections.singletonMap(<%=capDashPk%>,
                        AttributeValue.builder().s(exclusiveStart<%=pascalPk%>).build()));
            }
            result = dynamoDb.scan(scanBuilder.build());
        } catch (ResourceNotFoundException e) {
            throw new TableDoesNotExistException("<%=capEntityName%> table " + tableName
                    + " does not exist");
        }

        final List<<%=capEntityName%>> <%=entityName%>s = result.items().stream()
                .map(this::convert)
                .collect(Collectors.toList());

        <%=capEntityName%>Page.<%=capEntityName%>PageBuilder builder = <%=capEntityName%>Page.builder().<%=entityName%>s(<%=entityName%>s);
        if (result.lastEvaluatedKey() != null && !result.lastEvaluatedKey().isEmpty()) {
            if ((!result.lastEvaluatedKey().containsKey(<%=capDashPk%>)
                    || isNullOrEmpty(result.lastEvaluatedKey().get(<%=capDashPk%>).s()))) {
                throw new IllegalStateException(
                    "<%=pk%> did not exist or was not a non-empty string in the lastEvaluatedKey");
            } else {
                builder.lastEvaluatedKey(result.lastEvaluatedKey().get(<%=capDashPk%>).s());
            }
        }

        return builder.build();
    }

    /**
     * Updates an <%=entityName%> object.
     * @param <%=entityName%> <%=entityName%> to update
     * @return updated <%=entityName%>
     */
    public <%=capEntityName%> update<%=capEntityName%>(final <%=capEntityName%> <%=entityName%>) {
        if (<%=entityName%> == null) {
            throw new IllegalArgumentException("<%=capEntityName%> to update was null");
        }
        String <%=pk%> = <%=entityName%>.get<%=pascalPk%>();
        if (isNullOrEmpty(<%=pk%>)) {
            throw new IllegalArgumentException("<%=pk%> was null or empty");
        }
        Map<String, AttributeValue> expressionAttributeValues = new HashMap<>();

<%attributes.forEach(e=>{-%>
<%if(e.primaryKey !== 'true'){-%>
        try {
            expressionAttributeValues.put(":<%=e.name%>_var",
                    AttributeValue.builder().<%=utils.getNorS(e)%>(<%=entityName%>.get<%=utils.pascalCase(e.name)%>().toString()).build());
        } catch (NullPointerException e) {
            throw new IllegalArgumentException(<%-utils.capDashName(e.name)%>_WAS_NULL);
        }
<%}-%>
<%})-%>
        
        final UpdateItemResponse result;
        try {
            result = dynamoDb.updateItem(UpdateItemRequest.builder()
                    .tableName(tableName)
                    .key(Collections.singletonMap(<%=capDashPk%>,
                            AttributeValue.builder().s(<%=entityName%>.get<%=pascalPk%>()).build()))
                    .returnValues(ReturnValue.ALL_NEW)
                    .updateExpression(UPDATE_EXPRESSION)
                    .conditionExpression("attribute_exists(<%=pk%>) AND version = :v")
                    .expressionAttributeValues(expressionAttributeValues)
                    .build());
        } catch (ConditionalCheckFailedException e) {
            throw new UnableToUpdateException(
                    "Either the <%=entityName%> did not exist or the provided version was not current");
        } catch (ResourceNotFoundException e) {
            throw new TableDoesNotExistException("<%=capEntityName%> table " + tableName
                    + " does not exist and was deleted after reading the <%=entityName%>");
        }
        return convert(result.attributes());
    }

    /**
     * Deletes an <%=entityName%>.
     * @param <%=entityName%>Id <%=entityName%> id of <%=entityName%> to delete
     * @return the deleted <%=entityName%>
     */
    public <%=capEntityName%> delete<%=capEntityName%>(final String <%=pk%>) {
        final DeleteItemResponse result;
        try {
            return Optional.ofNullable(dynamoDb.deleteItem(DeleteItemRequest.builder()
                            .tableName(tableName)
                            .key(Collections.singletonMap(<%=capDashPk%>,
                                    AttributeValue.builder().s(<%=pk%>).build()))
                            .conditionExpression("attribute_exists(<%=pk%>)")
                            .returnValues(ReturnValue.ALL_OLD)
                            .build()))
                    .map(DeleteItemResponse::attributes)
                    .map(this::convert)
                    .orElseThrow(() -> new IllegalStateException(
                            "Condition passed but deleted item was null"));
        } catch (ConditionalCheckFailedException e) {
            throw new UnableToDeleteException(
                    "A competing request changed the <%=entityName%> while processing this request");
        } catch (ResourceNotFoundException e) {
            throw new TableDoesNotExistException("<%=capEntityName%> table " + tableName
                    + " does not exist and was deleted after reading the <%=entityName%>");
        }
    }

    private <%=capEntityName%> convert(final Map<String, AttributeValue> item) {
        if (item == null || item.isEmpty()) {
            return null;
        }
        <%=capEntityName%>.<%=capEntityName%>Builder builder = <%=capEntityName%>.builder();

<%attributes.forEach(e=>{-%>
        try {
            builder.<%=e.name%>(<%-utils.getEleSetData(e)%>);
        } catch (NullPointerException e) {
            throw new IllegalStateException(
                    "item did not have an <%=e.name%> attribute or it was not a <%=utils.checkDT(e.dataType)%>");
        }

<%})-%>

        return builder.build();
    }

    private Map<String, AttributeValue> create<%=capEntityName%>Item(final Create<%=capEntityName%>Request <%=entityName%>) {
        Map<String, AttributeValue> item = new HashMap<>();
        item.put(<%=capDashPk%>, AttributeValue.builder().s(UUID.randomUUID().toString()).build());        
<%attributes.forEach(e=>{-%>
<%if(e.primaryKey !== 'true' && e.name !== 'version'){-%>
        try {
            item.put("<%=e.name%>",
                    AttributeValue.builder().<%=utils.getNorS(e)%>(<%=entityName%>.get<%=utils.pascalCase(e.name)%>().toString()).build());
        } catch (NullPointerException e) {
            throw new IllegalArgumentException(<%-utils.capDashName(e.name)%>_WAS_NULL);
        }
<%}else if(e.name === "version"){-%>
        item.put("version", AttributeValue.builder().n("1").build());
<%}-%>
<%})-%>

        return item;
    }


    /**
     * Creates an <%=entityName%>.
     * @param create<%=capEntityName%>Request details of <%=entityName%> to create
     * @return created <%=entityName%>
     */
    public <%=capEntityName%> create<%=capEntityName%>(final Create<%=capEntityName%>Request create<%=capEntityName%>Request) {
        if (create<%=capEntityName%>Request == null) {
            throw new IllegalArgumentException("Create<%=capEntityName%>Request was null");
        }
        int tries = 0;
        while (tries < 10) {
            try {
                Map<String, AttributeValue> item = create<%=capEntityName%>Item(create<%=capEntityName%>Request);
                dynamoDb.putItem(PutItemRequest.builder()
                        .tableName(tableName)
                        .item(item)
                        .conditionExpression("attribute_not_exists(<%=pk%>)")
                        .build());
                return <%=capEntityName%>.builder()
<%attributes.forEach(e=>{-%>
                        .<%=e.name%>(<%-utils.getEleSetData(e)%>)
<%})-%>
                        .build();
            } catch (ConditionalCheckFailedException e) {
                tries++;
            } catch (ResourceNotFoundException e) {
                throw new TableDoesNotExistException(
                        "<%=capEntityName%> table " + tableName + " does not exist");
            }
        }
        throw new CouldNotCreate<%=capEntityName%>Exception(
                "Unable to generate unique <%=entityName%> id after 10 tries");
    }

    private static boolean isNullOrEmpty(final String string) {
        return string == null || string.isEmpty();
    }
}

