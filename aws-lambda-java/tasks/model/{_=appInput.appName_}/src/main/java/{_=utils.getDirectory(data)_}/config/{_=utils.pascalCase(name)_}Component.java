<%let packageName = utils.getPackageName(data)-%>
<%let capEntityName = utils.pascalCase(name)-%>
<%let entityName = name-%>
package <%=packageName%>.config;

import <%=packageName%>.handler.Create<%=capEntityName%>Handler;
import <%=packageName%>.handler.Delete<%=capEntityName%>Handler;
import <%=packageName%>.handler.Get<%=capEntityName%>Handler;
import <%=packageName%>.handler.Get<%=capEntityName%>sHandler;
import <%=packageName%>.handler.Update<%=capEntityName%>Handler;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {<%=capEntityName%>Module.class})
public interface <%=capEntityName%>Component {

    void inject(Create<%=capEntityName%>Handler requestHandler);

    void inject(Delete<%=capEntityName%>Handler requestHandler);

    void inject(Get<%=capEntityName%>Handler requestHandler);

    void inject(Get<%=capEntityName%>sHandler requestHandler);

    void inject(Update<%=capEntityName%>Handler requestHandler);
}
