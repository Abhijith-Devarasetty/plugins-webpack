<%let packageName = utils.getPackageName(data)-%>
<%let capEntityName = utils.pascalCase(name)-%>
<%let entityName = name-%>
package <%=packageName%>.model.request;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonAutoDetect
public class Create<%=capEntityName%>Request {
<%attributes.forEach(e=>{-%>
<%if(e.primaryKey !== 'true' && e.name !== 'version'){-%>
    private <%=utils.getDataType(e)%> <%=e.name%>;
<%}-%>
<%})-%>
}
