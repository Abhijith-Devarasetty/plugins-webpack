<%let packageName = utils.getPackageName(data)-%>
package <%=packageName%>.handler;

<%let capEntityName = utils.pascalCase(name)-%>
<%let entityName = name-%>
import <%=packageName%>.config.Dagger<%=capEntityName%>Component;
import <%=packageName%>.config.<%=capEntityName%>Component;
import <%=packageName%>.dao.<%=capEntityName%>Dao;
import <%=packageName%>.exception.CouldNotCreate<%=capEntityName%>Exception;
import <%=packageName%>.model.<%=capEntityName%>;
import <%=packageName%>.model.request.Create<%=capEntityName%>Request;
import <%=packageName%>.model.response.ErrorMessage;
import <%=packageName%>.model.response.GatewayResponse;
import <%=packageName%>.services.lambda.runtime.Context;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;
import javax.inject.Inject;

public class Create<%=capEntityName%>Handler implements <%=capEntityName%>RequestStreamHandler {
<%attributes.forEach(e=>{-%>
<%if(e.primaryKey !== 'true' && e.name !== 'version'){-%>
        private static final ErrorMessage REQUIRE_<%-utils.capDashName(e.name)%>_ERROR
        = new ErrorMessage("Require <%=e.name%> to create an <%=entityName%>",
        SC_BAD_REQUEST);
<%}-%>
<%})-%>

    @Inject
    ObjectMapper objectMapper;
    @Inject
    <%=capEntityName%>Dao <%=entityName%>Dao;
    private final <%=capEntityName%>Component <%=entityName%>Component;

    public Create<%=capEntityName%>Handler() {
        <%=entityName%>Component = Dagger<%=capEntityName%>Component.builder().build();
        <%=entityName%>Component.inject(this);
    }

    @Override
    public void handleRequest(InputStream input, OutputStream output,
                              Context context) throws IOException {
        final JsonNode event;
        try {
            event = objectMapper.readTree(input);
        } catch (JsonMappingException e) {
            writeInvalidJsonInStreamResponse(objectMapper, output, e.getMessage());
            return;
        }

        if (event == null) {
            writeInvalidJsonInStreamResponse(objectMapper, output, "event was null");
            return;
        }
        JsonNode create<%=capEntityName%>RequestBody = event.findValue("body");
        if (create<%=capEntityName%>RequestBody == null) {
            objectMapper.writeValue(output,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(
                                    new ErrorMessage("Body was null",
                                            SC_BAD_REQUEST)),
                            APPLICATION_JSON, SC_BAD_REQUEST));
            return;
        }
        final Create<%=capEntityName%>Request request;
        try {
            request = objectMapper.treeToValue(
                    objectMapper.readTree(create<%=capEntityName%>RequestBody.asText()),
                    Create<%=capEntityName%>Request.class);
        } catch (JsonParseException | JsonMappingException e) {
            objectMapper.writeValue(output,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(
                                    new ErrorMessage("Invalid JSON in body: "
                                            + e.getMessage(), SC_BAD_REQUEST)),
                            APPLICATION_JSON, SC_BAD_REQUEST));
            return;
        }

        if (request == null) {
            objectMapper.writeValue(output,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(REQUEST_WAS_NULL_ERROR),
                            APPLICATION_JSON, SC_BAD_REQUEST));
            return;
        }
        
<%attributes.forEach(e=>{-%>
<%if(e.primaryKey !== 'true' && e.name !== 'version'){-%>
        if (request.get<%=utils.pascalCase(e.name)%>() == null) {
                objectMapper.writeValue(output,
                        new GatewayResponse<>(
                                objectMapper.writeValueAsString(REQUIRE_<%-utils.capDashName(e.name)%>_ERROR),
                                APPLICATION_JSON, SC_BAD_REQUEST));
                return;
        }

<%}-%>
<%})-%>

        try {
            final <%=capEntityName%> <%=entityName%> = <%=entityName%>Dao.create<%=capEntityName%>(request);
            objectMapper.writeValue(output,
                    new GatewayResponse<>(objectMapper.writeValueAsString(<%=entityName%>),
                            APPLICATION_JSON, SC_CREATED)); //TODO redirect with a 303
        } catch (CouldNotCreate<%=capEntityName%>Exception e) {
            objectMapper.writeValue(output,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(
                                    new ErrorMessage(e.getMessage(),
                                            SC_INTERNAL_SERVER_ERROR)),
                            APPLICATION_JSON, SC_INTERNAL_SERVER_ERROR));
        }
    }
}
