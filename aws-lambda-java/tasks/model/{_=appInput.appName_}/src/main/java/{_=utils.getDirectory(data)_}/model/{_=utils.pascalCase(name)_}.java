<%let packageName = utils.getPackageName(data)-%>
package <%=packageName%>.model;

<%let capEntityName = utils.pascalCase(name)-%>
<%let entityName = name-%>
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class <%=capEntityName%> {

<%attributes.forEach(e=>{-%>
	private <%=utils.getDataType(e)%> <%=e.name%>;
<%})-%>
}
