<%let packageName = utils.getPackageName(data)-%>
package <%=packageName%>.handler;

<%let capEntityName = utils.pascalCase(name)-%>
<%let entityName = name-%>
<%let pk = utils.lowerFirst(utils.getPrimary(attributes))-%>
<%let dashPk = utils.dashName(pk)-%>
<%let capDashPk = utils.toUpper(dashPk)-%>
import <%=packageName%>.config.Dagger<%=capEntityName%>Component;
import <%=packageName%>.config.<%=capEntityName%>Component;
import <%=packageName%>.dao.<%=capEntityName%>Dao;
import <%=packageName%>.exception.<%=capEntityName%>DoesNotExistException;
import <%=packageName%>.model.<%=capEntityName%>;
import <%=packageName%>.model.response.ErrorMessage;
import <%=packageName%>.model.response.GatewayResponse;
import <%=packageName%>.services.lambda.runtime.Context;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;
import javax.inject.Inject;

public class Get<%=capEntityName%>Handler implements <%=capEntityName%>RequestStreamHandler {
    @Inject
    ObjectMapper objectMapper;
    @Inject
    <%=capEntityName%>Dao <%=entityName%>Dao;
    private final <%=capEntityName%>Component <%=entityName%>Component;

    public Get<%=capEntityName%>Handler() {
        <%=entityName%>Component = Dagger<%=capEntityName%>Component.builder().build();
        <%=entityName%>Component.inject(this);
    }

    @Override
    public void handleRequest(InputStream input, OutputStream output,
                              Context context) throws IOException {
        final JsonNode event;
        try {
            event = objectMapper.readTree(input);
        } catch (JsonMappingException e) {
            writeInvalidJsonInStreamResponse(objectMapper, output, e.getMessage());
            return;
        }
        if (event == null) {
            writeInvalidJsonInStreamResponse(objectMapper, output, "event was null");
            return;
        }
        final JsonNode pathParameterMap = event.findValue("pathParameters");
        final String <<%=pk%>> = Optional.ofNullable(pathParameterMap)
                .map(mapNode -> mapNode.get("<%=dashPk%>"))
                .map(JsonNode::asText)
                .orElse(null);
        if (isNullOrEmpty(<%=pk%>)) {
            objectMapper.writeValue(output,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(<%=capDashPk%>_WAS_NOT_SET),
                            APPLICATION_JSON, SC_BAD_REQUEST));
            return;
        }
        try {
            <%=capEntityName%> <%=entityName%> = <%=entityName%>Dao.get<%=capEntityName%>(<%=pk%>);
            objectMapper.writeValue(output,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(<%=entityName%>),
                            APPLICATION_JSON, SC_OK));
        } catch (<%=capEntityName%>DoesNotExistException e) {
            objectMapper.writeValue(output,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(
                                    new ErrorMessage(e.getMessage(), SC_NOT_FOUND)),
                            APPLICATION_JSON, SC_NOT_FOUND));
        }
    }
}
