<%let packageName = utils.getPackageName(data)-%>
<%let capEntityName = utils.pascalCase(name)-%>
<%let entityName = name-%>
package <%=packageName%>.model.response;

import <%=packageName%>.model.<%=capEntityName%>;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
@JsonAutoDetect
public class Get<%=capEntityName%>sResponse {
    private final String lastEvaluatedKey;
    private final List<<%=capEntityName%>> <%=entityName%>s;
}
