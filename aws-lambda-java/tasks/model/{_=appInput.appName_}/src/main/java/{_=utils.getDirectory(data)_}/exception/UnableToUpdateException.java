package <%=utils.getPackageName(data)%>.exception;

public class UnableToUpdateException extends IllegalStateException {
    public UnableToUpdateException(String message) {
        super(message);
    }
}
