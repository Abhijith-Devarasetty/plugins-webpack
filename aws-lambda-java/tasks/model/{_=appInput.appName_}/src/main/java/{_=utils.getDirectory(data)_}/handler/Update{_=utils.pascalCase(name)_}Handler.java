<%let packageName = utils.getPackageName(data)-%>
package <%=packageName%>.handler;

<%let capEntityName = utils.pascalCase(name)-%>
<%let entityName = name-%>
<%let pk = utils.lowerFirst(utils.getPrimary(attributes))-%>
<%let dashPk = utils.dashName(pk)-%>
<%let capDashPk = utils.toUpper(dashPk)-%>
import <%=packageName%>.config.Dagger<%=capEntityName%>Component;
import <%=packageName%>.config.<%=capEntityName%>Component;
import <%=packageName%>.dao.<%=capEntityName%>Dao;
import <%=packageName%>.exception.TableDoesNotExistException;
import <%=packageName%>.exception.UnableToUpdateException;
import <%=packageName%>.model.<%=capEntityName%>;
import <%=packageName%>.model.request.Update<%=capEntityName%>Request;
import <%=packageName%>.model.response.ErrorMessage;
import <%=packageName%>.model.response.GatewayResponse;
import <%=packageName%>.services.lambda.runtime.Context;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;
import javax.inject.Inject;

public class Update<%=capEntityName%>Handler implements <%=capEntityName%>RequestStreamHandler {
    @Inject
    ObjectMapper objectMapper;
    @Inject
    <%=capEntityName%>Dao <%=entityName%>Dao;
    private final <%=capEntityName%>Component <%=entityName%>Component;

    public Update<%=capEntityName%>Handler() {
        <%=entityName%>Component = Dagger<%=capEntityName%>Component.builder().build();
        <%=entityName%>Component.inject(this);
    }

    @Override
    public void handleRequest(InputStream input, OutputStream output,
                              Context context) throws IOException {
        final JsonNode event;
        try {
            event = objectMapper.readTree(input);
        } catch (JsonMappingException e) {
            writeInvalidJsonInStreamResponse(objectMapper, output, e.getMessage());
            return;
        }
        if (event == null) {
            writeInvalidJsonInStreamResponse(objectMapper, output, "event was null");
            return;
        }
        final JsonNode pathParameterMap = event.findValue("pathParameters");
        final String <%=pk%> = Optional.ofNullable(pathParameterMap)
                .map(mapNode -> mapNode.get("<%=dashPk%>"))
                .map(JsonNode::asText)
                .orElse(null);
        if (isNullOrEmpty(<%=pk%>)) {
            objectMapper.writeValue(output,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(<%=capDashPk%>_WAS_NOT_SET),
                            APPLICATION_JSON, SC_BAD_REQUEST));
            return;
        }

        JsonNode update<%=capEntityName%>RequestBody = event.findValue("body");
        if (update<%=capEntityName%>RequestBody == null) {
            objectMapper.writeValue(output,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(
                                    new ErrorMessage("Body was null",
                                            SC_BAD_REQUEST)),
                            APPLICATION_JSON, SC_BAD_REQUEST));
            return;
        }

        final Update<%=capEntityName%>Request request;
        try {
            request = objectMapper.readValue(
                    update<%=capEntityName%>RequestBody.asText(), Update<%=capEntityName%>Request.class);
        } catch (JsonParseException | JsonMappingException e) {
            objectMapper.writeValue(output,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(
                                    new ErrorMessage("Invalid JSON in body: "
                                            + e.getMessage(), SC_BAD_REQUEST)),
                            APPLICATION_JSON, SC_BAD_REQUEST));
            return;
        }

        if (request == null) {
            objectMapper.writeValue(output,
                    new GatewayResponse<>(
                            objectMapper.writeValueAsString(REQUEST_WAS_NULL_ERROR),
                            APPLICATION_JSON, SC_BAD_REQUEST));
            return;
        }

        try {
            <%=capEntityName%> updated<%=capEntityName%> = <%=entityName%>Dao.update<%=capEntityName%>(
                    <%=capEntityName%>.builder().<%=pk%>(<%=pk%>)
<%attributes.forEach(e=>{-%>
<%if(e.primaryKey !== 'true'){-%>
                                .<%=e.name%>(request.get<%=utils.pascalCase(e.name)%>())
<%}-%>
<%})-%>
                            .build());
            objectMapper.writeValue(output, new GatewayResponse<>(
                    objectMapper.writeValueAsString(updated<%=capEntityName%>),
                    APPLICATION_JSON, SC_OK));
        } catch (UnableToUpdateException e) {
            objectMapper.writeValue(output, new GatewayResponse<>(
                    objectMapper.writeValueAsString(
                            new ErrorMessage(e.getMessage(), SC_CONFLICT)),
                    APPLICATION_JSON, SC_CONFLICT));
        } catch (TableDoesNotExistException e) {
            objectMapper.writeValue(output, new GatewayResponse<>(
                    objectMapper.writeValueAsString(
                            new ErrorMessage(e.getMessage(), SC_BAD_REQUEST)),
                    APPLICATION_JSON, SC_BAD_REQUEST));
        } catch (IllegalArgumentException e) {
            objectMapper.writeValue(output, new GatewayResponse<>(
                    objectMapper.writeValueAsString(
                            new ErrorMessage(e.getMessage(), SC_BAD_REQUEST)),
                    APPLICATION_JSON, SC_BAD_REQUEST));
        } catch (IllegalStateException e) {
            objectMapper.writeValue(output, new GatewayResponse<>(
                    objectMapper.writeValueAsString(
                            new ErrorMessage(e.getMessage(), SC_INTERNAL_SERVER_ERROR)),
                    APPLICATION_JSON, SC_INTERNAL_SERVER_ERROR));
        }
    }
}
