package <%=utils.getPackageName(data)%>.model;

import lombok.Builder;
import lombok.Getter;

import java.util.List;

<%let capEntityName = utils.pascalCase(name)-%>
<%let entityName = name-%>
@Builder
@Getter
public class <%=capEntityName%>Page {
    private final List<<%=capEntityName%>> <%=entityName%>s;
    private final String lastEvaluatedKey;
}
