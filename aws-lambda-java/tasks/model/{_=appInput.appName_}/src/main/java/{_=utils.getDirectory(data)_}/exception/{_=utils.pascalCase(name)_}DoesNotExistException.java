package <%=utils.getPackageName(data)%>.exception;

<%let capEntityName = utils.pascalCase(name)-%>
<%let entityName = name-%>
public class <%=capEntityName%>DoesNotExistException extends IllegalArgumentException {

    public <%=capEntityName%>DoesNotExistException(String message) {
        super(message);
    }
}
