<%let packageName = utils.getPackageName(data)-%>
<%let capEntityName = utils.pascalCase(name)-%>
<%let entityName = name-%>
package <%=capEntityName%>.model.request;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@JsonAutoDetect
public class Get<%=capEntityName%>sRequest {
    private String exclusiveStartKey;
}
