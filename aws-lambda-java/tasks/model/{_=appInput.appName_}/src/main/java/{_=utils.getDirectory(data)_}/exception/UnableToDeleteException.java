package <%=utils.getPackageName(data)%>.exception;

public class UnableToDeleteException extends IllegalStateException {
    public UnableToDeleteException(String message) {
        super(message);
    }
}
