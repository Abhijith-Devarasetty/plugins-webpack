<%let packageName = utils.getPackageName(data)-%>
<%let capEntityName = utils.pascalCase(name)-%>
<%let entityName = name-%>
package <%=capEntityName%>.model.request;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@JsonAutoDetect
public class Update<%=capEntityName%>Request {
<%attributes.forEach(e=>{-%>
	private <%=utils.getDataType(e)%> <%=e.name%>;
<%})-%>
}
