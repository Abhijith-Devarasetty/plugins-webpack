<%let packageName = utils.getPackageName(data)-%>
package <%=packageName%>.handler;

<%let capEntityName = utils.pascalCase(name)-%>
<%let entityName = name-%>
import <%=packageName%>.config.Dagger<%=capEntityName%>Component;
import <%=packageName%>.config.<%=capEntityName%>Component;
import <%=packageName%>.dao.<%=capEntityName%>Dao;
import <%=packageName%>.model.<%=capEntityName%>Page;
import <%=packageName%>.model.response.GatewayResponse;
import <%=packageName%>.model.response.Get<%=capEntityName%>sResponse;
import <%=packageName%>.services.lambda.runtime.Context;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Optional;
import javax.inject.Inject;

public class Get<%=capEntityName%>sHandler implements <%=capEntityName%>RequestStreamHandler {
    @Inject
    ObjectMapper objectMapper;
    @Inject
    <%=capEntityName%>Dao <%=entityName%>Dao;
    private final <%=capEntityName%>Component <%=entityName%>Component;

    public Get<%=capEntityName%>sHandler() {
        <%=entityName%>Component = Dagger<%=capEntityName%>Component.builder().build();
        <%=entityName%>Component.inject(this);
    }

    @Override
    public void handleRequest(InputStream input, OutputStream output,
                              Context context) throws IOException {
        final JsonNode event;
        try {
            event = objectMapper.readTree(input);
        } catch (JsonMappingException e) {
            writeInvalidJsonInStreamResponse(objectMapper, output, e.getMessage());
            return;
        }
        final JsonNode queryParameterMap = event.findValue("queryParameters");
        final String exclusiveStartKeyQueryParameter = Optional.ofNullable(queryParameterMap)
                .map(mapNode -> mapNode.get("exclusive_start_key").asText())
                .orElse(null);

        <%=capEntityName%>Page page = <%=entityName%>Dao.get<%=capEntityName%>s(exclusiveStartKeyQueryParameter);
        //TODO handle exceptions
        objectMapper.writeValue(output, new GatewayResponse<>(
                objectMapper.writeValueAsString(
                        new Get<%=capEntityName%>sResponse(page.getLastEvaluatedKey(), page.get<%=capEntityName%>s())),
                APPLICATION_JSON, SC_OK));
    }
}
