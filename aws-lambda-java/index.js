const _ = require("lodash");
const path = require("path");
let jsonata = require("jsonata");
const { isEmpty } = require("lodash");
// const test = require("./utils.js");
// const {getRestCall} = require("./utils.js");

_.mixin({ pascalcase: _.flow(_.camelCase, _.upperFirst) });

function pascalCase(input) {
  return _.pascalcase(input);
}

function dashName(name){
 return _.camelCase(name).replace(/[A-Z]/g, '_$&').toLowerCase();
}

function lowerFirst(name){
  return _.lowerFirst(name);
}

function toUpper(name){
  return _.toUpper(name);
}

function capDashName(name){
  return toUpper(dashName(name));
}

function getDirectory(data) {
  let p = data.project.application.package.replace(/\./g, "//");
  return path.normalize(p);
}

function getPackageName(data) {
  return data.project.application.package;
}

function getEntityName(data) {
  return data.project.entities[0].name;
}

function getCapEntityName(data) {
  let entityName = data.project.entities[0].name;
  return pascalCase(entityName);
}

function repoPrimary(schema, entity) {
  if (jsonata("$count(*[primaryKey ='true'])>1").evaluate(schema)) {
    return pascalCase(entity) + "Id";
  }
  if (jsonata("$count(*[primaryKey ='true'])<1").evaluate(schema)) {
    return "Long";
  }
  for (let i = 0; i < schema.length; i++) {
    if (schema[i].primaryKey === "true") {
      switch (getDataType(schema[i])) {
        case "int":
          return "Integer";
        case "long":
          return "Long";
        case "Long":
          return "Long";
        default:
          return "Long";
      }
    }
  }
}

function getImports(attributes) {
  let set = new Set();
  let result = "";
  attributes.forEach((ele) => {
    if (!_.isEmpty(ele.actualDataType)) {
      set.add("import " + ele.actualDataType + ";");
    }
    if (ele.dataType == "clob") {
      set.add("import java.sql.Clob;");
    }
  });
  for (let temp of set) {
    result =
      result +
      temp +
      `
`;
  }
  return result;
}

function getPrimaryDataType(attributes) {
  for (let i = 0; i < attributes.length; i++) {
    if (attributes[i].primaryKey === "true") {
      return getDataType(attributes[i]);
    }
  }
}


function getDataType(ele) {
  switch (ele.dataType) {
    case "int":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "int";
      }
    case "integer":
      return "int";
    case "decimal":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "float";
      }
    case "string":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "String";
      }
    case "boolean":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "boolean";
      }
    case "date":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "Date";
      }
    case "time":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "Timestamp";
      }
    case "clob":
      return "Clob";
    case "blob":
      return "byte[]";
    default:
      return ele.dataType;
  }
}

function getRepository(data) {
  switch (data.project.application.database.dbName) {
    case "mongo":
      return "MongoRepository";
    default:
      return "JpaRepository";
  }
}

function getDataBase(data) {
  switch (data.project.application.database.dbName) {
    case "h2":
      return `
      <dependency>
			    <groupId>com.h2database</groupId>
			    <artifactId>h2</artifactId>
			    <scope>runtime</scope>
		  </dependency>`;

    case "mysql":
      return `
      <dependency>
			    <groupId>mysql</groupId>
			    <artifactId>mysql-connector-java</artifactId>
			    <scope>runtime</scope>
      </dependency>`;

    case "mongo":
      return `
      <dependency>
			    <groupId>org.springframework.boot</groupId>
			    <artifactId>spring-boot-starter-data-mongodb</artifactId>
      </dependency>`;

    case "postgresql":
      return `
      <dependency>
			    <groupId>org.postgresql</groupId>
			    <artifactId>postgresql</artifactId>
			    <scope>runtime</scope>
		  </dependency>`;
  }
}

function getDataBaseProperties(data) {
  let path = data.project.application.database.url;
  switch (data.project.application.database.dbName) {
    case "h2":
      return `
spring.h2.console.enabled=true
spring.database.platform=h2
spring.database.url=${path}`;

    case "mysql":
      return `
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.url=${path}
spring.datasource.username= 
spring.datasource.password=
spring.jpa.show-sql=true
spring.jpa.hibernate.ddl-auto=update
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5Dialect`;

    case "mongo":
      return `spring.data.mongo.uri=${path}`;

    case "postgresql":
      return `
spring.jpa.database=POSTGRESQL
spring.datasource.platform=postgres
spring.datasource.url=${path}
spring.datasource.username= 
spring.datasource.password= 
spring.jpa.show-sql=true
spring.jpa.generate-ddl=true
spring.jpa.hibernate.ddl-auto=update
spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true`;
  }
}

function getPrimary(attributes){
  for (let i = 0; i < attributes.length; i++) {
    if (attributes[i].primaryKey === "true") {
      return pascalCase(attributes[i].name);
    }
  }
}

function getEleSetData(e){
  let str= '';
  if(e.actualDataType != ""){
    datatype = e.actualDataType.split('.');
    datatype = datatype[datatype.length-1];
    switch (datatype){
      case 'string': 
        str = `item.get("${e.name}").s()`;
        break;
      case 'Long': 
        str = `Long.valueOf(item.get("${e.name}").n())`;
        break;
      case 'BigDecimal': 
        str = `new BigDecimal(item.get("${e.name}").n())`;
        break;
      default: str = `item.get("${e.name}").s()`;
    }
  }
  else{
    str = `item.get("${e.name}").s()`;
  }
  return str;
}

function checkDT(dataType){
  if(dataType === 'string'){
    return 'String';
  }
  else if(dataType === 'int'){
    return 'Number';
  }
}

function getNorS(ele){
  if(ele.dataType === "string"){
    return 's';
  }
  else if(ele.dataType === "int"){
    return 'n';
  }
}

function getUpdateExpres(attributes){
  let str = 'SET';
  for(i = 0;i<attributes.length;i++){
    if(i<attributes.length-2 && attributes[i].primaryKey !== 'true'){
      str = `${str} ${attributes[i].name} = :${attributes[i].name}_var,`;
    }else if(i === attributes.length-2 && attributes[i].primaryKey !== 'true'){
      str = `${str} ${attributes[i].name} = :${attributes[i].name}_var AND`;
    }else if(i === attributes.length-1 && attributes[i].primaryKey !== 'true'){
      str = `${str} ${attributes[i].name} = :${attributes[i].name}_var`;
    }
  }
  return str;
}

module.exports = {
  getDirectory,
  getEntityName,
  getCapEntityName,
  getPackageName,
  getDataBase,
  getDataBaseProperties,
  getRepository,
  getDataType,
  getImports,
  getPrimaryDataType,
  repoPrimary,
  pascalCase,
  getPrimary,
  dashName,
  getEleSetData,
  checkDT,
  toUpper,
  capDashName,
  getNorS,
  getUpdateExpres,
  lowerFirst
};
