// module.exports.run = (inputData, lookup, tasks, utils) => {
//   //transform your data
//   //and return Object with tasks and ...params
//   return { tasks };
// };

let jsonata = require("jsonata");
module.exports.run = (inputData, lookup, tasks) => {
  //let tasksFile = JSON.parse(fs.readFileSync('tasksFile.json', { encoding: "utf8" }));
  console.log("from pre processor"+JSON.stringify(tasks));
  let expression = jsonata('entities[schema[meta[pk="true"]]]');
  let result = expression.evaluate(lookup);
  tasks["dao"] = result;
  return { tasks, result };
};
