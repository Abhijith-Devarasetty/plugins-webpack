const yaml = require("js-yaml");
const fs = require("fs");
var _ = require("lodash");

function getType(prop) {
  if (Object.keys(prop).includes("$ref")) {
    return `${prop.$ref}`.split("/")[2];
  }
  if (prop.type === "integer") {
    if (prop.format === "int64") return "long";
    if (prop.format === "int32") return "int";
  } else if (prop.type === "array") {
    if (
      Object.keys(prop.items).includes("type") &&
      prop.items.type === "string"
    ) {
      return prop.items.type + "[]";
    }
    if (Object.keys(prop.items).includes("$ref")) {
      return `${prop.items.$ref}`.split("/")[2] + "[]";
    }
  } else if (prop.type === "string") {
    if (Object.keys(prop).includes("format") && prop.format === "date-time") {
      return "date";
    }
    return "string";
  }
}

function processYaml(path) {
  const api = yaml.safeLoad(fs.readFileSync(path, "utf8"));
  // console.log(api);
  let actions = [];
  Object.keys(api.paths).forEach((route) => {
    Object.keys(api.paths[route]).forEach((req) => {
      let reqJson = api.paths[route][req];
      let temp = {
        method: "",
        method_name: "",
        request: "",
        response: "",
        req_params: [],
        path_params: [],
      };
      temp.method_name = reqJson.operationId;
      temp.method = `http ${_.toUpper(req)} :${route}`;
      reqJson.parameters.forEach((p) => {
        //request params
        if (p.in === "body") {
          if (Object.keys(p.schema).includes("$ref")) {
            p.schema = api.definitions[`${p.schema.$ref}`.split("/")[2]];
          } else if (
            Object.keys(p.schema).includes("items") &&
            Object.keys(p.schema.items).includes("$ref")
          ) {
            p.schema.items =
              api.definitions[`${p.schema.items.$ref}`.split("/")[2]];
          }
          if (p.schema.type === "array") {
            temp.request = p.schema.items.xml.name + "[]";
          }
          if (p.schema.type === "object") {
            temp.request = p.schema.xml.name;
          }
        } else if (p.in === "path") {
          temp.path_params = [
            ...temp.path_params,
            `${p.name}:${p.type === "array" ? p.items.type + "[]" : p.type}`,
          ];
        } else if (p.in === "query") {
          temp.req_params = [
            ...temp.path_params,
            `${p.name}:${p.type === "array" ? p.items.type + "[]" : p.type}`,
          ];
        }
      });
      //responses
      if (Object.keys(reqJson.responses).includes("200")) {
        let p = reqJson.responses["200"];
        if (Object.keys(p.schema).includes("$ref")) {
          p.schema = api.definitions[`${p.schema.$ref}`.split("/")[2]];
        } else if (
          Object.keys(p.schema).includes("items") &&
          Object.keys(p.schema.items).includes("$ref")
        ) {
          p.schema.items =
            api.definitions[`${p.schema.items.$ref}`.split("/")[2]];
        }
        if (reqJson.responses["200"].schema.type === "array") {
          temp.response = reqJson.responses["200"].schema.items.xml.name + "[]";
        }
        if (reqJson.responses["200"].schema.type === "object") {
          if (Object.keys(reqJson.responses["200"].schema).includes("xml")) {
            temp.response = reqJson.responses["200"].schema.xml.name;
          } else {
            temp.response = "Object";
          }
        }
      }
      actions.push(temp);
    });
  });
  let entities = [];
  Object.keys(api.definitions).forEach((e) => {
    let temp = {
      name: e,
      attributes: [],
      taskId: "model",
      tableName:'',
      meta: {},
      isCompositeKey: false,
      relations: {},
    };
    if (api.definitions[e].type === "object") {
      let props = api.definitions[e].properties;
      Object.keys(props).forEach((p) => {
        let t = {
          name: p,
          dataType: getType(props[p])
        };
        if (Object.keys(props[p]).includes("enum")) {
          t["enum"] = props[p]["enum"];
          t.dataType = p;
        }
        temp.attributes.push(t);
      });
    }
    entities.push(temp);
  });
  // console.log(JSON.stringify({ entities, actions }));
  return { entities, actions };
}


module.exports = { processYaml };
