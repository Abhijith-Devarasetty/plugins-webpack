package <%=utils.getPackageName(data)%>.service;

import java.util.*;
import java.util.stream.Collectors;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

<%let capEntityName = utils.pascalCase(name)-%>
<%let EntityName = name-%>

import <%=utils.getPackageName(data)%>.model.<%=capEntityName%>;

@Service
public class <%=capEntityName%>ApacheService {

<%let count = 1%>
<%config.routes.forEach(r => {-%>
<%let action = utils.getRestCall(r)-%>
<%if(r.response.includes("array")){-%>
	List<<%=capEntityName%>> result<%=count%>  = new ArrayList<<%=capEntityName%>>();
<%}else{-%>
	<%-utils.getResponseEntity(r,utils.getRef)%> result<%=count%> = new <%-utils.getResponseEntity(r,utils.getRef)%>();
<%}-%>
	public <%-utils.getResponseEntity(r,utils.getRef)%>  <%-utils.actionName(r,utils.getRef)%>(<%-utils.getRouteParams(r)%>){
		CamelContext ctx = new DefaultCamelContext();
    	RouteBuilder route = new RouteBuilder() {
<%if(r.path_params !== undefined){-%>
		String getUrl = <%-utils.getRouteUrl(r)%>;
<%}-%>
    	    
    	@Override
    	public void configure() throws Exception {
    	   
    	    from("direct:<%-utils.actionName(r,utils.getRef)%>")
    	    .setHeader(Exchange.HTTP_METHOD,simple("<%-action['met']%>"))
    	    .process(new Processor() {
				
				@Override
				public void process(Exchange exchange) throws Exception {
					result<%=count%> = (<%-utils.getResponseEntity(r,utils.getRef)%>) exchange.getIn().getBody(<%=capEntityName%>.class);
<%if(r.path_params !== undefined){-%>
					exchange.getIn().setHeader(Exchange.HTTP_URI, getUrl);
<%}-%>
				}
			})
    	    .to("<%=r.url%>");

    	    }
    	};try {
    	    ctx.addRoutes(route);
    	    ctx.start();
    	    Thread.sleep(40000);
    	    ctx.stop();
    	 } catch (Exception e) {
    	    e.printStackTrace();
    	 }
    	return result<%=count%>;
	}

<%count = count + 1-%>

<%})-%>
    
}
