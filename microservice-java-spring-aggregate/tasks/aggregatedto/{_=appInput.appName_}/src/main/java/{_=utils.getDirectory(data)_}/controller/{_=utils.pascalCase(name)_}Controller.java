package <%=utils.getPackageName(data)%>.controller;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

<%let capEntityName = utils.pascalCase(name)-%>
<%let EntityName = name-%>
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import <%=utils.getPackageName(data)%>.model.<%=capEntityName%>;
import <%=utils.getPackageName(data)%>.service.<%=capEntityName%>Service;
import io.swagger.annotations.ApiOperation;

@RequestMapping("/<%=capEntityName%>")
@RestController
public class <%=capEntityName%>Controller {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private <%=capEntityName%>Service service;
	
	@GetMapping("/getall")
	@ResponseBody
	public List<<%=capEntityName%>> get<%=capEntityName%>(){
		log.info("controller get all <%=capEntityName%> method");
		return service.getAll<%=capEntityName%>();
	}

}
