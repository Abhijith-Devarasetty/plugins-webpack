package <%=utils.getPackageName(data)%>.service;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

<%let capEntityName = utils.pascalCase(name)-%>
<%let EntityName = name-%>
<%let parEntities = utils.getParentEntities(attributes)-%>
<%let len-%>

import <%=utils.getPackageName(data)%>.model.<%=capEntityName%>;

<%if(utils.checkMiddleware(data)){-%>
<%parEntities.forEach(e=>{-%>
import <%=utils.getPackageName(data)%>.model.<%=utils.pascalCase(e)%>;
import <%=utils.getPackageName(data)%>.service.<%=utils.pascalCase(e)%><%=utils.pascalCase(data.project.repository)%>Service;
<%})-%>

@Service
public class <%=capEntityName%>Service {

<%parEntities.forEach(e=>{-%>
    @Autowired
    private <%=utils.pascalCase(e)%><%=utils.pascalCase(data.project.repository)%>Service <%=e%>Service;

<%})-%>

	public List<<%=capEntityName%>> getAll<%=capEntityName%>(){
                return ( (List<<%=utils.pascalCase(parEntities[0])%>>) <%-utils.getAllApacheMet(parEntities,utils.getRef)%>).stream()
                .map(this::convertTo<%=capEntityName%>)
                .collect(Collectors.toList());
        }
    
        private <%=capEntityName%> convertTo<%=capEntityName%>(<%=utils.pascalCase(parEntities[0])%> <%=parEntities[0]%> ) {
        <%=capEntityName%> <%=EntityName%> = new <%=capEntityName%>();
        <%=utils.pascalCase(parEntities[1])%> <%=parEntities[1]%>  = <%-utils.getRellAttr(data,parEntities[1],parEntities[0])%>;  
<%attributes.forEach(a => {-%>
        <%-utils.getAggregateSetter(a,EntityName)%>
<%})-%>
        return <%=EntityName%>;
    }

}
<%}else{-%>
<%parEntities.forEach(e=>{-%>
import <%=utils.getPackageName(data)%>.model.<%=utils.pascalCase(e)%>;
import <%=utils.getPackageName(data)%>.service.<%=utils.pascalCase(e)%>Service;
<%})-%>

@Service
public class <%=capEntityName%>Service {

<%parEntities.forEach(e=>{-%>
    @Autowired
    private <%=utils.pascalCase(e)%>Service <%=utils.lowerFirst(e)%>Service;
    
<%})-%>

        public List<<%=capEntityName%>> getAll<%=capEntityName%>(){
                return ( (List<<%=utils.pascalCase(parEntities[0])%>>) <%=parEntities[0]%>Service.getAll<%=utils.pascalCase(parEntities[0])%>()).stream()
                .map(this::convertTo<%=capEntityName%>)
                .collect(Collectors.toList());
        }


        private <%=capEntityName%> convertTo<%=capEntityName%>(<%=utils.pascalCase(parEntities[0])%> <%=parEntities[0]%>) {
            <%=utils.pascalCase(parEntities[1])%> <%=parEntities[1]%>  = <%=parEntities[1]%>Service.<%-utils.getRellAttr(data,parEntities[1],parEntities[0])%>;
            <%=capEntityName%> <%=EntityName%> = new <%=capEntityName%>();
<%attributes.forEach(a => {-%>
            <%-utils.getAggregateSetter(a,EntityName)%>
<%})-%>
            return <%=EntityName%>;
        }

}
<%}-%>
