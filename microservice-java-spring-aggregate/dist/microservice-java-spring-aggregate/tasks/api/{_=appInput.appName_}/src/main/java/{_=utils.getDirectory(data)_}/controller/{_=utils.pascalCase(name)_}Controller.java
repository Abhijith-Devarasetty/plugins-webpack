package <%=utils.getPackageName(data)%>.controller;

import java.util.*;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.web.bind.annotation.*;

<%let capEntityName = utils.pascalCase(name)-%>
<%let EntityName = name-%>
<%let compositeKeys = utils.compositeKeys(attributes)-%>
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import <%=utils.getPackageName(data)%>.model.<%=capEntityName%>;
import <%=utils.getPackageName(data)%>.service.<%=capEntityName%>Service;
import io.swagger.annotations.ApiOperation;

@RequestMapping("/<%=capEntityName%>")
@RestController
public class <%=capEntityName%>Controller {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private <%=capEntityName%>Service service;
	
	@GetMapping("/getall")
	@ResponseBody
	public List<<%=capEntityName%>> get<%=capEntityName%>(){
		log.info("controller get all <%=capEntityName%> method");
		return service.getAll<%=capEntityName%>();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@PostMapping("/create")
	@ApiOperation(value = "Post <%=capEntityName%> api")
	@ResponseBody
	public <%=capEntityName%> add<%=capEntityName%>(@RequestBody <%=capEntityName%> <%=EntityName%>) {
		log.info("controller create <%=capEntityName%>");
		return service.add<%=capEntityName%>(<%=EntityName%>);
	}
<% -%>
<%if(utils.composite(attributes)){-%>

	@GetMapping("/getbyid/{id}")
	@ApiOperation(value = "Get <%=capEntityName%> by ID api")
	@ResponseBody
	public Optional<<%=capEntityName%>> get<%=capEntityName%>ById(<% -%>
<%let len = compositeKeys.length-%>
<%compositeKeys.forEach(e => {-%>
<%len = len-1-%>
  @RequestParam <%=utils.getDataType(e)%> <%=e.name%><%if(len!==0){%>,<%}%><% -%>
<%})-%>
  ) {
		<%=capEntityName%>Id <%=EntityName%>Id = new <%=capEntityName%>Id(<% -%>
<%len = compositeKeys.length-%>
<%compositeKeys.forEach(e => {-%>
<%len = len-1-%>
  <%=e.name%><%if(len!==0){%>,<%}%><% -%>
<%})-%>
  );
		log.info("controller find by Id method");
		return service.get<%=capEntityName%>ByCompositeId(<%=EntityName%>Id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@DeleteMapping("/delete/{id}")
	@ApiOperation(value = "Delete <%=capEntityName%> by ID api")
	@ResponseBody
	public Map<String, String> delete<%=capEntityName%>ById(<% -%>
<%len = compositeKeys.length-%>
<%compositeKeys.forEach(e => {-%>
<%len = len-1-%>
  @RequestParam <%=utils.getDataType(e)%> <%=e.name%><%if(len!==0){%>,<%}%><% -%>
<%})-%>
  ) {
		<%=capEntityName%>Id <%=EntityName%>Id = new <%=capEntityName%>Id(<% -%>
<%len = compositeKeys.length-%>
<%compositeKeys.forEach(e => {-%>
<%len = len-1-%>
  <%=e.name%><%if(len!==0){%>,<%}%><% -%>
<%})-%>
  );
		log.info("controller find all method");
		service.delete<%=capEntityName%>ByCompositeId(<%=EntityName%>Id);
		HashMap<String,String> res = new HashMap<String, String>();
        res.put("message","done");
		return  res;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@PutMapping("/update/{id}")
	@ApiOperation(value = "Update <%=capEntityName%> api")
	@ResponseBody
	public <%=capEntityName%> update<%=capEntityName%>(@RequestBody <%=capEntityName%> <%=EntityName%>,<% -%>
<%len = compositeKeys.length-%>
<%compositeKeys.forEach(e => {-%>
<%len = len-1-%>
  @RequestParam <%=utils.getDataType(e)%> <%=e.name%><%if(len!==0){%>,<%}%><% -%>
<%})-%>
  ){
		<%=capEntityName%>Id <%=EntityName%>Id = new <%=capEntityName%>Id(
<%len = compositeKeys.length-%>
<%compositeKeys.forEach(e => {-%>
<%len = len-1-%>
		<%=e.name%><%if(len!==0){%>,<%}%>
<%})-%>
  );
		log.info("controller update <%=capEntityName%> method");
		return service.update<%=capEntityName%>(<%=EntityName%>,<%=EntityName%>Id);
	}

<%}else{-%>
	
	@GetMapping("/getbyid/{id}")
	@ApiOperation(value = "Get <%=capEntityName%> by ID api")
	@ResponseBody
	public Optional<<%=capEntityName%>> get<%=capEntityName%>ById(@PathVariable <%=utils.getPrimaryDataType(attributes)%> id) {
		log.info("controller find by Id method");
		return service.get<%=capEntityName%>ById(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@DeleteMapping("/delete/{id}")
	@ApiOperation(value = "Delete <%=capEntityName%> by ID api")
	@ResponseBody
	public Map<String, String> delete<%=capEntityName%>ById(@PathVariable <%=utils.getPrimaryDataType(attributes)%> id) {
		log.info("controller find all method");
		service.delete<%=capEntityName%>ById(id);
		HashMap<String,String> res = new HashMap<String, String>();
        res.put("message","done");
		return  res;
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@PutMapping("/update/{id}")
	@ApiOperation(value = "Update <%=capEntityName%> api")
	@ResponseBody
	public <%=capEntityName%> update<%=capEntityName%>(@RequestBody <%=capEntityName%> <%=EntityName%>,@PathVariable <%=utils.getPrimaryDataType(attributes)%> id){
		log.info("controller update <%=capEntityName%> method");
		return service.update<%=capEntityName%>(<%=EntityName%>,id);
	}
<%}-%>

<%if(!utils.isEmpty(actions)){-%>
<%actions.forEach(a=>{-%>
<%let action = utils.getRestCall(a)-%>
<%let methLen = action['str'].length-%>
	@<%-action["met"]%>Mapping("<%-action['str']%>")
	@ResponseBody
	public <%=utils.getResponseEntity(a,utils.getRef)%> <%=utils.actionName(a)%>(<%-utils.getFinalParams(a)%>) {
		<%-utils.CheckAggregateDto(a,utils.getRef)%>
	}

<%})-%>
<%}%>
}

