package <%=utils.getPackageName(appInput.config)%>.composite;

import java.io.Serializable;
import java.util.Objects;
<% let entityName= utils.pascalCase(name);-%>
<%let compositeKeys = utils.compositeKeys(attributes)-%>

public class <%=entityName%>Id implements Serializable {

<%compositeKeys.forEach(e => {-%>
    private <%=utils.getDataType(e)%> <%=e.name%>;
<%})-%>

    public <%=entityName%>Id() {
    }

    public <%=entityName%>Id(
<%let len = compositeKeys.length-%>
<% compositeKeys.forEach(e=>{-%>
<%len = len-1-%>
		<%=utils.getDataType(e)%> <%=e.name%><%if(len!==0){%>,<%}%>
<%})-%>
	) {
<% compositeKeys.forEach(e=>{-%>
		this.<%=e.name%> = <%=e.name%>;
<%})-%>
	}

    @Override
    public int hashCode() {
        return Objects.hash(
<%len = compositeKeys.length%>
<%compositeKeys.forEach(e=>{-%>
<%len = len-1-%>
                <%=e.name%><%if(len!==0){%>,<%}%>
<%})-%>
            );
    }
}
