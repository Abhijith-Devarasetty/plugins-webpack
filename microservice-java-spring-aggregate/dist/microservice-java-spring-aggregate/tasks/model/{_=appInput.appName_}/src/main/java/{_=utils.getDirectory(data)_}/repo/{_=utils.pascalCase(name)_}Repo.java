package <%=utils.getPackageName(data)%>.repo;
<%let capEntityName = utils.getCapEntityName(data)-%>
<%let EntityName = utils.getEntityName(data)-%>
<%if(data.project.application.database.dbName != "mongo"){-%>
import org.springframework.data.jpa.repository.JpaRepository;
<%}else{-%>
import org.springframework.data.mongodb.repository.MongoRepository;
<%}-%>
import org.springframework.stereotype.Repository;
<%if(utils.composite(attributes)){-%>
import <%=utils.getPackageName(data)%>.composite.<%=utils.pascalCase(name)%>Id;
<%}-%>
import <%=utils.getPackageName(data)%>.model.<%=utils.pascalCase(name)%>;

@Repository
public interface <%=utils.pascalCase(name)%>Repo extends <%=utils.getRepository(data)%><<%=utils.pascalCase(name)%>, <%=utils.repoPrimary(attributes,name)%>>{

<%=utils.getQuery(data,name,relations)%>

}
