package <%=utils.getPackageName(data)%>.model;

<% let n= utils.pascalCase(name);-%>
<% let entityName = name;-%>

public class <%=n%> {

<%attributes.forEach(e=>{-%>
<%if(e.primaryKey == 'true'){-%>
	@Id
<%}-%>
<%if(e.dataType == "blob" | e.dataType == "clob"){-%>
	@Lob
<%}-%>
	private <%=utils.getDataType(e)%> <%=e.name%>;

	public <%=utils.getDataType(e)%> get<%=utils.pascalCase(e.name)%>(){
		return <%=e.name%>;
	}

	public void set<%=utils.pascalCase(e.name)%>(<%=utils.getDataType(e)%> <%=e.name%>){
		this.<%=e.name%> = <%=e.name%>;
	}
<%})-%>

	public <%=n%>() {  }

	public <%=n%>(
<%let len = attributes.length-%>
<% attributes.forEach(e=>{-%>
<%len = len-1-%>
		<%=utils.getDataType(e)%> <%=e.name%><%if(len!==0){%>,<%}%>
<%})-%>
	) {
<% attributes.forEach(e=>{-%>
		this.<%=e.name%> = <%=e.name%>;
<%})-%>
	}

}
