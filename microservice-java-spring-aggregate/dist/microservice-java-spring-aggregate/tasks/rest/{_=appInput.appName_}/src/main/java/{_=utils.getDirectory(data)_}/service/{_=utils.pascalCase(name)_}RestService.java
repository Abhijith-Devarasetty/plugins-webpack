package <%=utils.getPackageName(data)%>.service;

<%let capEntityName = utils.pascalCase(name)-%>
<%let EntityName = name-%>

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import <%=utils.getPackageName(data)%>.model.<%=capEntityName%>;

@Service
public class <%=capEntityName%>RestService {

    private static final Logger logger = LoggerFactory.getLogger(<%=capEntityName%>RestService.class);

    @Autowired
	private RestTemplate restTemplate;

<%config.routes.forEach(r => {-%>
<%let action = utils.getRestCall(r)-%>

    public <%-utils.getResponseEntity(r,utils.getRef)%>  <%-utils.actionName(r,utils.getRef)%>(<%-utils.getRouteParams(r)%>){
        ResponseEntity<<%-utils.getResponseEntity(r,utils.getRef)%>> responseEntity = restTemplate.exchange(<%-utils.getRouteUrl(r)%>, HttpMethod.<%-utils.toUpper(action['met'])%>, null, new ParameterizedTypeReference<<%-utils.getResponseEntity(r,utils.getRef)%>>() { });
        <%-utils.getResponseEntity(r,utils.getRef)%> result = responseEntity.getBody();
        return result;
    }
<%})-%>

}
