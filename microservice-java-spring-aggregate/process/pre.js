const { processData } = require("./processdata.js");
let jsonata = require("jsonata");
const {processYaml} = require("./swaggerParser.js");

module.exports.run = (inputData, lookup, tasks , utils ) => {
  
  let temp = {
    applications:[
      inputData
    ]
  }


  // let tasksData = jsonata("${**.name : $.[**.name, **.depends]}").evaluate(
  //   tasks
  // );
  // console.log("from pre processor" + JSON.stringify(tasksData));
  // let entities = jsonata("**.entities{**.taskId: [ **.entity ]}").evaluate(
  //   lookup
  // );
  // console.log("from pre processor" + JSON.stringify(entities));
  console.log("input data",JSON.stringify(inputData));
  let processedData = processData(temp,utils.getRef);
  console.log("processed data",JSON.stringify(processedData));
  tasks["model"] = processedData.project.entities.filter(i => i.taskId==="model");
  tasks["dao"] = processedData.project.entities.filter(i => i.taskId==="dao");
  tasks["dto"] = processedData.project.entities.filter(i => i.taskId==="dto");
  tasks["composite"] = processedData.project.entities.filter(function(i){
    if(jsonata("$count(*[primaryKey ='true'])>1").evaluate(i.schema)){
      return i;
    }
  });

  if(processedData.project.application.bpmn !== undefined){
    tasks["bpmn"] = processedData.project.application.bpmn;
  }

  if(processedData.project.application.dmn !== undefined){
    tasks["dmn"] = processedData.project.application.dmn;
  }
  
  // if(inputData.config.apigen!==undefined){
  //   let path = inputData.config.apigen.openapi.file;   
  //   let swaggerData = processYaml(path);
  //   tasks["model"]=[...tasks["model"],...swaggerData.entities];
  //   if(processedData.project.application.action!==undefined){
  //     processedData.project.application.action = [...processedData.project.application.action, ...swaggerData.actions]
  //   }else{
  //     processedData.project.application["actions"] = [...swaggerData.actions];
  //   }
    
  // }

  tasks["aggregatedto"] = processedData.project.entities.filter(i => i.taskId==="aggregatedto");


  let apis = [];
  if(processedData.project.api !== undefined){
    processedData.project.api.forEach( a=>{
      let temp1 = tasks.model.filter(m => m.name === utils.getRef(a.targetEntity).entity)[0];
      temp1["actions"] = a.actions;
      if(a.openapi !== undefined){
        let path = a.openapi.file;
        let swaggerData = processYaml(path);
        tasks["model"]=[...tasks["model"],...swaggerData.entities];
        if(temp1.actions!==undefined){
          temp1.actions = [...temp1.actions, ...swaggerData.actions]
        }else{
          temp1["actions"] = [...swaggerData.actions];
        }
      }
      apis.push(temp1);
    })
  }
  tasks["api"] = apis;

  
  let camel = [];
  let restTempl = [];
  if(processedData.project.middleware !== undefined){
    let middleware = processedData.project.middleware;
    middleware.repo.forEach(r => {
      let temp =  tasks.model.filter(e => e.name === utils.getRef(r.repoEntity).entity);
      if(temp.length !== 0){
        r.repoEntity = tasks.model.filter(e => e.name === utils.getRef(r.repoEntity).entity)[0];
      }else{
        r.repoEntity = tasks.dto.filter(e => e.name === utils.getRef(r.repoEntity).entity)[0];
      }
      r["name"] = r.repoEntity.name;
      if(r.repository === "camel"){
        camel.push(r);
      }
      if(r.repository === "restTemplate"){
        restTempl.push(r);
      }
    })
  }
  tasks["camel"] = camel;
  tasks["rest"] = restTempl;

  if(tasks["camel"].length === 0){
    if(tasks["rest"].length === 0){
      processedData.project.repository = '';
    }
    if(tasks["rest"].length !== 0){
      processedData.project.repository = 'rest';
    }
  }else{
    if(tasks["camel"].length !== 0){
      processedData.project.repository = 'apache';
    }
  }


  // console.log("tasks%%%%%%%%%%%%%%", JSON.stringify(tasks["rest"]));
  return { tasks, data: processedData};
};
