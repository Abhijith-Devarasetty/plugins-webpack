const _ = require("lodash");
const path = require("path");
function getName(name) {
  return _.capitalize(name);
}
function getPackageName(framework) {
  let p = framework.replace(/\./g, "//");
  return path.normalize(p);
}

function getTracing(tracing){
  let result='';
  let trace = Object.keys(tracing);
  switch (trace[0]) {
    case "zipkin":
      result = `
      @Bean
      @LoadBalanced
      public RestTemplate getRestTemplate() {
        return new RestTemplate();
      }
      
      @Bean
      public Sampler defaultSampler() {
        return Sampler.ALWAYS_SAMPLE;
      }`      
      break;
  
    default:
      break;
  }
  return result;
}


module.exports = { getName, getPackageName, getTracing };
