package <%=utils.getPackageName(data)%>.model;

import java.util.*;
import java.sql.*;
<%=utils.getImports(attributes)%>

<%if(data.project.application.database.dbName !== "mongo"){-%>
import javax.persistence.*;
<%}else{-%>
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
<%}-%>

<% let n= utils.pascalCase(name);-%>
<% let entityName = name;-%>


public class <%=n%> {

<%attributes.forEach(e=>{-%>

	private <%=utils.getDataType(e)%> <%=e.name%>;

	public <%=utils.getDataType(e)%> get<%=utils.pascalCase(e.name)%>(){
		return <%=e.name%>;
	}

	public void set<%=utils.pascalCase(e.name)%>(<%=utils.getDataType(e)%> <%=e.name%>){
		this.<%=e.name%> = <%=e.name%>;
	}

<%if(e.Enum!==undefined){-%>
	enum <%=e.Enum%>{
<%let len = e.values.length-%>
<%e.values.forEach(v=>{-%>
<%len = len-1-%>
		<%=v%><%if(len!==0){%>,<%}%>
<%})-%>
	}
<%}-%>
<%})-%>

	public <%=n%>() {  }

	public <%=n%>(
<%let len = attributes.length-%>
<% attributes.forEach(e=>{-%>
<%len = len-1-%>
		<%=utils.getDataType(e)%> <%=e.name%><%if(len!==0){%>,<%}%>
<%})-%>
	) {
<% attributes.forEach(e=>{-%>
		this.<%=e.name%> = <%=e.name%>;
<%})-%>
	}
}
