package <%=utils.getPackageName(data)%>.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

<%let capEntityName = utils.pascalCase(name)-%>
<%let EntityName = name-%>

import <%=utils.getPackageName(data)%>.model.<%=capEntityName%>;
import <%=utils.getPackageName(data)%>.repo.<%=capEntityName%>Repo;

@RunWith(SpringRunner.class)
@SpringBootTest
public class <%=capEntityName%>ServiceTest {
	
	@Autowired
	private <%=capEntityName%>Service service;
	
	@MockBean
	private <%=capEntityName%>Repo repo;
	

	@Test
	public void testAdd<%=capEntityName%>(){
		<%=capEntityName%> <%=EntityName%> = new <%=capEntityName%>();
		<%-utils.getAttributesData(attributes,EntityName)%>
		
	    Mockito.when(repo.save(<%=EntityName%>)).thenReturn(<%=EntityName%>);
	    
	    assertThat(service.add<%=capEntityName%>(<%=EntityName%>)).isEqualTo(<%=EntityName%>);
	}

	
	@Test
	public void testGet<%=capEntityName%>ById(){
		<%=capEntityName%> <%=EntityName%> = new <%=capEntityName%>();
		<%-utils.getAttributesData(attributes,EntityName)%>
		
	    Mockito.when(repo.findById(1)).thenReturn(Optional.of(<%=EntityName%>));

	    assertThat(service.get<%=capEntityName%>ById(1)).isEqualTo(Optional.of(<%=EntityName%>));
	}

	
	@Test
	public void testGetAll<%=capEntityName%>(){
		<%=capEntityName%> <%=EntityName%>1 = new <%=capEntityName%>();
		<%-utils.getAttributesData(attributes,EntityName+"1")%>

		<%=capEntityName%> <%=EntityName%>2 = new <%=capEntityName%>();
		<%-utils.getAttributesData(attributes,EntityName+"2")%>
		
		List<<%=capEntityName%>> <%=EntityName%>List = new ArrayList<>();
		<%=EntityName%>List.add(<%=EntityName%>1);
		<%=EntityName%>List.add(<%=EntityName%>2);
		
		Mockito.when(repo.findAll()).thenReturn(<%=EntityName%>List);
		
		assertThat(service.getAll<%=capEntityName%>()).isEqualTo(<%=EntityName%>List);
	}
	

	@Test
	public void testDelete<%=capEntityName%>ById(){
		<%=capEntityName%> <%=EntityName%> = new <%=capEntityName%>();
		<%-utils.getAttributesData(attributes,EntityName)%>
		
	    Mockito.when(repo.findById(1)).thenReturn(Optional.of(<%=EntityName%>));
		Mockito.when(repo.existsById(<%=EntityName%>.get<%=utils.getPrimary(attributes)%>())).thenReturn(false);
		
	    assertFalse(repo.existsById(<%=EntityName%>.get<%=utils.getPrimary(attributes)%>()));
	}

    
	@Test
	public void testUpdate<%=capEntityName%>() {
		<%=capEntityName%> <%=EntityName%> = new <%=capEntityName%>();
		<%-utils.getAttributesData(attributes,EntityName)%>
		
		Mockito.when(repo.findById(1)).thenReturn(Optional.of(<%=EntityName%>));
		
		// TO Do...
		// Update the <%=capEntityName%> Object
		// <%=capEntityName%>.setFirstName("first1");
		Mockito.when(repo.save(<%=EntityName%>)).thenReturn(<%=EntityName%>);
		assertThat(service.update<%=capEntityName%>(<%=EntityName%>,1)).isEqualTo(<%=EntityName%>);
	}
}
