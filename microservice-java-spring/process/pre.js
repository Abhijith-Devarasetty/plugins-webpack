const { processData } = require("./processdata.js");
let jsonata = require("jsonata");
const {processYaml} = require("./swaggerParser.js");

module.exports.run = (inputData, lookup, tasks , utils ) => {
  
  let temp = {
    applications:[
      inputData
    ]
  }


  // let tasksData = jsonata("${**.name : $.[**.name, **.depends]}").evaluate(
  //   tasks
  // );
  // console.log("from pre processor" + JSON.stringify(tasksData));
  // let entities = jsonata("**.entities{**.taskId: [ **.entity ]}").evaluate(
  //   lookup
  // );
  // console.log("from pre processor" + JSON.stringify(entities));
  console.log("input data",JSON.stringify(inputData));
  let processedData;
  try{
    processedData = processData(temp,utils.getRef);
  }
  catch(exception){
    console.log(exception);
  }
  console.log("processed data",JSON.stringify(processedData));
  tasks["model"] = processedData.project.entities.filter(i => i.taskId==="model");
  tasks["dao"] = processedData.project.entities.filter(i => i.taskId==="dao");
  tasks["dto"] = processedData.project.entities.filter(i => i.taskId==="dto");
  tasks["composite"] = processedData.project.entities.filter(function(i){
    if(jsonata("$count(*[primaryKey ='true'])>1").evaluate(i.schema)){
      return i;
    }
  });

  if(processedData.project.application.bpmn !== undefined){
    tasks["bpmn"] = processedData.project.application.bpmn;
  }

  if(processedData.project.application.dmn !== undefined){
    tasks["dmn"] = processedData.project.application.dmn;
  }
  

  let apis = [];
  if(processedData.project.api !== undefined){
    processedData.project.api.forEach( a=>{
      let temp1 = tasks.model.filter(m => m.name === utils.getRef(a.targetEntity).entity)[0];
      temp1["actions"] = a.actions;
      if(a.openapi !== undefined){
        let path = a.openapi.file;
        let swaggerData = processYaml(path);
        tasks["model"]=[...tasks["model"],...swaggerData.entities];
        if(temp1.actions!==undefined){
          temp1.actions = [...temp1.actions, ...swaggerData.actions]
        }else{
          temp1["actions"] = [...swaggerData.actions];
        }
      }
      apis.push(temp1);
    })
  }
  tasks["api"] = apis;

  // tasks["resEntity"] = [];
  // if(processedData.project.resources.entities !== undefined){
  //   processedData.project.resources.entities.forEach(e => {
  //     let temp = processedData.project.entities.filter(f =>  f.name === e.entity )[0];
  //     temp["path"] = e.path;
  //     temp.taskId = "resEntity";
  //     tasks["resEntity"].push(temp);
  //   });
  //   // console.log("$$$$$$$$$$$$",tasks["resEntity"] );  
  // }
  
  // console.log("tasks model", tasks);
  return { tasks, data: processedData};
};
