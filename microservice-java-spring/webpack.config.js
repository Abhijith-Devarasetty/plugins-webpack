const CopyPlugin = require("copy-webpack-plugin");
const path = require("path");
const fs = require("fs");

const manifest = JSON.parse(
  fs.readFileSync(path.join(__dirname, "manifest.json"), "utf8")
);
const { pre, tasksConfig, main, templateId } = manifest;

module.exports = {
  entry: { "process/pre": path.resolve(__dirname, pre), index: "./index.js" },
  output: {
    path: path.resolve(__dirname, "dist", templateId),
    filename: `[name].js`,
    libraryTarget: "umd",
  },
  node: {
    __dirname: false,
    __filename: false,
  },
  target: "electron-main",

  plugins: [
    new CopyPlugin({
      patterns: [
        { from: main, to: main },
        { from: path.join(tasksConfig), to: path.join(tasksConfig) },
        { from: "template", to: "template" },
        { from: "template.defaults", to: "template.defaults" },
        { from: "tasks", to: "tasks" },
        { from: "validator.json", to: "validator.json" },
        {
          from: "manifest.json",
          to: "manifest.json",
        },
      ],
    }),
  ],
};
