const { hashElement } = require("folder-hash");
const { zip } = require("zip-a-folder");
const path = require("path");
const fs = require("fs");

const manifest = JSON.parse(
  fs.readFileSync(path.join(process.cwd(), "./manifest.json"), "utf8")
);
const { templateId } = manifest;

hashElement(path.join(process.cwd(), "dist", templateId), "", {
  files: {
    exclude: ["manifest.json"],
    ignoreRootName: true,
  },
})
  .then((hash) => {
    fs.writeFileSync(
      path.join(process.cwd(), "./dist", templateId, "./manifest.json"),
      JSON.stringify({
        ...manifest,
        templateHash: hash.children.find((i) => i.name === "template").hash,
        processHash: hash.children.find((i) => i.name === "process").hash,
        buildDate: new Date().toISOString(),
      })
    );
    zipFolder();
  })
  .catch((error) => {
    return console.error("hashing failed:", error);
  });

const zipFolder = async () => {
  fs.existsSync("release") || fs.mkdirSync("release");
  await zip(
    path.join(process.cwd(), "dist"),
    path.join(process.cwd(), "release", `${manifest.templateId}.ftar`)
  );
};
