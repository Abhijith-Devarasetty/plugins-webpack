const schema = [
<%let len = data.project.entities.length-%>
<%data.project.entities.forEach(e =>{-%>
<%len = len - 1 -%>
  {
    name: "<%=e.name%>",
    path: "<%=e.name%>",
    objCount : 10,
    schema: {
<%e.attributes.forEach(a=>{-%>
      <%=a.name%>: "{{<%=a.fakeMeta%>}}",
<%})-%>
    },
  }<%if(len!==0){%>,<%}%>
<%})-%>
];

module.exports = {schema}
