
   
  let fakerSchema = require("./fakeSchema.js")
  const fs = require('fs');
  var faker = require("faker");
  var _ = require("lodash");
  


 let fakerObj = fakerSchema.schema.reduce(function(obj, entityObj) {
      obj = Array.from({ length: entityObj.objCount }).map(() => Object.keys(entityObj.schema).reduce((entity, key) => {
          entity[key] = faker.fake(entityObj.schema[key])
          return entity
        }, {}))
        console.log(obj);
        fs.writeFile("./data/"+entityObj.name+".json", JSON.stringify(obj,undefined,2), (err) => {
          if (err) throw err;
          console.log('The file has been saved!');
        });
      return obj;
  }, {});


