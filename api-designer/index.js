const _ = require("lodash");

_.mixin({ pascalcase: _.flow(_.camelCase, _.upperFirst) });

function pascalCase(input) {
  return _.pascalcase(input);
}


module.exports = {
  pascalCase
};
