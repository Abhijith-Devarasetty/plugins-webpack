const { processData } = require("./processdata.js");

module.exports.run = (inputData, lookup, tasks , utils ) => {
  
  let temp = {
    applications:[
      inputData
    ]
  }

  console.log("input data",JSON.stringify(inputData));
  let processedData = processData(temp,utils.getRef);
  console.log("processed data",JSON.stringify(processedData));

  

  // console.log("tasks model", tasks);
  return { tasks, data: processedData};
};
