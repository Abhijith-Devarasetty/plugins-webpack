let jsonata = require("jsonata");

function processData(input,getRef) {
  let framework = jsonata("**.framework.($keys())").evaluate(input);
  let output = {
    project: {
      application: processApplications(input.applications),
      entities: processEntities(input.applications,framework,getRef),
      components: processComponents(input.applications,framework,getRef),
      resources: processResources(input.applications,getRef)
    },
  };
  return output;
}

function processApplications(applications) {
  if (applications.length > 0) {
    // let application = [];
    let value;
    applications.forEach((app) => {
      let appName = jsonata("$.appName").evaluate(app);
      value = {
        appName: appName,
        templateId: app.templateId,
        framework: jsonata("$.framework").evaluate(app),
        package: jsonata("**.package_name").evaluate(app),
        port: jsonata("**.port").evaluate(app),
        database: {
          url: jsonata("**.database.**.url").evaluate(app),
          dbName: jsonata("**.database.($keys())").evaluate(app),
          username: "",
          password: "",
        },
        // action: jsonata("**.actions").evaluate(app),
        apigen: jsonata("**.apigen").evaluate(app),
        tracing: jsonata("**.tracing").evaluate(app),
        logging: jsonata("**.logging").evaluate(app),
        discovery: jsonata("**.discovery").evaluate(app),
        entities: jsonata("$.**.entities.entity").evaluate(app),
        components: jsonata("$.**.components.componentName").evaluate(app),
        bpmn: jsonata("$.**.bpmnConfig").evaluate(app),
        dmn: jsonata("$.**.dmnConfig").evaluate(app),
      };
    });
    return value;
  } else {
    return {};
  }
}

function processEntities(applications,framework,getRef) {
  if (jsonata("**.entities").evaluate(applications) != undefined) {
    let entityArray = [];
    let entities = jsonata("**.entities").evaluate(applications);
    entities.forEach((entity) => {
      entity = getRef(entity);
      let tName = '';
      if(jsonata("**.tableName").evaluate(entity)!==undefined){
        tName = jsonata("**.tableName").evaluate(entity);
      }
      let value = {
        name: entity.entity,
        isCompositeKey: jsonata("$count(schema.meta[pk ='true'])>1").evaluate(
          entity
        ),
        tableName: tName,
        taskId: jsonata("**.taskId").evaluate(entity),
        attributes: processAttributes(entity.schema),
        relations: jsonata("**.relations").evaluate(entity),
      };
      // console.log("entity",value);
      
      entityArray.push( value );
    });
    return entityArray;
  } else {
    return [];
  }
}

function processAttributes(schema) {
  let result = [];
  schema.forEach((ele) => {
    result.push({
      name: ele.attribute,
      dataType: ele.dataType,
      actualDataType : ele.meta.dataType != undefined ? ele.meta.dataType : "",
      fakeMeta: ele.meta.fake,
      primaryKey: ele.meta.pk ? "true" : "fasle"
    });
  });
  return result;
}

function processComponents(applications, framework, getRef) {
  if (jsonata("**.components").evaluate(applications) != undefined) {
    let components = jsonata("**.components").evaluate(applications);
    let arr = [];
    console.log("######",components);
    components.forEach((comp) => {
      comp = getRef(comp);
      console.log("######",comp);
      if (comp.taskId === "TabLayoutForm") {
        let appName = jsonata("$.componentName").evaluate(comp);
        let value = {
          componentName: appName,
          tabLayoutData: processTabElements(comp),
          taskId: "TabLayoutForm",
          layout: jsonata("$.layout").evaluate(comp.componentStructure)
        };
        arr.push(value);
      } else {
        let appName = jsonata("$.componentName").evaluate(comp);
        let value = {
          name: appName,
          fields: processFields(comp),
          taskId: "form"
        };
        arr.push({ [appName]: value });
      }
    });
    return arr;
  } else {
    return [];
  }
}

function processTabElements(comp) {
  if(comp.componentStructure.UiElementJson !== undefined){
    return processUiElementJson(comp.componentStructure.UiElementJson.file);
  }
  elements = jsonata("**.elements").evaluate(comp);
  let tempTabs = [];
  let sectionNames = [];
  let tabs = [];
  let temp;
  let tabNames = [];
  elements.forEach((e) => {
    if (!tabNames.includes(e.tab)) {
      tabNames.push(e.tab);
    }
  });

  elements.forEach(e =>{
    tempTabs[e.tab] = elements.filter(i => i.tab === e.tab);
  });

  tabNames.forEach(n => {
    let sections = [];
    tempTabs[n].forEach(t => {
      if(!sectionNames.includes(t.section)){
        sectionNames.push(t.section);
      }
    })
    sectionNames.forEach(s => {
      temp = tempTabs[n].filter(i => i.section === s);
      sections.push({"sectionName":s,elements:temp});
    })
    sectionNames = [];
    tabs.push({"tabName":n,"sections":sections});
  })
  
  return tabs;
}

function processFields(comp, framework) {
  let arr = [];
  comp = jsonata("**.elements").evaluate(comp);
  comp.forEach((field) => {
    let temp = {
      element: field.type,
      label: field.label,
      name: field.name,
      model: field.model,
    };
    if (field.options !== undefined) {
      temp["options"] = field.options;
    }
    if (field.option_action !== undefined) {
      temp["option_action"] = field.option_action;
    }
    arr.push(temp);
  });
  return arr;
}


function processEnums(input) {}

function processResources(application,getRef) {
  if(jsonata("**.resources").evaluate(application) !== undefined){
    let resources = jsonata("**.resources").evaluate(application);
    let entities = [];
    let components = [];
    resources.forEach(r => {
      resource = getRef(r);
      if(resource.config.entities !== undefined){
        resource.config.entities.forEach(e =>{
          let temp = getRef(e);
          temp["path"] = resource.path;
          entities.push(temp);
        });
      }
      if(resource.config.components !== undefined){
        resource.config.components.forEach(c => {
          let temp = getRef(c);
          temp["path"] = resource.path;
          components.push(temp);
        })
      }
      // console.log("##########",entities,components);
    });
    return {
      "entities":entities,
      "components":components
    }
  }
}

module.exports = { processData };
