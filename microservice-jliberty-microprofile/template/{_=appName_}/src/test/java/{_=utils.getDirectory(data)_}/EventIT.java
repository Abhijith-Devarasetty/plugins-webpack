package it.io.openliberty.guides.event;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashMap;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.Response;

import io.openliberty.guides.event.models.Event;

public abstract class EventIT {

    private WebTarget webTarget;

    protected Form form;
    protected Client client;
    protected Response response;
    protected HashMap<String, String> eventForm;

    protected static String baseUrl;
    protected static String port;
    protected static final String EVENTS = "events";

    protected int postRequest(HashMap<String, String> formDataMap) {
        formDataMap.forEach((formField, data) -> {
            form.param(formField, data);
        });
        webTarget = client.target(baseUrl + EVENTS);
        response = webTarget.request().post(Entity.form(form));
        form = new Form();
        return response.getStatus();
    }

    protected int updateRequest(HashMap<String, String> formDataMap, int eventId) {
        formDataMap.forEach((formField, data) -> {
            form.param(formField, data);
        });
        webTarget = client.target(baseUrl + EVENTS + "/" + eventId);
        response = webTarget.request().put(Entity.form(form));
        form = new Form();
        return response.getStatus();
    }
    
    protected int deleteRequest(int eventId) {
        webTarget = client.target(baseUrl + EVENTS + "/" + eventId);
        response = webTarget.request().delete();
        return response.getStatus();
    }

    protected JsonArray getRequest() {
        webTarget = client.target(baseUrl + EVENTS);
        response = webTarget.request().get();
        return response.readEntity(JsonArray.class);
    }

    protected JsonObject getIndividualEvent(int eventId) {
        webTarget = client.target(baseUrl + EVENTS + "/" + eventId);
        response = webTarget.request().get();
        return response.readEntity(JsonObject.class);
    }
    
    protected JsonObject findEvent(Event e) {
        JsonArray events = getRequest();
        for (int i = 0; i < events.size(); i++) {
            JsonObject testEvent = events.getJsonObject(i);
            Event test = new Event(testEvent.getString("name"),
                    testEvent.getString("location"), testEvent.getString("time"));
            if (test.equals(e)) {
                return testEvent;
            }
        }
        return null;
    }

    protected void assertData(JsonObject event, String name, String loc, String date) {
        assertEquals(event.getString("name"), name);
        assertEquals(event.getString("location"), loc);
        assertEquals(event.getString("time"), date);
    }

}