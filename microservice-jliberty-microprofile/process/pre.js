const { processData } = require("./processdata.js");
let jsonata = require("jsonata");
const {processYaml} = require("./swaggerParser.js");

module.exports.run = (inputData, lookup, tasks , utils ) => {
  
  let temp = {
    applications:[
      inputData
    ]
  }


  // let tasksData = jsonata("${**.name : $.[**.name, **.depends]}").evaluate(
  //   tasks
  // );
  // console.log("from pre processor" + JSON.stringify(tasksData));
  // let entities = jsonata("**.entities{**.taskId: [ **.entity ]}").evaluate(
  //   lookup
  // );
  // console.log("from pre processor" + JSON.stringify(entities));
  console.log("input data",JSON.stringify(inputData));
  let processeDate = processData(temp,utils.getRef);
  console.log("processed data",JSON.stringify(processeDate));
  tasks["models"] = processeDate.project.entities.filter(i => i.taskId==="model");
  // tasks["dao"] = processeDate.project.entities.filter(i => i.taskId==="dao");
  // tasks["dto"] = processeDate.project.entities.filter(i => i.taskId==="dto");
  // tasks["composite"] = processeDate.project.entities.filter(function(i){
  //   if(jsonata("$count(*[primaryKey ='true'])>1").evaluate(i.schema)){
  //     return i;
  //   }
  // })


  // if(inputData.config.apigen!==undefined){
  //   let path = inputData.config.apigen.openapi.file;   
  //   let swaggerData = processYaml(path);
  //   tasks["model"]=[...tasks["model"],...swaggerData.entities];
  //   if(processeDate.project.application.action!==undefined){
  //     processeDate.project.application.action = [...processeDate.project.application.action, ...swaggerData.actions]
  //   }else{
  //     processeDate.project.application["actions"] = [...swaggerData.actions];
  //   }
    
  // }

  // console.log("tasks model", tasks);
  return { tasks, data: processeDate};
};
