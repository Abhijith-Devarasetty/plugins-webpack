# Microservices
The FDL (FAST Declartive language) is declartive configuration management to generate code from FAST templates by providng options  all your projects, applications, entities  in a single file or multiple files with a user-friendly syntax.

FAST editor is powerful catalyst for the FDL providing the intellsence like codelens for quick reference of the documentation and default values 

project is root configuration which encapsulates the mutiple code generation modules like the workspace

application refers to the module with refer the template at parent level and all the different configuration options for the framework

entity refers the domain object for the application 

component refers the user interface widgets with layout controlled through the templates

# Inputs Example 

    application #microservice-jLiberty-microprofile ExampleService{   
        framework @microprofile(
            package_name "com.company.app",
            port 9080,    
            entities []
        ),    
        database @derby
    }
