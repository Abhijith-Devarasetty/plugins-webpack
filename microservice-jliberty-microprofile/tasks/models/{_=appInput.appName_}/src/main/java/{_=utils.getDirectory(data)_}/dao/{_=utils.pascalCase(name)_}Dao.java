package <%=utils.getPackageName(data)%>.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

<% let capEntityName= utils.pascalCase(name);-%>
<% let entityName = name;-%>

import <%=utils.getPackageName(data)%>.models.<%=capEntityName%>;

import javax.enterprise.context.RequestScoped;

@RequestScoped
public class <%=capEntityName%>Dao {

    @PersistenceContext(name = "jpa-unit")
    private EntityManager em;

    public void createEvent(<%=capEntityName%> <%=entityName%>) {
        em.persist(<%=entityName%>);
    }

    public <%=capEntityName%> readEvent(int id) {
        return em.find(<%=capEntityName%>.class, id);
    }

    public void updateEvent(<%=capEntityName%> <%=entityName%>) {
        em.merge(<%=entityName%>);
    }

    public void deleteEvent(<%=capEntityName%> <%=entityName%>) {
        em.remove(<%=entityName%>);
    }

    public List<<%=capEntityName%>> readAllEvents() {
        return em.createNamedQuery("<%=capEntityName%>.findAll", <%=capEntityName%>.class).getResultList();
    }

    public List<<%=capEntityName%>> readEvent(
<%let len = attributes.length-%>
<% attributes.forEach(e=>{-%>
<%len = len-1-%>
		<%=utils.getDataType(e)%> <%=e.name%><%if(len!==0){%>,<%}%>
<%})-%>
	) {
        return em.createNamedQuery("<%=capEntityName%>.findEvent", <%=capEntityName%>.class)
<%len = attributes.length-%>
<%attributes.forEach(e => {-%>
<%len = len-1-%>
            .setParameter("<%=e.name%>", <%=e.name%>)<%if(len===0){%>.getResultList();<%}%>
<%})-%>
    }
}
