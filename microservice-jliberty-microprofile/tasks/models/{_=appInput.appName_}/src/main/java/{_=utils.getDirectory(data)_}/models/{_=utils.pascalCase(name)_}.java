package <%=utils.getPackageName(data)%>.models;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.NamedQuery;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.GenerationType;

<% let capEntityName= utils.pascalCase(name);-%>
<% let entityName = name;-%>

@Entity
@Table(name = "<%=capEntityName%>")
@NamedQuery(name = "<%=capEntityName%>.findAll", query = "SELECT e FROM <%=capEntityName%> e")
@NamedQuery(name = "<%=capEntityName%>.find<%=capEntityName%>", query = "SELECT e FROM <%=capEntityName%> e WHERE "
    + "<%-utils.getNamedQuery(attributes)%>")
public class <%=capEntityName%> implements Serializable {

    public <%=capEntityName%>() {
    }

    public <%=capEntityName%>(
<%let len = attributes.length-%>
<% attributes.forEach(e=>{-%>
<%len = len-1-%>
		<%=utils.getDataType(e)%> <%=e.name%><%if(len!==0){%>,<%}%>
<%})-%>
	) {
<% attributes.forEach(e=>{-%>
		this.<%=e.name%> = <%=e.name%>;
<%})-%>
    }
    
<%attributes.forEach(e=>{-%>
<%if(e.primaryKey == 'true'){-%>
	@Id
<%}-%>
<%if(e.dataType == "blob" | e.dataType == "clob"){-%>
	@Lob
<%}-%>
<%if(e.columnName != undefined && e.columnName != ""){-%>
	@Column(name="<%=e.columnName%>")
<%}-%>
	private <%=utils.getDataType(e)%> <%=e.name%>;

	public <%=utils.getDataType(e)%> get<%=utils.pascalCase(e.name)%>(){
		return <%=e.name%>;
	}

	public void set<%=utils.pascalCase(e.name)%>(<%=utils.getDataType(e)%> <%=e.name%>){
		this.<%=e.name%> = <%=e.name%>;
	}

<%})-%>

}
