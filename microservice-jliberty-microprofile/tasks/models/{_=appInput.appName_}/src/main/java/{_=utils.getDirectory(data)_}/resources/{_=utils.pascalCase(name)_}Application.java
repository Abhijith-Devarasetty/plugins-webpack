package <%=utils.getPackageName(data)%>.resources;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

<% let capEntityName= utils.pascalCase(name);-%>
<% let entityName = name;-%>

@ApplicationPath("/")
public class <%=capEntityName%>Application extends Application {

}
