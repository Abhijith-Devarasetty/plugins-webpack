package <%=utils.getPackageName(data)%>.resources;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.POST;
import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

<% let capEntityName= utils.pascalCase(name);-%>
<% let entityName = name;-%>

import <%=utils.getPackageName(data)%>.dao.<%=capEntityName%>Dao;
import <%=utils.getPackageName(data)%>.models.<%=capEntityName%>;

@RequestScoped
@Path("<%=entityName%>")
public class <%=capEntityName%>Resource {

    @Inject
    private <%=capEntityName%>Dao dao;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Response addNewEvent(<%=capEntityName%> <%=entityName%>) {
        dao.createEvent(<%=entityName%>);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Transactional
    public Response updateEvent(<%=capEntityName%> <%=entityName%>,@PathParam("id") int id) {
        <%=capEntityName%> o = dao.readEvent(id);
        if(o == null) {
            return Response.status(Response.Status.NOT_FOUND)
                           .entity("<%=capEntityName%> does not exist").build();
        }
<%attributes.forEach(e=>{-%>
        o.set<%utils.pascalCase(e.name)%>(<%=entityName%>.<%=e.name%>)
<%})-%>
        dao.updateEvent(o);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @DELETE
    @Path("{id}")
    @Transactional
    public Response deleteEvent(@PathParam("id") int id) {
        <%=capEntityName%> <%=entityName%> = dao.readEvent(id);
        if(<%=entityName%> == null) {
            return Response.status(Response.Status.NOT_FOUND)
                           .entity("<%=capEntityName%> does not exist").build();
        }
        dao.deleteEvent(<%=entityName%>);
        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public JsonObject getEvent(@PathParam("id") int eventId) {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        <%=capEntityName%> o = dao.readEvent(eventId);
        if(o != null) {
            builder<%attributes.forEach(e =>{-%>.add("<%=e.name%>",o.get<%=utils.pascalCase(e.name)%>())<%})%>;
        }
        return builder.build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Transactional
    public JsonArray getEvents() {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        JsonArrayBuilder finalArray = Json.createArrayBuilder();
        for (<%=capEntityName%> <%=entityName%> : dao.readAllEvents()) {
            builder<%attributes.forEach(e =>{-%>.add("<%=e.name%>",o.get<%=utils.pascalCase(e.name)%>())<%})%>;
            finalArray.add(builder.build());
        }
        return finalArray.build();
    }
}
