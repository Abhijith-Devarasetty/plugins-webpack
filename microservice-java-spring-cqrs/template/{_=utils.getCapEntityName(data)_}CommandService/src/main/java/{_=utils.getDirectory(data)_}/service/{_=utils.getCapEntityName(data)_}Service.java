package <%=utils.getPackageName(data)%>.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

<%let capEntityName = utils.getCapEntityName(data)-%>
<%let EntityName = utils.getEntityName(data)-%>
<%let attributes = data.project.entities[0].attributes%>
<%if(utils.composite(attributes)){-%>
import <%=utils.getPackageName(data)%>.composite.<%=capEntityName%>Id;
<%}-%>
import <%=utils.getPackageName(data)%>.model.<%=capEntityName%>;
import <%=utils.getPackageName(data)%>.repo.<%=capEntityName%>Repo;

@Service
public class <%=capEntityName%>Service {
	
	@Autowired
	private <%=capEntityName%>Repo repo;

	public <%=capEntityName%> add<%=capEntityName%>(<%=capEntityName%> <%=EntityName%>) {
		System.out.println("in service");
		return repo.save(<%=EntityName%>);
	}

<%if(utils.composite(attributes)){-%>

	public void delete<%=capEntityName%>ByCompositeId(<%=capEntityName%>Id <%=EntityName%>Id) {
		try{
			repo.deleteById(<%=EntityName%>Id);
		} catch(Exception e){
			
		}
	}

	public <%=capEntityName%> update<%=capEntityName%>(<%=capEntityName%> <%=EntityName%>,<%=capEntityName%>Id <%=EntityName%>Id){
		<%=capEntityName%> o = repo.findById(<%=EntityName%>Id).orElse(new <%=capEntityName%>());
		<% data.project.entities[0].attributes.forEach(e=>{ %>	
			o.set<%=utils.pascalCase(e.name)%>(<%=EntityName%>.get<%=utils.pascalCase(e.name)%>());
		<%}) %>
		return repo.save(o);
	}
<%}else{-%>

	public void delete<%=capEntityName%>ById(<%=utils.getPrimaryDataType(data)%> id) {
		try{
			repo.deleteById(id);
		} catch(Exception e){
			
		}
	}

	public <%=capEntityName%> update<%=capEntityName%>(<%=capEntityName%> <%=EntityName%>,<%=utils.getPrimaryDataType(data)%> id){
		<%=capEntityName%> o = repo.findById(id).orElse(new <%=capEntityName%>());
		<% data.project.entities[0].attributes.forEach(e=>{ %>	
			o.set<%=utils.pascalCase(e.name)%>(<%=EntityName%>.get<%=utils.pascalCase(e.name)%>());
		<%}) %>
		return repo.save(o);
	}
<%}-%>
}
