package <%=utils.getPackageName(data)%>;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class <%=utils.getCapEntityName(data)%>ApplicationTests {

	@Test
	void contextLoads() {
	}

}
