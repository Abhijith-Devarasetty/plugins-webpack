package <%=utils.getPackageName(data)%>.controller;

import java.util.*;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.web.bind.annotation.*;

<%let capEntityName = utils.getCapEntityName(data)-%>
<%let EntityName = utils.getEntityName(data)-%>
<%let attributes = data.project.entities[0].attributes%>
<%let compositeKeys = utils.compositeKeys(attributes)-%>
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import <%=utils.getPackageName(data)%>.model.<%=capEntityName%>;
import <%=utils.getPackageName(data)%>.service.<%=capEntityName%>Service;
import io.swagger.annotations.ApiOperation;

@RequestMapping("/<%=capEntityName%>")
@RestController
public class <%=capEntityName%>Controller {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private <%=capEntityName%>Service service;
	
	@GetMapping("/getall")
	@ResponseBody
	public List<<%=capEntityName%>> get<%=capEntityName%>(){
		log.info("controller get all <%=capEntityName%> method");
		return service.getAll<%=capEntityName%>();
	}

<% -%>
<%if(utils.composite(attributes)){-%>

	@GetMapping("/getbyid/{id}")
	@ApiOperation(value = "Get <%=capEntityName%> by ID api")
	@ResponseBody
	public Optional<<%=capEntityName%>> get<%=capEntityName%>ById(<% -%>
<%let len = compositeKeys.length-%>
<%compositeKeys.forEach(e => {-%>
<%len = len-1-%>
  @RequestParam <%=utils.getDataType(e)%> <%=e.name%><%if(len!==0){%>,<%}%><% -%>
<%})-%>
  ) {
		<%=capEntityName%>Id <%=EntityName%>Id = new <%=capEntityName%>Id(<% -%>
<%len = compositeKeys.length-%>
<%compositeKeys.forEach(e => {-%>
<%len = len-1-%>
  <%=e.name%><%if(len!==0){%>,<%}%><% -%>
<%})-%>
  );
		log.info("controller find by Id method");
		return service.get<%=capEntityName%>ByCompositeId(<%=EntityName%>Id);
	}

<%}else{-%>
	
	@GetMapping("/getbyid/{id}")
	@ApiOperation(value = "Get <%=capEntityName%> by ID api")
	@ResponseBody
	public Optional<<%=capEntityName%>> get<%=capEntityName%>ById(@PathVariable <%=utils.getPrimaryDataType(data)%> id) {
		log.info("controller find by Id method");
		return service.get<%=capEntityName%>ById(id);
	}

<%}-%>

<%if(!utils.isEmpty(data.project.application.action)){-%>
<%data.project.application.action.forEach(a=>{-%>
<%let action = utils.getRestCall(a)-%>
<%let methLen = action['str'].length-%>
	@<%-action["met"]%>Mapping("<%-action['str']%>")
	@ResponseBody
	public <%=utils.pascalCase(a.response.entity)%> <%=utils.actionName(a)%>(<%-utils.getFinalParams(a)%>) {
		// Business Logic Here ..... 	
	}

<%})-%>
<%}%>
}
