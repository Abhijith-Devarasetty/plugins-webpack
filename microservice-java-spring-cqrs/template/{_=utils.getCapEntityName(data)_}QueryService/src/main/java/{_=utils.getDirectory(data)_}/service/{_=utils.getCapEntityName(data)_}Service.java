package <%=utils.getPackageName(data)%>.service;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

<%let capEntityName = utils.getCapEntityName(data)-%>
<%let EntityName = utils.getEntityName(data)-%>
<%let attributes = data.project.entities[0].attributes%>
<%if(utils.composite(attributes)){-%>
import <%=utils.getPackageName(data)%>.composite.<%=capEntityName%>Id;
<%}-%>
import <%=utils.getPackageName(data)%>.model.<%=capEntityName%>;
import <%=utils.getPackageName(data)%>.repo.<%=capEntityName%>Repo;

@Service
public class <%=capEntityName%>Service {
	
	@Autowired
	private <%=capEntityName%>Repo repo;
	
	public List<<%=capEntityName%>> getAll<%=capEntityName%>(){
		return repo.findAll();
	}

<%if(utils.composite(attributes)){-%>
	public Optional<<%=capEntityName%>> get<%=capEntityName%>ByCompositeId(<%=capEntityName%>Id <%=EntityName%>Id) {
		return repo.findById(<%=EntityName%>Id);
	}

<%}else{-%>
	public Optional<<%=capEntityName%>> get<%=capEntityName%>ById(<%=utils.getPrimaryDataType(data)%> id) {
		return repo.findById(id);
	}

<%}-%>
}
