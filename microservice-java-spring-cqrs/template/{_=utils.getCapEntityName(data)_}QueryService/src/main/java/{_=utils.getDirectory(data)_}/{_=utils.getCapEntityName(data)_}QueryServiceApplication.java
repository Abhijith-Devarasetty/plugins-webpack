package com.company.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
<%if(data.project.application.discovery != undefined){-%>
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
<%}-%>
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

<%if(data.project.application.tracing!=undefined){-%>
import brave.sampler.Sampler;
<%}-%>

<%let capEntityName = utils.getCapEntityName(data)-%>
<%let EntityName = utils.getEntityName(data)-%>
<%if(data.project.application.discovery != undefined){-%>
@EnableDiscoveryClient
<%}-%>
@SpringBootApplication
public class <%=capEntityName%>QueryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(<%=capEntityName%>QueryServiceApplication.class, args);
	}
	
	@Bean
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}
	
	<%=utils.getTracing(data)%>

}
