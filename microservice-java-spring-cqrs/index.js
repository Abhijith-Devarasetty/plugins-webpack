const _ = require("lodash");
const path = require("path");
let jsonata = require("jsonata");
// const test = require("./utils.js");
// const {getRestCall} = require("./utils.js");

_.mixin({ pascalcase: _.flow(_.camelCase, _.upperFirst) });

function pascalCase(input) {
  return _.pascalcase(input);
}

function getDirectory(data) {
  let p = data.project.application.package.replace(/\./g, "//");
  return path.normalize(p);
}

function getPackageName(data) {
  return data.project.application.package;
}

function getEntityName(data) {
  return data.project.entities[0].name;
}

function getCapEntityName(data) {
  let entityName = data.project.entities[0].name;
  return pascalCase(entityName);
}

function repoPrimary(schema, entity) {
  if (jsonata("$count(*[primaryKey ='true'])>1").evaluate(schema)) {
    return pascalCase(entity) + "Id";
  }
  if (jsonata("$count(*[primaryKey ='true'])<1").evaluate(schema)) {
    return "Long";
  }
  for (let i = 0; i < schema.length; i++) {
    if (schema[i].primaryKey === "true") {
      switch (getDataType(schema[i])) {
        case "int":
          return "Integer";
        case "long":
          return "Long";
        case "Long":
          return "Long";
        default:
          return "Long";
      }
    }
  }
}

function getImports(attributes) {
  let set = new Set();
  let result = "";
  attributes.forEach((ele) => {
    if (!_.isEmpty(ele.actualDataType)) {
      set.add("import " + ele.actualDataType + ";");
    }
    if (ele.dataType == "clob") {
      set.add("import java.sql.Clob;");
    }
  });
  for (let temp of set) {
    result =
      result +
      temp +
      `
`;
  }
  return result;
}

function getPrimaryDataType(data) {
  let attributes = data.project.entities[0].attributes;
  for (let i = 0; i < attributes.length; i++) {
    if (attributes[i].primaryKey === "true") {
      return getDataType(attributes[i]);
    }
  }
}

function primaryKey(schema) {
  for (let i = 0; i < schema.length; i++) {
    if (schema[i].meta.pk === "true") {
      switch (getDataType(schema[i])) {
        case "int":
          return "Integer";
        case "long":
          return "Long";
        case "Long":
          return "Long";
        default:
          return "Long";
      }
    }
  }
}

function getDataType(ele) {
  switch (ele.dataType) {
    case "int":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "int";
      }
    case "integer":
      return "int";
    case "decimal":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "float";
      }
    case "string":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "String";
      }
    case "boolean":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "boolean";
      }
    case "date":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "Date";
      }
    case "time":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "Timestamp";
      }
    case "clob":
      return "Clob";
    case "blob":
      return "byte[]";
    default:
      return ele.dataType;
  }
}

function getPrimaryDatType(schema) {
  schema.forEach((ele) => {
    if (ele.meta.pk == "true") {
      switch (ele.dataType) {
        case "int":
          return "Integer";
      }
    }
  });
}

function getRepository(data) {
  switch (data.project.application.database.dbName) {
    case "mongo":
      return "MongoRepository";
    default:
      return "JpaRepository";
  }
}

function checkEureka(data) {
  if (data.project.application.discovery != undefined) {
    switch (Object.keys(data.project.application.discovery)[0]) {
      case "eureka":
        return `
        <dependency>
          <groupId>org.springframework.cloud</groupId>
          <artifactId>spring-cloud-starter-netflix-eureka-client</artifactId>
        </dependency>`;
    }
  }
}

function checkJpa(data) {
  if (data.project.application.database.dbName != "mongo") {
    return `
  <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
  </dependency>`;
  }
}

function getDataBase(data) {
  switch (data.project.application.database.dbName) {
    case "h2":
      return `
      <dependency>
			    <groupId>com.h2database</groupId>
			    <artifactId>h2</artifactId>
			    <scope>runtime</scope>
		  </dependency>`;

    case "mysql":
      return `
      <dependency>
			    <groupId>mysql</groupId>
			    <artifactId>mysql-connector-java</artifactId>
			    <scope>runtime</scope>
      </dependency>`;

    case "mongo":
      return `
      <dependency>
			    <groupId>org.springframework.boot</groupId>
			    <artifactId>spring-boot-starter-data-mongodb</artifactId>
      </dependency>`;

    case "postgresql":
      return `
      <dependency>
			    <groupId>org.postgresql</groupId>
			    <artifactId>postgresql</artifactId>
			    <scope>runtime</scope>
		  </dependency>`;
  }
}

function getDataBaseProperties(data) {
  let path = data.project.application.database.url;
  switch (data.project.application.database.dbName) {
    case "h2":
      return `
spring.h2.console.enabled=true
spring.database.platform=h2
spring.database.url=${path}`;

    case "mysql":
      return `
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.url=${path}
spring.datasource.username= 
spring.datasource.password=
spring.jpa.show-sql=true
spring.jpa.hibernate.ddl-auto=update
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5Dialect`;

    case "mongo":
      return `spring.data.mongo.uri=${path}`;

    case "postgresql":
      return `
spring.jpa.database=POSTGRESQL
spring.datasource.platform=postgres
spring.datasource.url=${path}
spring.datasource.username= 
spring.datasource.password= 
spring.jpa.show-sql=true
spring.jpa.generate-ddl=true
spring.jpa.hibernate.ddl-auto=update
spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true`;
  }
}

function checkTracing(data) {
  if (data.project.application.tracing !== undefined) {
    switch (Object.keys(data.project.application.tracing)[0]) {
      case "zipkin":
        return `
    <dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-sleuth</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-zipkin</artifactId>
		</dependency>`;
    }
  }
}

function getTracing(data) {
  let result = "";
  if (data.project.application.tracing != undefined) {
    let trace = Object.keys(data.project.application.tracing);
    switch (trace[0]) {
      case "zipkin":
        result = `    
    @Bean
    public Sampler defaultSampler() {
      return Sampler.ALWAYS_SAMPLE;
    }`;
        break;

      default:
        break;
    }
  }
  return result;
}

function getRestCall(act) {
  let http = new RegExp(/(http|grpc)/g, "g");
  let met = new RegExp(/(GET|POST|PUT|DELETE)/g, "g");
  let str = new RegExp(/:(\/\S*)/g, "g");
  let protocol = _.capitalize(_.lowerCase(act.method.match(met)[0]));
  return {
    http: act.method.match(http)[0],
    met: protocol,
    str: act.method.match(str)[0].slice(1),
  };
}

function actionName(act) {
  if (act.method_name !== undefined) {
    return act.method_name;
  } else {
    let str = new RegExp(/:(\/\S*)/g, "g");
    let methLen = str.length;
    return act.method.match(str)[0].slice(1).substring(1, methLen);
  }
}

function getRequestBody(action) {
  req = action.request;
  if (req !== undefined) {
    // console.log("type of",typeof(req));
    if (typeof req === "object") {
      return "@RequestBody " + _.capitalize(req.entity) + " " + req.entity;
    } else {
      if (req != "") {
        return "@RequestBody " + pascalCase(req) + " " + _.toLower(req);
      } else {
        return "";
      }
    }
  } else {
    return "";
  }
}

function getReqParams(action) {
  if (action.req_params !== undefined) {
    params1 = action.req_params;
    str = "";
    params1.forEach((p) => {
      str = str + "@RequestParam " + p.split(":")[1] + " " + p.split(":")[0];
      // console.log("in for each of reqParam",str);
    });
    return str;
  } else {
    return "";
  }
}

function getPathParams(action) {
  if (action.path_params !== undefined) {
    params = action.path_params;
    str = "";
    params.forEach((p) => {
      str =
        str +
        "@PathVariable('" +
        p.split(":")[0] +
        "') " +
        getActionDataType(p.split(":")[1].trim()) +
        " " +
        p.split(":")[0];
      // console.log("path",p.split(":")[1]);
    });
    return str;
  } else {
    return "";
  }
}

function getFinalParams(action) {
  let str = (getReqParams(action) + getPathParams(action)).split("@");
  str = getRequestBody(action) + str.join(" , @");
  if (str.substr(0, 2) == " ,") {
    str = str.substring(2);
  }
  return str;
}

function getActionDataType(type) {
  switch (type) {
    case "int":
      return "int";
    case "integer":
      return "int";
    case "string":
      return "String";
    default:
      return type;
  }
}

function composite(attributes) {
  if (jsonata("$count(*[primaryKey ='true'])>1").evaluate(attributes)) {
    return "composite";
  } else {
    return "";
  }
}

function compositeKeys(attributes) {
  let arr = [];
  attributes.forEach((e) => {
    if (e.primaryKey === "true") {
      arr.push(e);
    }
  });
  return arr;
}

function getSerializ(schema) {
  if (jsonata("$count(*[primaryKey ='true'])>1").evaluate(schema)) {
    return "implements Serializable";
  } else {
    return "";
  }
}

function getRelation(data, entityName) {
  rel = jsonata(`project.entities[name = "${entityName}"].relations`).evaluate(data);
  if (rel != undefined) {
    let result = '';
    rel.forEach((r) => {
      let relData = {
        rhsEntity: r.rhs.split("|")[0].substring(5),
        rhsAttr: r.rhs.split("|")[1].substring(6),
        lhsEntity: r.lhs.split("|")[0].substring(5),
        lhsAttr: r.lhs.split("|")[1].substring(6),
      };
      if (r.relationship === "OneToMany") {
        if (relData.lhsEntity === entityName) {
          result = result + `
	  @OneToMany(mappedBy="${entityName}")
    private List<${pascalCase(relData.rhsEntity)}> ${relData.rhsEntity};
`;
        }
        if (relData.rhsEntity === entityName) {
          result = result + `
    @ManyToOne
    @JoinColumn(name="${getJoinColumn(r, relData)}", nullable=false)
    private ${pascalCase(relData.lhsEntity)} ${relData.lhsEntity};
`;
        }
      }else if(r.relationship === "ManyToMany"){
        if (relData.lhsEntity === entityName) {
          result = result + `
    @ManyToMany
    @JoinTable(name="${getMergeTable(r)}",joinColumns=@JoinColumn(name="${getJoinColumn(r,relData)}"),
    inverseJoinColumns=@JoinColumn(name="${getInverseJoinColumns(r,relData)}"))
    private List<${pascalCase(relData.rhsEntity)}> ${relData.rhsEntity}s=new ArrayList<>();
`;
        }
        if (relData.rhsEntity === entityName) {
          result = result + `
    @ManyToMany(mappedBy="${relData.rhsEntity}s")
    private List<${pascalCase(relData.lhsEntity)}> ${relData.lhsEntity}=new ArrayList<>();
`;
        }
      }else if(r.relationship === "OneToOne"){
        if(relData.lhsEntity === entityName){
          if(r.meta.targetEntity === undefined){
            result = result + `
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "${getJoinColumn(r,relData)}", referencedColumnName = "${relData.rhsAttr}")
    private ${pascalCase(relData.rhsEntity)} ${relData.rhsEntity};
`;
          }else{
            if(r.meta.targetEntity === entityName){
              result = result + `
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "${getJoinColumn(r,relData)}", referencedColumnName = "${relData.rhsAttr}")
    private ${pascalCase(relData.rhsEntity)} ${relData.rhsEntity};
`;
            }else{
              result = result + `
    @OneToOne(mappedBy = "${relData.lhsEntity}")
    private ${pascalCase(relData.rhsEntity)} ${relData.rhsEntity};
`;
            }
          }
        }
        if(relData.rhsEntity === entityName){
          if(r.meta.targetEntity === undefined){
            result = result + `
    @OneToOne(mappedBy = "${relData.rhsEntity}")
    private ${pascalCase(relData.lhsEntity)} ${relData.lhsEntity};
`;
          }else{
            if(r.meta.targetClass === entityName){
              result = result + `
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "${getJoinColumn(r,relData)}", referencedColumnName = "${relData.lhsAttr}")
    private ${pascalCase(relData.lhsEntity)} ${relData.lhsEntity};
`;
            }else{
              result = result + `
    @OneToOne(mappedBy = "${relData.rhsEntity}")
    private ${pascalCase(relData.lhsEntity)} ${relData.lhsEntity};
`;
            }
          }
        }
      }
    });
    return result;
  }
}

function getJoinColumn(rel, relData) {
  let joinColumn;
  if (!_.isEmpty(rel.meta) && rel.meta.joinColumn != undefined) {
    joinColumn = rel.meta.joinColumn;
  } else {
    if (rel.relationship == "OneToMany") {
      joinColumn = relData.lhsEntity + "_fk";
    } else if (rel.relationship == "ManyToMany") {
      joinColumn = relData.lhsAttr;
    } else if (rel.relationship == "OneToOne") {
      joinColumn = "fk";
    }
  }
  return joinColumn;
}

function getInverseJoinColumns(rel,relData) {
  let inverseJoinColumn;
  if (!_.isEmpty(rel.meta) && rel.meta.inverseJoinColumn != undefined) {
    inverseJoinColumn = rel.meta.inverseJoinColumns;
  } else {
    inverseJoinColumn = relData.rhsAttr;
  }
  return inverseJoinColumn;
}

function getMergeTable(rel) {
  let mergeTable;
  if (!_.isEmpty(rel.meta) && rel.meta.mergeTable != undefined) {
    mergeTable = rel.meta.mergeTable;
  } else {
    mergeTable = "merge_table";
  }
  return mergeTable;
}

function checkKogitoDepen(data){
  if(jsonata("**.bpmn").evaluate(data) !== undefined | jsonata("**.dmn").evaluate(data) !== undefined){
    return `
    <dependency>
      <groupId>org.kie.kogito</groupId>
      <artifactId>kogito-springboot-starter</artifactId>
    </dependency>
    <dependency>
      <groupId>io.rest-assured</groupId>
      <artifactId>rest-assured</artifactId>
      <scope>test</scope>
    </dependency>
    <dependency>
      <groupId>org.kie.kogito</groupId>
      <artifactId>kogito-scenario-simulation</artifactId>
      <scope>test</scope>
    </dependency>
    `
  }
}

function checkKogitoDepenMang(data){
  console.log("kogito",jsonata("**.bpmn").evaluate(data))
  if(jsonata("**.bpmn").evaluate(data) !== undefined | jsonata("**.dmn").evaluate(data) !== undefined){
    return `
      <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-dependencies</artifactId>
        <version>\${springboot.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
      <dependency>
        <groupId>org.kie.kogito</groupId>
        <artifactId>kogito-bom</artifactId>
        <version>\${project.version}</version>
        <type>pom</type>
        <scope>import</scope>
      </dependency>
    `
  }
}

module.exports = {
  getDirectory,
  getTracing,
  getEntityName,
  getCapEntityName,
  checkEureka,
  getPackageName,
  getDataBase,
  getDataBaseProperties,
  checkTracing,
  getPrimaryDatType,
  getJoinColumn,
  getInverseJoinColumns,
  getMergeTable,
  checkJpa,
  getRepository,
  getDataType,
  getImports,
  getRestCall,
  getPrimaryDataType,
  primaryKey,
  repoPrimary,
  composite,
  compositeKeys,
  getSerializ,
  getFinalParams,
  actionName,
  pascalCase,
  getRelation,
  checkKogitoDepen,
  checkKogitoDepenMang
};
