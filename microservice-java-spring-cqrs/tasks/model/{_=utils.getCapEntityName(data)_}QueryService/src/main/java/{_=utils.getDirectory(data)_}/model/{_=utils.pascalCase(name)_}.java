package <%=utils.getPackageName(data)%>.model;

import java.util.*;
import java.sql.*;
<%=utils.getImports(attributes)%>
<%if(data.project.application.database.dbName != "mongo"){-%>
import javax.persistence.*;
<%}else{-%>
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
<%}-%>
<% let n= utils.pascalCase(name);-%>
<% let entityName = name;-%>

<%if(utils.composite(attributes)){-%>
import javax.persistence.IdClass;
import java.io.Serializable;
import <%=utils.getPackageName(appInput.config)%>.composite.<%=utils.capitalize(entity)%>Id;

@IdClass(<%=n%>Id.class)
<%}-%>
<%if(data.project.application.database.dbName != "mongo"){-%>
@Entity
<%if(tableName !== ''){-%>
@Table( name = "<%=tableName%>")
<%}-%>
<%}else{-%>
@Document
<%}-%>
public class <%=n%> <%=utils.getSerializ(attributes)%>{

<%attributes.forEach(e=>{-%>
<%if(e.primaryKey == 'true'){-%>
	@Id
<%}-%>
<%if(e.dataType == "blob" | e.dataType == "clob"){-%>
	@Lob
<%}-%>
<%if(e.columnName != undefined && e.columnName != ""){-%>
	@Column(name="<%=e.columnName%>")
<%}-%>
	private <%=utils.getDataType(e)%> <%=e.name%>;

	public <%=utils.getDataType(e)%> get<%=utils.capitalize(e.name)%>(){
		return <%=e.name%>;
	}

	public void set<%=utils.capitalize(e.name)%>(<%=utils.getDataType(e)%> <%=e.name%>){
		this.<%=e.name%> = <%=e.name%>;
	}

<%if(e.Enum!==undefined){-%>
	enum <%=e.Enum%>{
<%let len = e.values.length-%>
<%e.values.forEach(v=>{-%>
<%len = len-1-%>
		<%=v%><%if(len!==0){%>,<%}%>
<%})-%>
	}
<%}-%>
<%})-%>

<%-utils.getRelation(data,name)%>

<%let relations = appInput.config.framework.springboot.entities[0].relations-%>
<%if(relations != undefined && relations.length != 0 && !utils.isEmpty(relations)){-%>
<%appInput.config.framework.springboot.entities[0].relations.forEach(r=>{ -%>
<%if(r.lhs.entity==entityName || r.rhs.entity==entityName){-%>
<%if(r.relationship=="OneToOne"){-%>
<%if(r.lhs.entity==entityName){-%>
<%if(utils.isEmpty(r.meta.targetEntity)){-%>
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "<%=utils.getJoinColumn(r)%>", referencedColumnName = "<%=r.rhs.attribute%>")
	private <%=utils.capitalize(r.rhs.entity)%> <%=r.rhs.entity%>;
<%}else{-%>
<%if(entityName == r.meta.targetEntity){-%>
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "<%=utils.getJoinColumn(r)%>", referencedColumnName = "<%=r.rhs.attribute%>")
<%}else{-%>
	@OneToOne(mappedBy = "<%=r.lhs.entity%>")
<%}-%>
	private <%=utils.capitalize(r.rhs.entity)%> <%=r.rhs.entity%>;
<%}%>
<%}-%>
<%if(r.rhs.entity==entityName){-%>
<%if(utils.isEmpty(r.meta.targetEntity)){-%>
	@OneToOne(mappedBy = "<%=r.rhs.entity%>")
	private <%=utils.capitalize(r.lhs.entity)%> <%=r.lhs.entity%>;
<%}else{-%>
<%if(entityName == r.meta.targetClass){-%>
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "<%=utils.getJoinColumn(r)%>", referencedColumnName = "<%=r.lhs.attribute%>")
<%}else{-%>
	@OneToOne(mappedBy = "<%=r.rhs.entity%>")
<%}-%>
	private <%=utils.capitalize(r.lhs.entity)%> <%=r.lhs.entity%>;
<%}-%>
<%}%>
<%}%>
<%if(r.relationship=="OneToMany"){-%>
<%if(r.lhs.entity==entityName){-%>
	@OneToMany(mappedBy="<%=entityName%>")
    private List<<%=utils.capitalize(r.rhs.entity)%>> <%=r.rhs.entity%>;
<%}-%>
<%if(r.rhs.entity==entityName){-%>
	@ManyToOne
    @JoinColumn(name="<%=utils.getJoinColumn(r)%>", nullable=false)
    private <%=utils.capitalize(r.lhs.entity)%> <%=r.lhs.entity%>;
<%}-%>
<%}-%>
<%if(r.relationship=="ManyToMany"){%>
<%if(r.lhs.entity==entityName){%>
	@ManyToMany
	@JoinTable(name="<%=utils.getMergeTable(r)%>",joinColumns=@JoinColumn(name="<%=utils.getJoinColumn(r)%>"),
	inverseJoinColumns=@JoinColumn(name="<%=utils.getInverseJoinColumns(r)%>"))
	private List<<%=utils.capitalize(r.rhs.entity)%>> <%=r.rhs.entity%>s=new ArrayList<>();
<%}else if(r.rhs.entity==entityName){%>
	@ManyToMany(mappedBy="<%=r.rhs.entity%>s")
	private List<<%=utils.capitalize(r.lhs.entity)%>> <%=r.lhs.entity%>=new ArrayList<>();
<%}-%>
<%}-%>
<%}-%>
<%})-%>
<%}-%>

	public <%=n%>() {  }

	public <%=n%>(
<%let len = attributes.length-%>
<% attributes.forEach(e=>{-%>
<%len = len-1-%>
		<%=utils.getDataType(e)%> <%=e.name%><%if(len!==0){%>,<%}%>
<%})-%>
	) {
<% attributes.forEach(e=>{-%>
		this.<%=e.name%> = <%=e.name%>;
<%})-%>
	}
}
