let jsonata = require("jsonata");

function processData(input, getRef) {
  let framework = jsonata("**.framework.($keys())").evaluate(input);
  let output = {
    projects: {
      application: processApplications(input.applications),
      entities: processEntities(input.applications, framework, getRef),
      components: processComponents(input.applications, framework, getRef),
    },
  };
  return output;
}

function processApplications(applications) {
  if (applications.length > 0) {
    let application = [];
    applications.forEach((app) => {
      let appName = jsonata("$.appName").evaluate(app);
      let value = {
        name: appName,
        templateId: app.templateId,
        framework: jsonata("$.framework").evaluate(app),
        package: jsonata("**.package_name").evaluate(app),
        port: jsonata("**.port").evaluate(app),
        database: {
          url: jsonata("**.database.**.url").evaluate(app),
          dbName: "",
          username: "",
          password: "",
        },
        tracing: jsonata("**.tracing").evaluate(app),
        logging: jsonata("**.logging").evaluate(app),
        discovery: jsonata("**.discovery").evaluate(app),
        entities: jsonata("$.**.entities.entity").evaluate(app),
        components: jsonata("$.**.components.componentName").evaluate(app),
      };
      application.push({ appConfig: value });
    });
    return application;
  } else {
    return [];
  }
}

function processEntities(applications, framework, getRef) {
  if (
    jsonata("**." + framework + ".entities").evaluate(applications) != undefined
  ) {
    let entityArray = [];
    let entities = jsonata("**." + framework + ".entities").evaluate(
      applications
    );
    entities.forEach((entity) => {
      entity = getRef(entity);
      let entityName = jsonata("$.entity").evaluate(entity);
      let value = {
        name: entity.entity,
        isCompositeKey: jsonata("$count(schema.meta[pk ='true'])>1").evaluate(
          entity
        ),
        taskId: jsonata("**.taskId").evaluate(entity),
        attributes: processAttributes(entity.schema, framework),
      };
      entityArray.push({ [entityName]: value });
    });
    return entityArray;
  } else {
    return [];
  }
}

function processAttributes(schema, framework) {
  let result = [];
  schema.forEach((ele) => {
    if (framework == "angular") {
      if (ele.dataType == "int" || ele.dataType == "integer") {
        ele.dataType = "number";
      }
    }
    result.push({
      name: ele.attribute,
      dataType: ele.dataType,
      columnName:
        ele.meta.column != undefined ? ele.meta.column : ele.attribute,
      primaryKey: ele.meta.id ? "true" : "fasle",
    });
  });
  return result;
}

function processComponents(applications, framework, getRef) {
  if (jsonata("**.components").evaluate(applications) != undefined) {
    let components = jsonata("**.components").evaluate(applications);
    let arr = [];
    components.forEach((comp) => {
      comp = getRef(comp);
      if (comp.taskId === "TabLayoutForm") {
        let appName = jsonata("$.componentName").evaluate(comp);
        let value = {
          componentName: appName,
          tabLayoutData: processTabElements(comp),
          taskId: "TabLayoutForm",
          layout: jsonata("$.layout").evaluate(comp.componentStructure)
        };
        arr.push(value);
      } else {
        let appName = jsonata("$.componentName").evaluate(comp);
        let value = {
          name: appName,
          fields: processFields(comp, framework),
          taskId: "form"
        };
        arr.push({ [appName]: value });
      }
    });
    return arr;
  } else {
    return [];
  }
}

function processTabElements(comp) {
  elements = jsonata("**.elements").evaluate(comp);
  let tempTabs = [];
  let sectionNames = [];
  let tabs = [];
  let temp;
  let tabNames = [];
  elements.forEach((e) => {
    if (!tabNames.includes(e.tab)) {
      tabNames.push(e.tab);
    }
  });

  elements.forEach(e =>{
    tempTabs[e.tab] = elements.filter(i => i.tab === e.tab);
  });

  tabNames.forEach(n => {
    let sections = [];
    tempTabs[n].forEach(t => {
      if(!sectionNames.includes(t.section)){
        sectionNames.push(t.section);
      }
    })
    sectionNames.forEach(s => {
      temp = tempTabs[n].filter(i => i.section === s);
      sections.push({"sectionName":s,elements:temp});
    })
    sectionNames = [];
    tabs.push({"tabName":n,"sections":sections});
  })
  
  return tabs;
}

function processFields(comp, framework) {
  let arr = [];
  comp = jsonata("**.elements").evaluate(comp);
  comp.forEach((field) => {
    let temp = {
      element: field.type,
      label: field.label,
      name: field.name,
      model: field.model,
    };
    if (field.options !== undefined) {
      temp["options"] = field.options;
    }
    if (field.option_action !== undefined) {
      temp["option_action"] = field.option_action;
    }
    arr.push(temp);
  });
  return arr;
}

function processEnums(input) {}

function processResources(input) {}

module.exports = { processData };
