let jsonata = require("jsonata");
var _ = require('lodash');

_.mixin({ pascalcase: _.flow(_.camelCase, _.upperFirst) });

function pascalCase(input) {
  return _.pascalcase(input);
}

function compName(name,flag){
  if(flag === "dash"){
    //return _.camelCase(name.replace(/[A-Z]/g, '-$&').toLowerCase());
    return _.camelCase(name).replace(/[A-Z]/g, '-$&').toLowerCase();
  }else if(flag === "lower"){
    return _.lowerCase(name);
  }else if (flag === "upper"){
    return _.upperCase(name);
  }
}

function getAppName(data){
  return jsonata("**.appConfig.name").evaluate(data);
}

function getComponentName(config) {
  return config.framework.angular.components[0].componentName;
}

function getComponentFileds(componentName, data) {
  return jsonata("**." + componentName + ".fields").evaluate(data);
}

function getEntityName(componentName, data) {
  return jsonata("**." + componentName + ".fields[0].model").evaluate(data);
}

function getEntitySchema(entityName,data) {
  return jsonata("**.entities."+entityName+".attributes").evaluate(data);
}

function getRouting(data){
  // console.log(jsonata("**.projects.components").evaluate(data));
  return jsonata("**.projects.components").evaluate(data);
}

function getId(schema){
  for(let i=0;i<schema.length;i++){
    if(schema[i].primaryKey === "true"){
      return schema[i].name;
    }
  }
}

function getDataType(ele) {
  // console.log("ele",ele);
  // console.log("ele datatype",ele.dataType);
  switch (ele.dataType) {
    case "int":
        return "number";
    case "decimal":
        return "number";
    case "string":
        return "string";
    case "boolean":
        return "boolean";
    case "date":
        return "Date";
    case "time":
        return "Timestamp";
    default:
      return ele.dataType;
  }
}

function getUiElement(material,ele,layout){
  if(material === "material"){
    return getMaterialUiElement(ele,layout);
  }
  if(material === "primeNg"){
    return getPrimeNgUiElement(ele,layout);
  }
}

function getPrimeNgUiElement(ele,layout){
  let grid = 12/layout;
  let options = '';
  switch(ele.type){
    case 'Input': return `
    <div class="ui-g-${grid}">
      <label id="${ele.name}">${ele.label}</label><br />
      <input type="text" formControlName="${ele.name}" pInputText />
    </div>`;

    case 'Datepicker': return `
    <div class="ui-g-${grid}">
      <label id="${ele.name}">${ele.label}</label><br />
      <p-calendar formControlName="${ele.name}" ></p-calendar>
    </div>`;
    
    case 'Radio':
      options ='';
      ele.options.forEach(o => {
        options = options + `
        <p-radioButton name="groupname" value="${o}" label="${o}"></p-radioButton>`;
      });
      return `
      <div class="ui-g-${grid}">
        <label id="${ele.name}">${ele.label}</label><br />`+ options +`
      </div>`
      ;

    case 'Dropdown':
      ``;

    case 'Checkbox':
      options = '';
      ele.options.forEach(o => {
        options = options + `
        <p-checkbox name="groupname" value="${o}" label="${o}"></p-checkbox>`;
      })
      return `
      <div class="ui-g-${grid}">
        <label id="${ele.name}">${ele.label}</label><br />`+ options +`
      </div>`;

    case 'Button':
      return `
      <div class="ui-g-${grid}">
        <p-button id="${ele.name}" label="${ele.label}" ></p-button>
      </div>`;

    case 'TextArea':
      return `
      <div class="ui-g-${grid}">
        <label id="${ele.name}">${ele.label}</label><br />
        <textarea formControlName="${ele.name}" pInputTextarea ></textarea>
      </div>`
  }
}

function getMaterialUiElement(ele,layout){
  let grid = 12/layout;
  let options = '';
  switch(ele.type){
    case 'Input': return `
        <div class="col-md-${grid} element">
          <mat-label>${ele.label}</mat-label><br />
          <input class="input" formControlName="${ele.name}" matInput />
        </div>`;

    case 'Datepicker': return `
        <div class="col-md-${grid} element">
          <mat-label>${ele.label}</mat-label><br />
            <input class="input" formControlName="${ele.name}" matInput [matDatepicker]="picker">
            <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
          <mat-datepicker #picker></mat-datepicker>
        </div>`;
    
    case 'Radio':
      options ='';
      ele.options.forEach(o => {
        options = options + `
          <mat-radio-button value="${o}">${o}</mat-radio-button>`;
      });
      return `
        <div class="col-md-${grid} element">
          <label>${ele.label}</label><br />
          <mat-radio-group formControlName="${ele.name}" aria-label="${ele.label}">`+ options +
          `
          </mat-radio-group>
        </div>`;

    case 'Select':
      options = '';
      ele.options.forEach(o => {
        options = options + `
            <mat-option value="${o}">${o}</mat-option>`
      });
      return `
        <div class="col-md-${grid} element">
          <mat-label>${ele.label}</mat-label><br />
          <mat-select class="select" formControlName="${ele.name}" matNativeControl required>`
          + options +
          `
          </mat-select>
        </div>`;

    case 'Checkbox':
      options = '';
      ele.options.forEach(o => {
        options = options + `
            <mat-checkbox formControlName="${o}" class="checkbox-option">${o}</mat-checkbox>`;
      })
      return `
        <div class="col-md-${grid} element">
          <mat-label>${ele.label}</mat-label><br />
          <section class="checkbox">`
          + options +
          `
          </section>
        </div>`;

    case 'Button':
      let action ='';
      if(ele.option_action !== undefined){
        action = `(click)=${ele.option_action}()`;
      }
      return `
        <div class="col-md-${grid} element">
          <button mat-raised-button ${action} color="primary">${ele.label}</button><br/>
        </div>
        `;
  }
}

function getActions(tabLayoutData){
  let actions = '';
  tabLayoutData.forEach(t => {
    t.sections.forEach(s => {
      s.elements.forEach(e => {
        if(e.type === "Button"){
          if(e.option_action!==undefined){
            actions = actions + ` ${e.option_action}(){
    // Logic here
  }
`;
          }
        }
      })
    })
  })
  return actions;
}

function getSelectedEle(eleType,tabLayoutData){
  var arr = [];
  tabLayoutData.forEach(t=>{
    let temp = [];
    t.sections.forEach(s=>{
      // temp = s.elements.filter(e => e.type === eleType && e.option_action !== undefined);
      // arr = [...arr,...temp];
      arr = [...s.elements.filter(e => e.type === eleType && e.option_action !== undefined),...arr];
    });
  })
  // console.log(arr);
  return arr;
}

module.exports = {
  getComponentName,
  getComponentFileds,
  getEntityName,
  getEntitySchema,
  getRouting,
  getAppName,
  getId,
  compName,
  getDataType,
  pascalCase,
  getUiElement,
  getActions,
  getSelectedEle
};
