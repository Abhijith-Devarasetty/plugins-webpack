import { Component,  OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-<%=utils.compName(componentName,"dash")%>',
  templateUrl: './<%=utils.compName(componentName,"dash")%>.component.html',
  styleUrls: ['./<%=utils.compName(componentName,"dash")%>.component.css'],
})

export class <%=componentName%>Component implements OnInit {
<%tabLayoutData.forEach(t => {-%>
<%t.sections.forEach(s => {-%>
  <%=utils.pascalCase(s.sectionName)%>: any;
<%})-%>
<%})-%>

  constructor( private formBuilder: FormBuilder ) {
<%let len%>
<%tabLayoutData.forEach(t => {-%>
<%t.sections.forEach(s => {-%>
    this.<%=utils.pascalCase(s.sectionName)%> = this.formBuilder.group({
<%len = s.elements.size-%>
<%s.elements.forEach(e => {-%>
<%len = len - 1;-%>
<%if(e.type === "Checkbox"){-%>
<%e.options.forEach(o =>{-%>
      <%=o%>: [false, Validators.required],
<%})-%>
<%}else{-%>
      <%=e.name%>: ['', Validators.required]<%if(len!==0){%>,<%}%>
<%}-%>
<%})-%>
    });
<%})-%>
<%})-%>
  }

  ngOnInit() { }

<%=utils.getActions(tabLayoutData)%>

}
