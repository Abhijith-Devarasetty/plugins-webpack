import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import {InputTextModule} from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';
import {CalendarModule} from 'primeng/calendar';
import {RadioButtonModule} from 'primeng/radiobutton';
import {CheckboxModule} from 'primeng/checkbox';
import {TabViewModule} from 'primeng/tabview';
import { By } from "@angular/platform-browser";
<%let com = componentName-%>
import { <%=com%>Component } from './<%=utils.compName(com,"dash")%>.component';

describe('<%=com%>Component', () => {
  let component: <%=com%>Component;
  let fixture: ComponentFixture<<%=com%>Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        ReactiveFormsModule,
        InputTextModule,
        DropdownModule,
        CalendarModule,
        RadioButtonModule,
        CheckboxModule,
        TabViewModule
      ],
      declarations: [ <%=com%>Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(<%=com%>Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

<%tabLayoutData.forEach(t => {-%>
<%t.sections.forEach(s => {-%>
<%s.elements.forEach(e => {-%>
  it('testing <%=e.label%> element',()=>{
    const ele = fixture.debugElement.query(By.css('#<%=e.name%>')).nativeElement;
    expect(ele.textContent).toEqual("<%=e.label%>");
  });

<%})-%>
<%})-%>
<%})-%>

<%let optionsActions = utils.getSelectedEle("Button",tabLayoutData)-%>
<%optionsActions.forEach(a=>{-%>
  it('testing the <%=a.option_action%> function',()=>{
    spyOn(component,"<%=a.option_action%>");
    let temp = component.<%=a.option_action%>();
    expect(component.<%=a.option_action%>).toHaveBeenCalled();
  });

<%})-%>

});
