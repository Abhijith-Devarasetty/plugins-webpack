import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { <%=componentName%>Service } from './../<%=utils.compName(componentName,"dash")%>.service';
import { <%=componentName%> } from '../interface/<%=utils.compName(componentName,"dash")%>';

@Component({
  selector: 'app-<%=utils.compName(componentName,"dash")%>-edit',
  templateUrl: './<%=utils.compName(componentName,"dash")%>-edit.component.html',
  styleUrls: ['./<%=utils.compName(componentName,"dash")%>-edit.component.css']
})
export class <%=componentName%>EditComponent implements OnInit {

  constructor(public service: <%=componentName%>Service, public router: Router) { }

<%let entity = utils.getEntityName(componentName,data)-%>
<%let schema = utils.getEntitySchema(entity,data)-%>
<%let len = schema.length%>
<%schema.forEach(ele => {-%>
  <%=ele.name%> : <%=utils.getDataType(ele)%>; 
<%})-%>
  action: string;
  tablename: string;

  localData: <%=componentName%> = {<% -%>
<%schema.forEach(ele => {-%>
<%len =len -1-%>
  <%=ele.name%> : <%if(ele.dataType=='number'){%>0<%}%><%if(ele.dataType!='number'){%>""<%}%><%if(len!==0){%>,<%}%><% -%>
<%})-%>
  <% -%>}

  ngOnInit(){
    this.action = this.service.action;
    this.localData = this.service.element;
    this.tablename = this.service.tablename;
  }
<%let fields = utils.getComponentFileds(componentName,data)-%>
  doAction() {
      if(this.action === "Update"){
<%fields.forEach(f => {-%>
        this.localData.<%=f.name%> = this.<%=f.name%>
<%})-%>
        this.service.update<%=componentName%>(this.localData,this.localData.<%=utils.getId(schema)%>)
        .subscribe((res: any) => {
        }, (err: any) => {
        console.log(err);
        });
      }else if(this.action === "Add"){
<%fields.forEach(f => {-%>
        this.localData.<%=f.name%> = this.<%=f.name%>
<%})-%>
        this.service.insert<%=componentName%>(this.localData)
        .subscribe((res:any) => {
        }, (err: any) => {
          console.log(err);
        })
      }else if(this.action === "Delete"){
        this.service.delete<%=componentName%>(this.localData.<%=utils.getId(schema)%>)
        .subscribe((res: any) => {
        }, (err: any) => {
          console.log(err);
        })
      }
      this.router.navigateByUrl(this.tablename);
  }

  navigatePage() {
    this.router.navigateByUrl(this.tablename);
  }

<%fields.forEach(f=>{-%>
<%if(f.option_action!==undefined){-%>
  <%=f.option_action%>(){
    this.service.<%=f.option_action%>();
  }
<%}-%>
<%})-%>
}
