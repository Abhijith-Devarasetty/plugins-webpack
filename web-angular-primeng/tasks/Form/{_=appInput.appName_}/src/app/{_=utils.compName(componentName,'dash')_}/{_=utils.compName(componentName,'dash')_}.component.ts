import { Router } from '@angular/router';
import {
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material';
import { MatTable } from '@angular/material';
import { <%=componentName%>Service } from './../<%=utils.compName(componentName,"dash")%>.service';
import { <%=componentName%> } from '../interface/<%=utils.compName(componentName,"dash")%>';

@Component({
  selector: 'app-<%=utils.compName(componentName,"dash")%>',
  templateUrl: './<%=utils.compName(componentName,"dash")%>.component.html',
  styleUrls: ['./<%=utils.compName(componentName,"dash")%>.component.css'],
})

export class <%=componentName%>Component implements OnInit {
  dataSource;
  data: <%=componentName%>[];

<%let fields = utils.getComponentFileds(componentName,data)-%>
  displayedColumns: string[] = [
<%fields.forEach(f =>{-%>
  '<%=f.name%>',
<%})-%>
  'edit',
  'delete'
  ];

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatTable, { static: true }) table: MatTable<any>;

  constructor(
    public router: Router,
    public service: <%=componentName%>Service
  ) {  }

  ngOnInit() {
    this.service.getAll<%=componentName%>()
      .subscribe((res: any) => {
        this.data = res;
        this.showData(this.data);
      }, err => {
        console.log(err);
      });
  }

  showData(data){
    this.dataSource = new MatTableDataSource<<%=componentName%>>(data);
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }  


  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  navigatePage(action, obj) {
    this.service.action = action;
    this.service.element = obj;
    this.service.tablename = "<%=utils.compName(componentName,'dash')%>";
    this.router.navigateByUrl('/<%=utils.compName(componentName,"dash")%>-edit');
  }

  // addRowData(rowObj) {
  //   this.service.addRowData(rowObj);
  // }

}
