<%let components = utils.getRouting(data)-%>
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {TableModule} from 'primeng/table';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import {AccordionModule} from 'primeng/accordion';
import {InputTextModule} from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';
import {CalendarModule} from 'primeng/calendar';
import {RadioButtonModule} from 'primeng/radiobutton';
import {CheckboxModule} from 'primeng/checkbox';
import {ButtonModule} from 'primeng/button';
import {InputSwitchModule} from 'primeng/inputswitch';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {KeyFilterModule} from 'primeng/keyfilter';
import {SliderModule} from 'primeng/slider';
import {TabViewModule} from 'primeng/tabview';
import {CardModule} from 'primeng/card';

<%components.forEach(com => {-%>
<%com = com.componentName-%>
import { <%=com%>Component } from './<%=utils.compName(com,"dash")%>/<%=utils.compName(com,"dash")%>.component';
<%})-%>

@NgModule({
  declarations: [
<%components.forEach(com => {-%>
<%com = com.componentName-%>
    <%=com%>Component,
<%})-%>
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    TableModule,
    FormsModule,
    HttpClientModule,
    AccordionModule,
    BrowserAnimationsModule,
    InputTextModule,
    DropdownModule,
    CalendarModule,
    RadioButtonModule,
    CheckboxModule,
    ButtonModule,
    InputSwitchModule,
    InputTextareaModule,
    KeyFilterModule,
    SliderModule,
    TabViewModule,
    CardModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
