const _ = require("lodash");
const path = require("path");

_.mixin({ pascalcase: _.flow(_.camelCase, _.upperFirst) });

function pascalCase(input) {
  return _.pascalcase(input);
}

function getEntityName(data) {
  return data.project.entities[0].name;
}

function getCapEntityName(data) {
  return pascalCase(data.project.entities[0].name);
}

function getDataType(ele) {
  switch (ele.dataType) {
    case "int":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "int";
      }
    case "integer":
      return "int";
    case "decimal":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "float";
      }
    case "string":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "String";
      }
    case "boolean":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "boolean";
      }
    case "date":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "Date";
      }
    case "time":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "Timestamp";
      }
    case "clob":
      return "Clob";
    case "blob":
      return "byte[]";
    default:
      return ele.dataType;
  }
}

function getDbUrl(data) {
  return data.project.application.database.url;
}

function getDbConImports(data){
  if(data.project.application.database.dbName === "mongo"){
    return `mgo "gopkg.in/mgo.v2"`;
  }else{
    return `"database/sql"
    _ "github.com/go-sql-driver/mysql"`;
  }
}

function getDbRepoImports(data){
  if(data.project.application.database.dbName === "mongo"){
    return `mgo "gopkg.in/mgo.v2"
    "gopkg.in/mgo.v2/bson"`;
  }else{
    return `"database/sql"
    _ "github.com/go-sql-driver/mysql"`;
  }
}

function getPrimaryKey(data){
  let schema = data.project.entities[0].attributes;
  for (let i = 0; i < schema.length; i++) {
    if (schema[i].primaryKey === "true") {
      return schema[i].name;
    }
  }
}

function getCapPrimaryKey(data){
  let schema = data.project.entities[0].attributes;
  for (let i = 0; i < schema.length; i++) {
    if (schema[i].primaryKey === "true") {
      return _.upperFirst(schema[i].name);
    }
  }
}

function getPrimaryDataType(data){
  let schema = data.project.entities[0].attributes;
  for (let i = 0; i < schema.length; i++) {
    if (schema[i].primaryKey === "true") {
      return getDataType(schema[i]);
    }
  }
}

module.exports = {
  getEntityName,
  getCapEntityName,
  getDbUrl,
  getDbConImports,
  getDbRepoImports,
  getPrimaryKey,
  getDataType,
  getPrimaryDataType,
  getCapPrimaryKey,
  pascalCase
};
