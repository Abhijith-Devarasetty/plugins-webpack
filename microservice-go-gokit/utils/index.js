const _ = require("lodash");
const path = require("path");

function getName(name) {
  return _.capitalize(name);
}

function getPackageDirectory(config) {
  let p = config.framework.gin.package_name.replace(/\./g, "//");
  return path.normalize(p);
}

function getPackageName(config){
  return config.framework.gin.package_name
}

function getEntityName(config){
  return config.framework.gin.entities[0].entity;
}

function getCapEntityName(config){
  return _.capitalize(config.framework.gin.entities[0].entity);
}

function getTracing(tracing){
  let result='';
  let trace = Object.keys(tracing);
  switch (trace[0]) {
    case "zipkin":
      result = `
      @Bean
      @LoadBalanced
      public RestTemplate getRestTemplate() {
        return new RestTemplate();
      }
      
      @Bean
      public Sampler defaultSampler() {
        return Sampler.ALWAYS_SAMPLE;
      }`      
      break;
  
    default:
      break;
  }
  return result;
}


module.exports = { getName, getPackageDirectory, getTracing, getPackageName, getEntityName, getCapEntityName };
