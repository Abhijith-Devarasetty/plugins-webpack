const { processData } = require("./processdata.js");

let jsonata = require("jsonata");
module.exports.run = (inputData, lookup, tasks, utils) => {
  //let tasksFile = JSON.parse(fs.readFileSync('tasksFile.json', { encoding: "utf8" }));
  // console.log("from pre processor" + JSON.stringify(inputData));
  let temp = {
    applications:[
      inputData
    ]
  }

  // let entities = jsonata("**.entities{**.taskId: [ **.entity ]}").evaluate(
  //   lookup
  // );
  // tasks = jsonata("**.entities{**.taskId: [ $ ]}").evaluate(lookup);
  console.log("input data",JSON.stringify(inputData));
  let processeData = processData(temp,utils.getRef);
  console.log("processed data",JSON.stringify(processeData));
 
  for(var i=0;i<(inputData.config.framework.gin.entities.length);i++){
    var entity = utils.getRef(inputData.config.framework.gin.entities[i]);
    inputData.config.framework.gin.entities[i] = entity;
  }

  // console.log("tasks",tasks);

  return { tasks, data: processeData };
};
