let jsonata = require("jsonata");

function processData(input,getRef) {
  let framework = jsonata("**.framework.($keys())").evaluate(input);
  let output = {
    project: {
      application: processApplications(input.applications),
      entities: processEntities(input.applications,framework,getRef),
      components: processComponents(input.applications,framework),
    },
  };
  return output;
}

function processApplications(applications) {
  if (applications.length > 0) {
    // let application = [];
    let value;
    applications.forEach((app) => {
      let appName = jsonata("$.appName").evaluate(app);
      value = {
        appName: appName,
        templateId: app.templateId,
        framework: jsonata("$.framework").evaluate(app),
        package: jsonata("**.package_name").evaluate(app),
        port: jsonata("**.port").evaluate(app),
        database: {
          url: jsonata("**.database.**.url").evaluate(app),
          dbName: jsonata("**.database.($keys())").evaluate(app),
          username: "",
          password: "",
        },
        action: jsonata("**.actions").evaluate(app),
        apigen: jsonata("**.apigen").evaluate(app),
        tracing: jsonata("**.tracing").evaluate(app),
        logging: jsonata("**.logging").evaluate(app),
        discovery: jsonata("**.discovery").evaluate(app),
        entities: jsonata("$.**.entities.entity").evaluate(app),
        components: jsonata("$.**.components.componentName").evaluate(app),
        bpmn: jsonata("$.**.bpmnConfig").evaluate(app),
        dmn: jsonata("$.**.dmnConfig").evaluate(app),
      };
    });
    return value;
  } else {
    return {};
  }
}

function processEntities(applications,framework,getRef) {
  if (jsonata("**."+ framework +".entities").evaluate(applications) != undefined) {
    let entityArray = [];
    let entities = jsonata("**." + framework + ".entities").evaluate(applications);
    entities.forEach((entity) => {
      entity = getRef(entity);
      // console.log("entity",entity);
      let value = {
        name: entity.entity,
        isCompositeKey: jsonata("$count(schema.meta[pk ='true'])>1").evaluate(
          entity
        ),
        // tableName: jsonata("**.taskId").evaluate(entity),
        taskId: jsonata("**.taskId").evaluate(entity),
        attributes: processAttributes(entity.schema),
        relations: jsonata("**.relations").evaluate(entity)
      };
      // console.log("entity",value);
      
      entityArray.push( value );
    });
    return entityArray;
  } else {
    return [];
  }
}

function processAttributes(schema) {
  let result = [];
  schema.forEach((ele) => {
    result.push({
      name: ele.attribute,
      dataType: ele.dataType,
      actualDataType : ele.meta.dataType != undefined ? ele.meta.dataType : "",
      columnName: ele.meta.column != undefined ? ele.meta.column : "",
      primaryKey: ele.meta.pk ? "true" : "fasle",
    });
  });
  return result;
}

function processComponents(applications,framework) {
  if (jsonata("**.components").evaluate(applications) != undefined) {
    let components = jsonata("**.components").evaluate(applications);
    let arr = [];
    components.forEach((comp) => {
      let appName = jsonata("$.componentName").evaluate(comp);
      let value = {
        name: appName,
        fields: processFields(comp),
      };
      arr.push( value );
    });
    return arr;
  } else {
    return [];
  }
}

function processFields(comp) {
  let arr = [];
  comp = jsonata("**.elements").evaluate(comp);
  comp.forEach((field) => {
    console.log(field);
    arr.push({
      element: field.input_type,
      label: field.label,
      name: field.name,
      model: field.model,
    });
  });
  return arr;
}

function processEnums(input) {}

function processResources(input) {}

module.exports = { processData };
