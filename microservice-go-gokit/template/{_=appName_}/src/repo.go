package main
<%let entity = utils.getEntityName(data)-%>
<%let schema = data.project.entities[0].attributes-%>

import (
	"context"
	
	"errors"
	"fmt"
	"github.com/go-kit/kit/log"
	<%-utils.getDbRepoImports(data)%>
)
<%let dbname = data.project.application.database.dbName%>
var RepoErr = errors.New("Unable to handle Repo Request")

const UserCollection = "<%=entity%>"

<%if(dbname === "mongo"){-%>
type repo struct {
	db     *mgo.Database
	logger log.Logger
}

func NewRepo(db *mgo.Database, logger log.Logger) (Repository, error) {
	return &repo{
		db:     db,
		logger: log.With(logger, "repo", "mongodb"),
	}, nil
}

func (repo *repo) Create<%=utils.pascalCase(entity)%>(ctx context.Context, <%=entity%> <%=utils.pascalCase(entity)%>) error {
	fmt.Println("Create <%=entity%> in db called")
	err := db.C(UserCollection).Insert(<%=entity%>)
	if err != nil {
		fmt.Println("Error occured inside Create<%=utils.pascalCase(entity)%>")
		return err
	} else {
		fmt.Println("<%=utils.pascalCase(entity)%> Created:", err)
	}
	return nil
}

func (repo *repo) Get<%=utils.pascalCase(entity)%>ById(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>) (interface{}, error) {
	coll := db.C(UserCollection)
	data := []<%=utils.pascalCase(entity)%>{}
	err := coll.Find(bson.M{"<%=utils.toLower(utils.getPrimaryKey(data))%>": <%=utils.getPrimaryKey(data)%>}).Select(bson.M{}).All(&data)
	if err != nil {
		fmt.Println("Error occured inside Get<%=entity%>ById in repo")
		return "", err
	}
	return data, nil
}


func (repo *repo) GetAll<%=utils.pascalCase(entity)%>s(ctx context.Context) (interface{}, error) {
	coll := db.C(UserCollection)
	<%=entity%> := []<%=utils.pascalCase(entity)%>{}
	err := coll.Find(bson.M{}).Select(bson.M{}).All(&<%=entity%>)
	if err != nil {
		fmt.Println("Error occured inside Get<%=entity%>ById in repo")
		return "", err
	}
	return <%=entity%>, nil
}

func (repo *repo) Delete<%=utils.pascalCase(entity)%>(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>) (string, error) {
	coll := db.C(UserCollection)
	err := coll.Remove(bson.M{"<%=utils.toLower(utils.getPrimaryKey(data))%>": <%=utils.getPrimaryKey(data)%>})
	if err != nil {
		fmt.Println("Error occured inside delete in repo")
		return "", err
	} else {
		msg := "<%=entity%> deleted successfully"
		return msg, nil
	}
}

func (repo *repo) Update<%=utils.pascalCase(entity)%>(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>, <%=entity%> <%=utils.pascalCase(entity)%>) error {
	coll := db.C(UserCollection)
	err := coll.Update(bson.M{"<%=utils.toLower(utils.getPrimaryKey(data))%>": <%=utils.getPrimaryKey(data)%>}, bson.M{"$set": bson.M{<% -%>
<%data.project.entities[0].attributes.forEach(e=>{ %>"<%=utils.camelCase(e.name)%>": <%=entity%>.<%=utils.upperFirst(e.name)%>,<%}) %>}})
	if err != nil {
		fmt.Println("Error occured inside update <%=entity%> repo")
		return err
	} else {
		return nil
	}

}
<%}else{-%>
<%let len = 0%>

type sqlrepo struct {
	sqldb  *sql.DB
	logger log.Logger
}

func NewSqlRepo(sqldb *sql.DB, logger log.Logger) (Repository, error) {
	return &sqlrepo{
		sqldb:  sqldb,
		logger: log.With(logger, "sqlrepo", "sqldb"),
	}, nil
}

func (sqlrepo *sqlrepo) Create<%=utils.pascalCase(entity)%>(ctx context.Context, <%=entity%> <%=utils.pascalCase(entity)%>) error {
	fmt.Println("create <%=entity%> sql repo", sqldb)
	dbt := sqlrepo.sqldb
	sqlresp, err := dbt.Prepare("INSERT INTO <%=entity%> (<%len = data.project.entities[0].attributes.length%><%data.project.entities[0].attributes.forEach(e=>{ %><%len = len-1%><%=e.name%><%if(len!==0){%>,<%}%><%}) %>) VALUES (<%len = data.project.entities[0].attributes.length%><%data.project.entities[0].attributes.forEach(e=>{ %><%len = len-1%>?<%if(len!==0){%>,<%}%><%}) %>)")
	sqlresp.Exec(<%len = data.project.entities[0].attributes.length%><%data.project.entities[0].attributes.forEach(e=>{ %><%len = len-1%><%=entity%>.<%=e.name%><%if(len!==0){%>,<%}%><%}) %>)
	fmt.Println("<%=entity%> Created:", sqlresp, err)
	return nil
}

func (sqlrepo *sqlrepo) Get<%=utils.pascalCase(entity)%>ById(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>) (interface{}, error) {
	<%=entity%> := <%=utils.pascalCase(entity)%>{}
	dbt := sqlrepo.sqldb
	err := dbt.QueryRow("SELECT * FROM <%=entity%> where <%=utils.getCapPrimaryKey(data)%> = ?", <%=utils.getPrimaryKey(data)%>).Scan(<%len = data.project.entities[0].attributes.length%><%data.project.entities[0].attributes.forEach(e=>{ %><%len = len-1%>&<%=entity%>.<%=e.name%><%if(len!==0){%>,<%}%><%}) %>)
	if err != nil {
		fmt.Println("Error occured inside Create in repo")
		return "", err
	}
	return <%=entity%>, nil
}

func (sqlrepo *sqlrepo) GetAll<%=utils.pascalCase(entity)%>s(ctx context.Context) (interface{}, error) {
	<%=entity%> := <%=utils.pascalCase(entity)%>{}
	var res []interface{}
	rows, err := sqlrepo.sqldb.QueryContext(ctx, "SELECT <%len = data.project.entities[0].attributes.length%><%data.project.entities[0].attributes.forEach(e=>{ %><%len = len-1%><%=entity%>.<%=e.name%><%if(len!==0){%>,<%}%><%}) %> FROM <%=entity%>")
	if err != nil {
		if err == sql.ErrNoRows {
			return <%=entity%>, err
		}
		return <%=entity%>, err
	}

	defer rows.Close()
	for rows.Next() {
		err = rows.Scan(<%len = data.project.entities[0].attributes.length%><%data.project.entities[0].attributes.forEach(e=>{ %><%len = len-1%>&<%=entity%>.<%=e.name%><%if(len!==0){%>,<%}%><%}) %>)
		res = append([]interface{}{<%=entity%>}, res...)
	}
	return res, nil
}

func (sqlrepo *sqlrepo) Delete<%=utils.pascalCase(entity)%>(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>) (string, error) {
	dbt := sqlrepo.sqldb
	sqlresp, err := dbt.Query("DELETE FROM <%=entity%> where <%=utils.getCapPrimaryKey(data)%>=?", <%=utils.getPrimaryKey(data)%>)
	fmt.Println("", sqlresp, err)

	return "", nil
}

func (sqlrepo *sqlrepo) Update<%=utils.pascalCase(entity)%>(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>, <%=entity%> <%=utils.pascalCase(entity)%>) error {
	dbt:=sqlrepo.sqldb
	sqlresp,err:=dbt.Query("update <%=entity%> set  <%len = data.project.entities[0].attributes.length%><%data.project.entities[0].attributes.forEach(e=>{ %><%len = len-1%><%=e.name%>=?<%if(len!==0){%>,<%}%><%}) %> where <%=utils.getCapPrimaryKey(data)%>=?",<%data.project.entities[0].attributes.forEach(e=>{ %><%=entity%>.<%=e.name%>,<%}) %><%=utils.getPrimaryKey(data)%>)
	fmt.Println("",sqlresp,err)
	return nil
}

<%}-%>