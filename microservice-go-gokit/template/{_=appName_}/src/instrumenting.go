package main
<%let entity = utils.getEntityName(data)%>
import (
	"context"
	"fmt"
	"time"

	"github.com/go-kit/kit/metrics"
)

type instrumentingMiddleware struct {
	requestCount   metrics.Counter
	requestLatency metrics.Histogram
	countResult    metrics.Histogram
	next           <%=appName%>
}

func (mw instrumentingMiddleware) Create(ctx context.Context, <%=entity%> <%=utils.pascalCase(entity)%>) (output string, err error) {
	defer func(begin time.Time) {
		lvs := []string{"method", "create", "error", fmt.Sprint(err != nil)}
		mw.requestCount.With(lvs...).Add(1)
		mw.requestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	output, err = mw.next.Create(ctx, <%=entity%>)
	return
}

func (mw instrumentingMiddleware) Get<%=utils.pascalCase(entity)%>ById(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>) (data interface{}, err error) {
	defer func(begin time.Time) {
		lvs := []string{"method", "Get<%=utils.pascalCase(entity)%>ById", "error", fmt.Sprint(err != nil)}
		mw.requestCount.With(lvs...).Add(1)
		mw.requestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	data, err = mw.next.Get<%=utils.pascalCase(entity)%>ById(ctx, <%=utils.getPrimaryKey(data)%>)
	return
}

func (mw instrumentingMiddleware) GetAll<%=utils.pascalCase(entity)%>s(ctx context.Context) (Data interface{}, err error) {
	defer func(begin time.Time) {
		lvs := []string{"method", "GetAll<%=utils.pascalCase(entity)%>s", "error", fmt.Sprint(err != nil)}
		mw.requestCount.With(lvs...).Add(1)
		mw.requestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	Data, err = mw.next.GetAll<%=utils.pascalCase(entity)%>s(ctx)
	return
}

func (mw instrumentingMiddleware) Delete<%=utils.pascalCase(entity)%>(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>) (msg string, err error) {
	defer func(begin time.Time) {
		lvs := []string{"method", "Delete<%=utils.pascalCase(entity)%>", "error", fmt.Sprint(err != nil)}
		mw.requestCount.With(lvs...).Add(1)
		mw.requestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	msg, err = mw.next.Delete<%=utils.pascalCase(entity)%>(ctx, <%=utils.getPrimaryKey(data)%>)
	return
}

func (mw instrumentingMiddleware) Update<%=utils.pascalCase(entity)%>(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>, <%=entity%> <%=utils.pascalCase(entity)%>) (err error) {
	defer func(begin time.Time) {
		lvs := []string{"method", "Update<%=utils.pascalCase(entity)%>", "error", fmt.Sprint(err != nil)}
		mw.requestCount.With(lvs...).Add(1)
		mw.requestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	}(time.Now())

	err = mw.next.Update<%=utils.pascalCase(entity)%>(ctx, <%=utils.getPrimaryKey(data)%>, <%=entity%>)
	return
}
