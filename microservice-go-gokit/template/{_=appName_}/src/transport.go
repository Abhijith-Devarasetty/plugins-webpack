package main
<%let entity = utils.getEntityName(data)%>
import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-kit/kit/endpoint"
	"github.com/gorilla/mux"
)

func Add<%=utils.pascalCase(entity)%>Endpoint(s <%=appName%>) endpoint.Endpoint {
	fmt.Println("createEndpoint called")
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(CreateRequest)
		msg, err := s.Create(ctx, req.<%=utils.pascalCase(entity)%>)
		return CreateResponse{Msg: msg, Err: err}, nil
	}
}

func Get<%=utils.pascalCase(entity)%>ByIdEndpoint(s <%=appName%>) endpoint.Endpoint {
	fmt.Println("Get ById Endpoint called")
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(Get<%=utils.pascalCase(entity)%>ByIdRequest)
		id, er := strconv.Atoi(req.<%=utils.getCapPrimaryKey(data)%>)
		if er != nil {
			return Get<%=utils.pascalCase(entity)%>ByIdResponse{Data: "", Err: er}, nil
		}
		<%=entity%>, err := s.Get<%=utils.pascalCase(entity)%>ById(ctx, id)
		return Get<%=utils.pascalCase(entity)%>ByIdResponse{Data: <%=entity%>, Err: err}, nil
	}

}

func GetAll<%=utils.pascalCase(entity)%>sEndpoint(s <%=appName%>) endpoint.Endpoint {
	fmt.Println("GetAll Endpoint called")
	return func(ctx context.Context, request interface{}) (interface{}, error) {

		<%=entity%>, err := s.GetAll<%=utils.pascalCase(entity)%>s(ctx)
		return GetAll<%=utils.pascalCase(entity)%>sResponse{Data: <%=entity%>, Err: err}, nil
	}

}

func Delete<%=utils.pascalCase(entity)%>Endpoint(s <%=appName%>) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(Delete<%=utils.pascalCase(entity)%>Request)
		fmt.Println("Delete Endpoint called", req)
		id, er := strconv.Atoi(req.<%=utils.getCapPrimaryKey(data)%>)
		if er != nil {
			return Delete<%=utils.pascalCase(entity)%>Response{Msg: "", Err: er}, nil
		}
		msg, err := s.Delete<%=utils.pascalCase(entity)%>(ctx, id)
		return Delete<%=utils.pascalCase(entity)%>Response{Msg: msg, Err: err}, nil
	}

}

func Update<%=utils.pascalCase(entity)%>Endpoint(s <%=appName%>) endpoint.Endpoint {
	fmt.Println("Update Endpoint called")
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(Update<%=utils.pascalCase(entity)%>Request)
		rc := req.<%=entity%>
		err := s.Update<%=utils.pascalCase(entity)%>(ctx, rc.<%=utils.getCapPrimaryKey(data)%>, req.<%=entity%>)
		return Update<%=utils.pascalCase(entity)%>Response{Err: err}, nil
	}
}

func decodeCreateRequest(_ context.Context, r *http.Request) (interface{}, error) {
	fmt.Println("decode create request called")
	var request CreateRequest
	if err := json.NewDecoder(r.Body).Decode(&request.<%=utils.pascalCase(entity)%>); err != nil {
		return nil, err
	}
	return request, nil
}

func decodeGet<%=utils.pascalCase(entity)%>ByIdRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req Get<%=utils.pascalCase(entity)%>ByIdRequest
	vars := mux.Vars(r)
	req = Get<%=utils.pascalCase(entity)%>ByIdRequest{
		<%=utils.getCapPrimaryKey(data)%>: vars["<%=utils.getPrimaryKey(data)%>"],
	}

	return req, nil
}

func decodeGetAll<%=utils.pascalCase(entity)%>sRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req GetAll<%=utils.pascalCase(entity)%>sRequest

	return req, nil
}

func decodeDelete<%=utils.pascalCase(entity)%>Request(_ context.Context, r *http.Request) (interface{}, error) {
	var req Delete<%=utils.pascalCase(entity)%>Request
	vars := mux.Vars(r)
	req = Delete<%=utils.pascalCase(entity)%>Request{
		<%=utils.getCapPrimaryKey(data)%>: vars["<%=utils.getPrimaryKey(data)%>"],
	}

	return req, nil
}

func decodeUpdate<%=utils.pascalCase(entity)%>Request(_ context.Context, r *http.Request) (interface{}, error) {
	var req Update<%=utils.pascalCase(entity)%>Request

	if err := json.NewDecoder(r.Body).Decode(&req.<%=entity%>); err != nil {
		return nil, err
	}
	return req, nil

}


func encodeResponse(ctx context.Context, w http.ResponseWriter, response interface{}) error {
	fmt.Println("encodeResponse Called")
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	return json.NewEncoder(w).Encode(response)
}

type (
	CreateRequest struct {
		<%=utils.pascalCase(entity)%> <%=utils.pascalCase(entity)%>
	}
	
	CreateResponse struct {
		Msg  string `json:"msg"`
		Err error  `json:"error,omitempty"`
	}
	Get<%=utils.pascalCase(entity)%>ByIdRequest struct {
		<%=utils.getCapPrimaryKey(data)%> string `json:"<%=utils.getPrimaryKey(data)%>"`
	}
	Get<%=utils.pascalCase(entity)%>ByIdResponse struct {
		Data interface{} `json:"<%=entity%>"`
		Err   error  `json:"error,omitempty"`
	}
	GetAll<%=utils.pascalCase(entity)%>sRequest struct {
	}
	GetAll<%=utils.pascalCase(entity)%>sResponse struct {
		Data interface{} `json:"<%=entity%>"`
		Err   error       `json:"error,omitempty"`
	}
	Delete<%=utils.pascalCase(entity)%>Request struct {
		<%=utils.getCapPrimaryKey(data)%> string `json:"<%=utils.getPrimaryKey(data)%>"`
	}
	Delete<%=utils.pascalCase(entity)%>Response struct {
		Msg string `json:"msg,omitempty"`
		Err error  `json:"error,omitempty"`
	}
	Update<%=utils.pascalCase(entity)%>Request struct {
		ID    string `json:"<%=utils.getPrimaryKey(data)%>"`
		<%=entity%> <%=utils.pascalCase(entity)%>
	}
	Update<%=utils.pascalCase(entity)%>Response struct {
		// Msg string `json:"msg,omitempty"`
		Err error `json:"error,omitempty"`
	}
)
