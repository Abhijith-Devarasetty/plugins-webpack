Instructions To Execute the Service

Run go Service: go build

-Then it will generate an executable file named as OrderService, execute OrderService.exe file

Running on port : 8083

POST Request : /createOrder

Model data:

    "customer_id":"101",
    "status":"ordered",
    "restaurant_id":"201",
    "order_items":[
				{
					"product_code":"301",
					"name":"Biryani",
					"unit_price":150,
					"quantity":1
				}
	          ]


