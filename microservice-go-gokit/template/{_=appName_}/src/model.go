package main
<%let entity = utils.getEntityName(data)%>
import "context"

type <%=utils.pascalCase(entity)%> struct {
	<%data.project.entities[0].attributes.forEach(e=>{ %>
		<%=utils.upperFirst(e.name)%>  <%=e.dataType%>  `json:"<%=e.name%>"`
	<%}) %>
}

type Repository interface {
	Create<%=utils.pascalCase(entity)%>(ctx context.Context, <%=entity%> <%=utils.pascalCase(entity)%>) error
	Get<%=utils.pascalCase(entity)%>ById(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>) (interface{}, error)
	GetAll<%=utils.pascalCase(entity)%>s(ctx context.Context) (interface{}, error)
	Delete<%=utils.pascalCase(entity)%>(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>) (string, error)
	Update<%=utils.pascalCase(entity)%>(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>, <%=entity%> <%=utils.pascalCase(entity)%>) error
}
