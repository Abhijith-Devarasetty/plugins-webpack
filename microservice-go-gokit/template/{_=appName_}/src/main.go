package main
<%let entity = utils.getEntityName(data)-%>
<%let port = data.project.application.port-%>
<%let dbname = data.project.application.database.dbName-%>

import (
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	httptransport "github.com/go-kit/kit/transport/http"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	logger := log.NewLogfmtLogger(os.Stderr)

	r := mux.NewRouter()

	fieldKeys := []string{"method", "error"}
	requestCount := kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
		Namespace: "my_group",
		Subsystem: "<%=appName%>",
		Name:      "request_count",
		Help:      "Number of requests received.",
	}, fieldKeys)
	requestLatency := kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
		Namespace: "my_group",
		Subsystem: "<%=appName%>",
		Name:      "request_latency_microseconds",
		Help:      "Total duration of requests in microseconds.",
	}, fieldKeys)
	countResult := kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
		Namespace: "my_group",
		Subsystem: "<%=appName%>",
		Name:      "count_result",
		Help:      "The result of each count method.",
	}, []string{})

	// DB Connection


	// var svcs <%=appName%>
	var svc <%=appName%>
	{
<%if(dbname === 'mysql'){-%>
		sqldb := GetSqlDB()
		repository, err := NewSqlRepo(sqldb, logger)
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}
		svc = NewService(repository, logger)
<%}-%>	
<%if(dbname === 'mongo'){-%>
		db := GetMongoDB()
		repository, err := NewRepo(db, logger)
		if err != nil {
			level.Error(logger).Log("exit", err)
			os.Exit(-1)
		}
		svc = NewService(repository, logger)
<%}-%>
	}
	svc = loggingMiddleware{logger, svc}
	svc = instrumentingMiddleware{requestCount, requestLatency, countResult, svc}

	Add<%=utils.pascalCase(entity)%>Handler := httptransport.NewServer(
		Add<%=utils.pascalCase(entity)%>Endpoint(svc),
		decodeCreateRequest,
		encodeResponse,
	)
	GetByIdHandler := httptransport.NewServer(
		Get<%=utils.pascalCase(entity)%>ByIdEndpoint(svc),
		decodeGet<%=utils.pascalCase(entity)%>ByIdRequest,
		encodeResponse,
	)
	GetAll<%=utils.pascalCase(entity)%>sHandler := httptransport.NewServer(
		GetAll<%=utils.pascalCase(entity)%>sEndpoint(svc),
		decodeGetAll<%=utils.pascalCase(entity)%>sRequest,
		encodeResponse,
	)
	Delete<%=utils.pascalCase(entity)%>Handler := httptransport.NewServer(
		Delete<%=utils.pascalCase(entity)%>Endpoint(svc),
		decodeDelete<%=utils.pascalCase(entity)%>Request,
		encodeResponse,
	)
	Update<%=utils.pascalCase(entity)%>Handler := httptransport.NewServer(
		Update<%=utils.pascalCase(entity)%>Endpoint(svc),
		decodeUpdate<%=utils.pascalCase(entity)%>Request,
		encodeResponse,
	)
	http.Handle("/", r)
	http.Handle("/create", Add<%=utils.pascalCase(entity)%>Handler)
	r.Handle("/getbyid/{<%=utils.getPrimaryKey(data)%>}", GetByIdHandler).Methods("GET")
	r.Handle("/delete/{<%=utils.getPrimaryKey(data)%>}", Delete<%=utils.pascalCase(entity)%>Handler).Methods("GET")
	r.Handle("/update", Update<%=utils.pascalCase(entity)%>Handler).Methods("PUT")
	r.Handle("/getall", GetAll<%=utils.pascalCase(entity)%>sHandler).Methods("GET")
	http.Handle("/metrics", promhttp.Handler())
	logger.Log("msg", "HTTP", "addr", ":<%=port%>")
	logger.Log("err", http.ListenAndServe(":<%=port%>", nil))
}
