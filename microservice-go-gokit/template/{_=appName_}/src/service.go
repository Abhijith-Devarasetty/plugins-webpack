package main
<%let entity = utils.getEntityName(data)%>
import (
	"context"
	"errors"
	"fmt"

	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
)

type <%=entity%>Struct struct {
	repository Repository
	logger     log.Logger
}

type <%=appName%> interface {
	Create(ctx context.Context, <%=entity%> <%=utils.pascalCase(entity)%>) (string, error)
	Get<%=utils.pascalCase(entity)%>ById(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>) (interface{}, error)
	GetAll<%=utils.pascalCase(entity)%>s(ctx context.Context) (interface{}, error)
	Delete<%=utils.pascalCase(entity)%>(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>) (string, error)
	Update<%=utils.pascalCase(entity)%>(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>, <%=entity%> <%=utils.pascalCase(entity)%>) error
}

var (
	ErrOrderNotFound   = errors.New("<%=entity%> not found")
	ErrCmdRepository   = errors.New("unable to command repository")
	ErrQueryRepository = errors.New("unable to query repository")
)

func NewService(rep Repository, logger log.Logger) <%=appName%> {
	return &<%=entity%>Struct{
		repository: rep,
		logger:     logger,
	}
}

func (o <%=entity%>Struct) Create(ctx context.Context, <%=entity%> <%=utils.pascalCase(entity)%>) (string, error) {
	fmt.Println("Crete method implementation called")
	logger := log.With(o.logger, "method", "Create")
	<%=entity%>s := <%=utils.pascalCase(entity)%>{
	<%data.project.entities[0].attributes.forEach(e=>{ %>
		<%=utils.upperFirst(e.name)%>:  <%=entity%>.<%=utils.upperFirst(e.name)%>,
	<%}) %>
	}

	if err := o.repository.Create<%=utils.pascalCase(entity)%>(ctx, <%=entity%>s); err != nil {
		fmt.Println("error generated")
		level.Error(logger).Log("err", err)
		return "", err
	}

	msg := "<%=entity%> Created"
	return msg, nil

}

func (s <%=entity%>Struct) Get<%=utils.pascalCase(entity)%>ById(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>) (interface{}, error) {
	logger := log.With(s.logger, "method", "Get<%=utils.pascalCase(entity)%>ById")
	var data interface{}
	data, err := s.repository.Get<%=utils.pascalCase(entity)%>ById(ctx, <%=utils.getPrimaryKey(data)%>)
	if err != nil {
		level.Error(logger).Log("err", err)

		return "", err
	}
	return data, nil
}

func (s <%=entity%>Struct) GetAll<%=utils.pascalCase(entity)%>s(ctx context.Context) (interface{}, error) {
	logger := log.With(s.logger, "method", "GetAll<%=utils.pascalCase(entity)%>s")
	var Data interface{}
	Data, err := s.repository.GetAll<%=utils.pascalCase(entity)%>s(ctx)
	if err != nil {
		level.Error(logger).Log("err", err)
		return "", err
	}
	return Data, nil
}


func (s <%=entity%>Struct) Delete<%=utils.pascalCase(entity)%>(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>) (string, error) {
	logger := log.With(s.logger, "method", "Delete<%=utils.pascalCase(entity)%>")
	msg, err := s.repository.Delete<%=utils.pascalCase(entity)%>(ctx, <%=utils.getPrimaryKey(data)%>)
	if err != nil {
		level.Error(logger).Log("err", err)

		return "", err
	}
	return msg, nil
}

func (s <%=entity%>Struct) Update<%=utils.pascalCase(entity)%>(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>,<%=entity%> <%=utils.pascalCase(entity)%>) error {
	logger := log.With(s.logger, "method", "ChangeDetails")
	err := s.repository.Update<%=utils.pascalCase(entity)%>(ctx, <%=utils.getPrimaryKey(data)%>, <%=entity%>)
	if err != nil {
		level.Error(logger).Log("err", err)
		return err
	} else {
		return nil
	}

}
