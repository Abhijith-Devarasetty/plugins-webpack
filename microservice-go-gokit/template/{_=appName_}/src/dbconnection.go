package main

import (
	"fmt"
	"os"
	<%-utils.getDbConImports(data)%>
)

<%let dbname = data.project.application.database.dbName-%>

<%if(dbname === 'mongo'){-%>
// GetMongoDB function to return DB connection
var db *mgo.Database

func GetMongoDB() *mgo.Database {
	dbName := "test"
	session, err := mgo.Dial("<%=utils.getDbUrl(data)%>")
	if err != nil {
		fmt.Println("session err:", err)
		os.Exit(2)
	}
	db = session.DB(dbName)
	return db
}
<%}-%>

<%if(dbname === 'mysql'){-%>
// GetMysql function to return DB connection
var sqldb *sql.DB

func GetSqlDB() *sql.DB {
	var err error
	sqldb, err := sql.Open("mysql", "<%=utils.getDbUrl(data)%>")
	if err != nil {
		panic(err.Error())
	}

	return sqldb
}
<%}-%>
