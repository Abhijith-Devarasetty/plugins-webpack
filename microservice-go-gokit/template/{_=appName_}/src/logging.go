package main
<%let entity = utils.getEntityName(data)%>
import (
	"context"
	"time"

	"github.com/go-kit/kit/log"
)

type loggingMiddleware struct {
	logger log.Logger
	next   <%=appName%>
}

func (mw loggingMiddleware) Create(ctx context.Context, <%=entity%> <%=utils.pascalCase(entity)%>) (msg string, err error) {
	defer func(begin time.Time) {
		mw.logger.Log(
			"method", "Create",
			"took", time.Since(begin),
			"err", err)
	}(time.Now())
	return mw.next.Create(ctx, <%=entity%>)
}

func (mw loggingMiddleware) Get<%=utils.pascalCase(entity)%>ById(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>) (data interface{}, Err error) {
	defer func(begin time.Time) {

		_ = mw.logger.Log(
			"method", "GetbyId",
			"<%=utils.getPrimaryKey(data)%>", <%=utils.getPrimaryKey(data)%>,
			"err", Err,
			"took", time.Since(begin),
		)
	}(time.Now())

	data, Err = mw.next.Get<%=utils.pascalCase(entity)%>ById(ctx, <%=utils.getPrimaryKey(data)%>)
	return
}

func (mw loggingMiddleware) GetAll<%=utils.pascalCase(entity)%>s(ctx context.Context) (Email interface{}, Err error) {
	defer func(begin time.Time) {

		_ = mw.logger.Log(
			"method", "GetAll<%=utils.pascalCase(entity)%>s",
			"err", Err,
			"took", time.Since(begin),
		)
	}(time.Now())

	Email, Err = mw.next.GetAll<%=utils.pascalCase(entity)%>s(ctx)
	return
}
func (mw loggingMiddleware) Delete<%=utils.pascalCase(entity)%>(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>) (Msg string, Err error) {
	defer func(begin time.Time) {

		_ = mw.logger.Log(
			"method", "Delete<%=utils.pascalCase(entity)%>",
			"<%=utils.getPrimaryKey(data)%>", <%=utils.getPrimaryKey(data)%>,
			"err", Err,
			"took", time.Since(begin),
		)
	}(time.Now())

	Msg, Err = mw.next.Delete<%=utils.pascalCase(entity)%>(ctx, <%=utils.getPrimaryKey(data)%>)

	return
}

func (mw loggingMiddleware) Update<%=utils.pascalCase(entity)%>(ctx context.Context, <%=utils.getPrimaryKey(data)%> <%=utils.getPrimaryDataType(data)%>, <%=entity%> <%=utils.pascalCase(entity)%>) (Err error) {
	defer func(begin time.Time) {

		_ = mw.logger.Log(
			"method", "update_<%=entity%>",
			"Msg", "Data updated",
			"err", Err,
			"took", time.Since(begin),
		)
	}(time.Now())

	Err = mw.next.Update<%=utils.pascalCase(entity)%>(ctx, <%=utils.getPrimaryKey(data)%>, <%=entity%>)
	return
}
