const _ = require("lodash");
const path = require("path");
let jsonata = require("jsonata");

_.mixin({ pascalcase: _.flow(_.camelCase, _.upperFirst) });

function pascalCase(input) {
  return _.pascalcase(input);
}

function getDirectory(data) {
  let p = data.project.application.package.replace(/\./g, "//");
  return path.normalize(p);
}

function getPackageName(data) {
  return data.project.application.package;
}

function getEntityName(data) {
  return data.project.entities[0].name;
}

function getCapEntityName(data) {
  let entityName = data.project.entities[0].name;
  return pascalCase(entityName);
}

function getPackageDirectory(data) {
  let p = data.project.application.package.replace(/\./g, "//");
  return path.normalize(p);
}

function getDataType(ele) {
  switch (ele.dataType) {
    case "int":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "Int";
      }
    case "integer":
      return "int";
    case "decimal":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "float";
      }
    case "string":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "String";
      }
    case "boolean":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "boolean";
      }
    case "date":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "Date";
      }
    case "time":
      if (ele.actualDataType != undefined && ele.actualDataType != "") {
        let arr = ele.actualDataType.split(".");
        return arr[arr.length - 1];
      } else {
        return "Timestamp";
      }
    case "clob":
      return "Clob";
    case "blob":
      return "byte[]";
    default:
      return ele.dataType;
  }
}

function getImports(attributes) {
  let set = new Set();
  let result = "";
  attributes.forEach((ele) => {
    if (!_.isEmpty(ele.actualDataType)) {
      set.add("import " + ele.actualDataType + ";");
    }
    if (ele.dataType == "clob") {
      set.add("import java.sql.Clob;");
    }
  });
  for (let temp of set) {
    result =
      result +
      temp +
      `
`;
  }
  return result;
}

function checkJpa(data) {
  if (data.project.application.database.dbName != "mongo") {
    return `
  <dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-data-jpa</artifactId>
  </dependency>`;
  }
}

function getDataBase(data) {
  switch (data.project.application.database.dbName) {
    case "h2":
      return `
      <dependency>
			    <groupId>com.h2database</groupId>
			    <artifactId>h2</artifactId>
			    <scope>runtime</scope>
		  </dependency>`;

    case "mysql":
      return `
      <dependency>
			    <groupId>mysql</groupId>
			    <artifactId>mysql-connector-java</artifactId>
			    <scope>runtime</scope>
      </dependency>`;

    case "mongo":
      return `
      <dependency>
			    <groupId>org.springframework.boot</groupId>
			    <artifactId>spring-boot-starter-data-mongodb</artifactId>
      </dependency>`;

    case "postgresql":
      return `
      <dependency>
			    <groupId>org.postgresql</groupId>
			    <artifactId>postgresql</artifactId>
			    <scope>runtime</scope>
		  </dependency>`;
  }
}


function getDataBaseProperties(data) {
  let path = data.project.application.database.url;
  switch (data.project.application.database.dbName) {
    case "h2":
      return `
spring.h2.console.enabled=true
spring.database.platform=h2
spring.database.url=${path}`;

    case "mysql":
      return `
spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver
spring.datasource.url=${path}
spring.datasource.username= 
spring.datasource.password=
spring.jpa.show-sql=true
spring.jpa.hibernate.ddl-auto=update
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.MySQL5Dialect`;

    case "mongo":
      return `spring.data.mongo.uri=${path}`;

    case "postgresql":
      return `
spring.jpa.database=POSTGRESQL
spring.datasource.platform=postgres
spring.datasource.url=${path}
spring.datasource.username= 
spring.datasource.password= 
spring.jpa.show-sql=true
spring.jpa.generate-ddl=true
spring.jpa.hibernate.ddl-auto=update
spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true`;
  }
}

function getRepository(data) {
  switch (data.project.application.database.dbName) {
    case "mongo":
      return "MongoRepository";
    default:
      return "JpaRepository";
  }
}

function repoPrimary(schema, entity) {
  if (jsonata("$count(*[primaryKey ='true'])>1").evaluate(schema)) {
    return pascalCase(entity) + "Id";
  }
  if (jsonata("$count(*[primaryKey ='true'])<1").evaluate(schema)) {
    return "Long";
  }
  for (let i = 0; i < schema.length; i++) {
    if (schema[i].primaryKey === "true") {
      switch (getDataType(schema[i])) {
        case "Int":
          return "Integer";
        case "long":
          return "Long";
        case "Long":
          return "Long";
        default:
          return "Long";
      }
    }
  }
}

function getPrimaryDataType(data) {
  let attributes = data.project.entities[0].attributes;
  for (let i = 0; i < attributes.length; i++) {
    if (attributes[i].primaryKey === "true") {
      return getDataType(attributes[i]);
    }
  }
}

module.exports = {
  getPackageDirectory,
  getPackageName,
  getEntityName,
  getCapEntityName,
  getDataBaseProperties,
  getDataType,
  getImports,
  getRepository,
  repoPrimary,
  getPrimaryDataType,
  getDirectory,
  pascalCase,
  checkJpa,
  getDataBase
};
