package <%=utils.getPackageName(data)%>.model

import java.util.*;
import java.sql.*;
<%=utils.getImports(attributes)%>
<%if(data.project.application.database.dbName != "mongo"){-%>
import javax.persistence.*;
<%}else{-%>
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
<%}-%>

<%if(data.project.application.database.dbName != "mongo"){-%>
@Entity
<%if(meta){-%><%if(meta.tableName !== undefined){-%>
@Table( name = "<%=meta.tableName%>")
<%}-%><%}-%>
<%}else{-%>
@Document
<%}-%>
data class <%=utils.pascalCase(name)%>(

<%let len = attributes.length-%>
<%attributes.forEach(e=>{ -%>
<%len = len-1-%>
<%if(e.primaryKey === 'true'){-%>
	@Id
<%}-%>
<%if(e.dataType == "blob" | e.dataType == "clob"){-%>
	@Lob
<%}-%>
<%if(e.columnName != ''){-%>
	@Column(name="<%=e.meta.column%>")
<%}-%>
    var <%=e.name%>: <%=utils.getDataType(e)%> <%if(len!==0){%>,<%}%>
<%})-%>
)
