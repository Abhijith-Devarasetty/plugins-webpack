package <%=utils.getPackageName(data)%>.repository

<%if(data.project.application.database.dbName != "mongo"){-%>
import org.springframework.data.jpa.repository.JpaRepository;
<%}else{-%>
import org.springframework.data.mongodb.repository.MongoRepository;
<%}-%>
import <%=utils.getPackageName(data)%>.model.<%=utils.capitalize(name)%>;
import org.springframework.stereotype.Repository;

@Repository
interface <%=utils.capitalize(name)%>Repository : <%=utils.getRepository(data)%><<%=utils.capitalize(name)%>, <%=utils.repoPrimary(attributes,name)%>> {
}
