const { processData } = require("./processdata.js");

let jsonata = require("jsonata");
module.exports.run = (inputData, lookup, tasks, utils) => {
  //let tasksFile = JSON.parse(fs.readFileSync('tasksFile.json', { encoding: "utf8" }));
  // console.log("from pre processor" + JSON.stringify(inputData));
  let temp = {
    applications:[
      inputData
    ]
  }

  console.log("input data",JSON.stringify(inputData));
  let processeData = processData(temp,utils.getRef);
  console.log("processed data",JSON.stringify(processeData));
  tasks["model"] = processeData.project.entities.filter(i => i.taskId==="model");
  
  // console.log(tasks);
  // console.log("tasks",JSON.stringify(tasks));

  return { tasks, data: processeData };
};
