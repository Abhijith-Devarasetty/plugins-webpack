package <%=utils.getPackageName(data)%>.controller
<%let entity = utils.getEntityName(data)-%>
<%let capEntity = utils.getCapEntityName(data)-%>

import <%=utils.getPackageName(data)%>.model.<%=capEntity%>
import <%=utils.getPackageName(data)%>.service.<%=capEntity%>Service
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;

import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

@RestController
@RequestMapping("/<%=utils.lowerCase(entity)%>")
@Api(value = "<%=utils.lowerCase(entity)%>")
class <%=capEntity%>Controller{

	@Autowired
	lateinit var service: <%=capEntity%>Service;

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@PostMapping("/create")
	@ApiOperation(value = "Post <%=capEntity%> api")
	@ResponseBody
	fun add<%=capEntity%>(@RequestBody <%=entity%>: <%=capEntity%>): <%=capEntity%> {
		return service.add(<%=entity%>);
	}

	@GetMapping("/getall")
	@ApiOperation(value = "Get All <%=capEntity%> api")
	@ResponseBody
	fun getAll<%=capEntity%>(): List<<%=capEntity%>> {
		return service.getAll();
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@DeleteMapping("/delete/{id}")
	@ApiOperation(value = "Delete <%=capEntity%> by ID api")
	fun delete<%=capEntity%>(@PathVariable id: <%=utils.getPrimaryDataType(data)%>): String {
		return service.delete(id);
	}

	@GetMapping("/getbyid/{id}")
	@ApiOperation(value = "Get <%=capEntity%> by Id api")
	@ResponseBody
	fun getById<%=capEntity%>(@PathVariable id: <%=utils.getPrimaryDataType(data)%>): <%=capEntity%> {
		return service.get<%=capEntity%>ById(id);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	@PutMapping("/update/{id}")
	@ApiOperation(value = "Put <%=capEntity%> api")
	@ResponseBody
	fun update<%=capEntity%>(@RequestBody <%=entity%>: <%=capEntity%>,@PathVariable id: <%=utils.getPrimaryDataType(data)%>): <%=capEntity%> {
		return service.update<%=capEntity%>(<%=entity%>, id);
	}

}
