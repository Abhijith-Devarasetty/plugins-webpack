package <%=utils.getPackageName(data)%>.service
<%let entity = utils.getEntityName(data)%>
<%let capEntity = utils.getCapEntityName(data)-%>
import org.springframework.stereotype.Service;

import <%=utils.getPackageName(data)%>.model.<%=capEntity%>
import <%=utils.getPackageName(data)%>.repository.<%=capEntity%>Repository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.client.RestTemplate;


@Service
class <%=capEntity%>Service {

	@Autowired
	lateinit var Repository: <%=capEntity%>Repository;

	fun add(@RequestBody <%=entity%>: <%=capEntity%>): <%=capEntity%> {
		return Repository.save(<%=entity%>);
	}

	fun getAll(): List<<%=capEntity%>> {
		return Repository.findAll();
	}

	fun delete(id: <%=utils.getPrimaryDataType(data)%>): String {
		Repository.delete(id);
		return "record deleted...";
	}

	fun get<%=capEntity%>ById(Id: <%=utils.getPrimaryDataType(data)%>): <%=capEntity%> {
		return Repository.findOne(Id);
	}
	
	fun update<%=capEntity%>(<%=entity%>: <%=capEntity%>,Id: <%=utils.getPrimaryDataType(data)%>):<%=capEntity%>{
		val obj: <%=capEntity%> = Repository.findOne(Id);
    	<%data.project.entities[0].attributes.forEach(e=>{ %>
			obj.<%=e.name%> = <%=entity%>.<%=e.name%>;
		<%}) %>
		return Repository.save(obj);
	}

}
