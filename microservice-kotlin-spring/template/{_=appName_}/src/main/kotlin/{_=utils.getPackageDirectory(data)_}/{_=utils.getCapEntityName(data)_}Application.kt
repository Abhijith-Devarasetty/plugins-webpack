package <%=utils.getPackageName(data)%>

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class <%=appName%>Application

fun main(args: Array<String>) {
    SpringApplication.run(<%=appName%>Application::class.java, *args)
}
