let jsonata = require("jsonata");
var _ = require('lodash');

_.mixin({ pascalcase: _.flow(_.camelCase, _.upperFirst) });

function pascalCase(input) {
  return _.pascalcase(input);
}

function compName(name,flag){
  if(flag === "dash"){
    return _.camelCase(name).replace(/[A-Z]/g, '-$&').toLowerCase();
  }else if(flag === "lower"){
    return _.lowerCase(name);
  }else if (flag === "upper"){
    return _.upperCase(name);
  }
}

function sectionName(name){
  return  pascalCase(name).replace(/[A-Z]/g, ' $&');
}

function getAppName(data){
  return jsonata("**.appConfig.name").evaluate(data);
}

function getComponentName(config) {
  return config.framework.angular.components[0].componentName;
}

function getComponentFileds(componentName, data) {
  return jsonata("**." + componentName + ".fields").evaluate(data);
}

function getEntityName(componentName, data) {
  return jsonata("**." + componentName + ".fields[0].model").evaluate(data);
}

function getEntitySchema(entityName,data) {
  return jsonata("**.entities."+entityName+".attributes").evaluate(data);
}

function getRouting(data){
  // console.log(jsonata("**.projects.components").evaluate(data));
  return jsonata("**.projects.components").evaluate(data);
}

function getId(schema){
  for(let i=0;i<schema.length;i++){
    if(schema[i].primaryKey === "true"){
      return schema[i].name;
    }
  }
}

function getDataType(ele) {
  switch (ele.dataType) {
    case "int":
        return "number";
    case "decimal":
        return "number";
    case "string":
        return "string";
    case "boolean":
        return "boolean";
    case "date":
        return "Date";
    case "time":
        return "Timestamp";
    default:
      return ele.dataType;
  }
}

function getUiElement(material,ele,layout){
  if(material === "material"){
    return getMaterialUiElement(ele,layout);
  }
}


function getMaterialUiElement(ele,layout){
  let grid = 12/layout;
  let options = '';
  let required = '';
  let isDisabled = '';
  if(ele.validators !== undefined && ele.validators.includes('required')){
    required = `<span class="required"> *</span>`;
  }
  if(ele.isDisabled !== undefined && ele.isDisabled === "true"){
    isDisabled = '[readonly]="true"';
  }
  switch(ele.type){
    case 'Input': return `
        <div class="col-md-${grid} element">
          <mat-label id ="${ele.name}">${ele.label}</mat-label>${required}<br />
          <input ${isDisabled} class="input" formControlName="${ele.name}" matInput />
        </div>`;

    case 'Datepicker': return `
        <div class="col-md-${grid} element">
          <mat-label id ="${ele.name}">${ele.label}</mat-label>${required}<br />
            <input class="input" formControlName="${ele.name}" matInput [matDatepicker]="picker">
            <mat-datepicker-toggle matSuffix [for]="picker"></mat-datepicker-toggle>
          <mat-datepicker #picker></mat-datepicker>
        </div>`;
    
    case 'Radio':
      options ='';
      if(ele.options !== undefined){
        ele.options.forEach(o => {
          options = options + `
            <mat-radio-button value="${o}">${o}</mat-radio-button>`;
        });
      }
      return `
        <div class="col-md-${grid} element">
          <mat-label id ="${ele.name}">${ele.label}</mat-label>${required}<br />
          <mat-radio-group formControlName="${ele.name}" aria-label="${ele.label}">`+ options +
          `
          </mat-radio-group>
        </div>`;

    case 'Select':
      options = '';
      if(ele.options !== undefined){
        ele.options.forEach(o => {
          options = options + `
              <mat-option value="${o}">${o}</mat-option>`
        });
      }
      return `
        <div class="col-md-${grid} element">
          <mat-label id ="${ele.name}">${ele.label}</mat-label>${required}<br />
          <mat-select class="select" formControlName="${ele.name}" matNativeControl required>`
          + options +
          `
          </mat-select>
        </div>`;

    case 'Checkbox':
      options = '';
      if(ele.options !== undefined){
        ele.options.forEach(o => {
          options = options + `
              <mat-checkbox formControlName="${o}" class="checkbox-option">${o}</mat-checkbox>&nbsp;&nbsp;&nbsp;`;
        })
      }
      return `
        <div class="col-md-${grid} element">
          <mat-label id ="${ele.name}">${ele.label}</mat-label>${required}<br />
          <section class="checkbox">`
          + options +
          `
          </section>
        </div>`;

    case 'Button':
      let action ='';
      if(ele.option_action !== undefined){
        action = `(click)=${ele.option_action}()`;
      }
      return `
        <div class="col-md-${grid} element">
          <button id ="${ele.name}" mat-raised-button ${action} color="primary">${ele.label}</button><br/>
        </div>
        `;

    case 'ViewText':
      return `
        <div class="col-md-4 element">
          <mat-label id ="${ele.name}"><b>${ele.label}</b></mat-label><br />
          <mat-label>${ele.value}</mat-label><br />
        </div>`;
  }
}

function getActions(tabLayoutData){
  let actions = '';
  tabLayoutData.forEach(t => {
    t.sections.forEach(s => {
      s.elements.forEach(e => {
        if(e.type === "Button"){
          if(e.option_action!==undefined){
            actions = actions + `   ${e.option_action}(){
    // Logic here
  }
`;
          }
        }
      })
    })
  })
  return actions;
}

function getSelectedEle(eleType,tabLayoutData){
  var arr = [];
  tabLayoutData.forEach(t=>{
    let temp = [];
    t.sections.forEach(s=>{
      arr = [...s.elements.filter(e => e.type === eleType && e.option_action !== undefined),...arr];
    });
  })
  // console.log(arr);
  return arr;
}

function getValidators(e){
  if(e.validators === undefined){
    return;
  }
  let str ='';
  e.validators.forEach(v => {
    if(v === 'required'){
      str += ' Validators.required';
    } else if(v.includes('minLength')){
      str += ` Validators.${v}`;
    } else if(v.includes('maxLength')){
      str += ` Validators.${v}`;
    } else if(v === 'email'){
      str += ` Validators.email`;
    }
  });
  str = str.split(" ").join(", ");
  str = str.substring(2,str.lenth);
  str = `,[${str}]`;
  return str;
}

module.exports = {
  getComponentName,
  getComponentFileds,
  getEntityName,
  getEntitySchema,
  getRouting,
  getAppName,
  getId,
  compName,
  getDataType,
  pascalCase,
  getUiElement,
  getActions,
  getSelectedEle,
  getValidators,
  sectionName
};
