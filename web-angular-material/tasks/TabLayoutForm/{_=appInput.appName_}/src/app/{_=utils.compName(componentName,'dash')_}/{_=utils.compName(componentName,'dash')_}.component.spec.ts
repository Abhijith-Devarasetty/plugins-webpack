import { TestBed, async, ComponentFixture } from "@angular/core/testing";
import { CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { By } from "@angular/platform-browser";
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import {MatButtonModule} from '@angular/material/button';
import {MatNativeDateModule} from '@angular/material/core';
import {MatCheckboxModule} from '@angular/material/checkbox';
<%let com = componentName-%>
import { <%=com%>Component } from './<%=utils.compName(com,"dash")%>.component';

describe("<%=com%>Component", () => {
    let component: <%=com%>Component;
    let fixture: ComponentFixture<<%=com%>Component>;
    let compiled: any;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatDatepickerModule,
        MatNativeDateModule,
        MatSelectModule,
        MatRadioModule,
        MatButtonModule,
        FormsModule,
        ReactiveFormsModule,
        MatCheckboxModule
      ],
      declarations: [<%=com%>Component],
      schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(<%=com%>Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
    compiled = fixture.nativeElement;
  })

  it('should create the app',()=>{
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

<%tabLayoutData.forEach(t => {-%>
<%t.sections.forEach(s => {-%>
<%s.elements.forEach(e => {-%>
  it('testing <%=e.label%> element',()=>{
    const ele = fixture.debugElement.query(By.css('#<%=e.name%>')).nativeElement;
    expect(ele.textContent).toEqual("<%=e.label%>");
  });

<%})-%>
<%})-%>
<%})-%>

<%let optionsActions = utils.getSelectedEle("Button",tabLayoutData)-%>
<%optionsActions.forEach(a=>{-%>
  it('testing the <%=a.option_action%> function',()=>{
    spyOn(component,"<%=a.option_action%>");
    let temp = component.<%=a.option_action%>();
    expect(component.<%=a.option_action%>).toHaveBeenCalled();
  });

<%})-%>

});
