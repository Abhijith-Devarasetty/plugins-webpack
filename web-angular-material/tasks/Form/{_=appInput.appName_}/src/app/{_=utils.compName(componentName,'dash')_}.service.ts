import { Injectable, ViewChild } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient,HttpHeaders, HttpParams } from '@angular/common/http';
import { <%=componentName%> } from './interface/<%=utils.compName(componentName,"dash")%>';


@Injectable({
  providedIn: 'root',
})

export class <%=componentName%>Service {

  constructor(private http: HttpClient) {  }

  url = "/api/adminProject";
  element: any;
  action: string;
  tablename: string;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }

  getAll<%=componentName%>(): Observable<<%=componentName%>[]>{
    return (this.http.get<<%=componentName%>[]>(this.url + "/getall"));
  }

  insert<%=componentName%>(obj: <%=componentName%>): Observable<<%=componentName%>> {
    return this.http.post<<%=componentName%>>(this.url + "/add", obj);
  }
 
  update<%=componentName%>(obj: <%=componentName%>,id): Observable<void> {
    return this.http.put<void>(this.url+"/update/"+id,obj)
  }
 
  delete<%=componentName%>(id) {
    return this.http.delete<<%=componentName%>>(this.url+"/delete/"+id, this.httpOptions)
  }

<%let fields = utils.getComponentFileds(componentName,data)-%>
<%fields.forEach(f=>{-%>
<%if(f.option_action!==undefined){-%>
  <%=f.option_action%>(){
    
  }
<%}-%>
<%})-%>

}
