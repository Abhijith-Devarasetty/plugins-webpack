<%let entity = utils.getEntityName(componentName,data)-%>
<%let schema = utils.getEntitySchema(entity,data)-%>
export interface <%=componentName%> {
<%schema.forEach(ele => {-%>
    <%=ele.name%>:<%=ele.dataType%>,
<%})-%>
}
