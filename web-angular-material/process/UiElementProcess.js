const fs = require("fs");

function processUiElementJson(path){
    let jsonData;
    jsonData = JSON.parse(fs.readFileSync(path, 'utf8'));
    // console.log("json data from file",JSON.stringify(jsonData));
    return jsonData;
}

module.exports = { processUiElementJson };