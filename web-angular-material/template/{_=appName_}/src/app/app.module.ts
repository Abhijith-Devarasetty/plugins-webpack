import { MatIconModule } from '@angular/material/icon';
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import {
  MatToolbarModule,
  MatTabsModule,
  MatCardModule,
  MatFormFieldModule,
  MatInputModule,
  MatCheckboxModule
} from '@angular/material';
import { MatMenuModule } from '@angular/material/menu';
import { MatTableModule } from '@angular/material/table';
import {
  OverlayModule,
  CdkOverlayOrigin,
  CdkConnectedOverlay,
} from '@angular/cdk/overlay';
import { MainComponent } from './main/main.component';
import {
  MatPaginatorModule,
  MatDialogModule,
  MatSidenavModule,
} from '@angular/material';
import { MatSortModule } from '@angular/material/sort';
import { MatListModule } from '@angular/material/list';
<%let components = utils.getRouting(data)-%>
<%components.forEach(com => {-%>
<%com = com.componentName-%>
// import { <%=com%>EditComponent } from './<%=utils.compName(com,"dash")%>-edit/<%=utils.compName(com,"dash")%>-edit.component';
import { <%=com%>Component } from './<%=utils.compName(com,"dash")%>/<%=utils.compName(com,"dash")%>.component';
<%})-%>
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {MatRadioModule} from '@angular/material/radio';
import {MatButtonModule} from '@angular/material/button';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
<%components.forEach(com => {-%>
<%com = com.componentName-%>
    <%=com%>Component,
    // <%=com%>EditComponent,
<%})-%>
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatTabsModule,
    MatCardModule,
    MatSnackBarModule,
    MatMenuModule,
    OverlayModule,
    CommonModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatSidenavModule,
    MatListModule,
    MatIconModule,
    HttpClientModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatRadioModule,
    MatButtonModule,
    MatCheckboxModule
  ],
  exports: [MatPaginatorModule],
  entryComponents: [],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
