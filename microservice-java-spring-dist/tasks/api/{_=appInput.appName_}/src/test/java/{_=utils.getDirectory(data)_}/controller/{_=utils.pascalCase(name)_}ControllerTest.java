package <%=utils.getPackageName(data)%>.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.doThrow;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

<%let capEntityName = utils.pascalCase(name)-%>
<%let EntityName = name-%>

import <%=utils.getPackageName(data)%>.model.<%=capEntityName%>;
import <%=utils.getPackageName(data)%>.service.<%=capEntityName%>Service;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers=<%=capEntityName%>Controller.class)
public class <%=capEntityName%>ControllerTest {
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private <%=capEntityName%>Service service;
	
	@InjectMocks
	<%=capEntityName%>Controller controller;

	
	@Test
	public void testAdd<%=capEntityName%>() throws Exception {
		<%=capEntityName%> <%=EntityName%> = new <%=capEntityName%>();
		<%-utils.getAttributesData(attributes,EntityName)%>
		
		String inputInJson = this.mapToJson(<%=EntityName%>);
		
		String URI = "/<%=capEntityName%>/create";
		
		Mockito.when(service.add<%=capEntityName%>(Mockito.any(<%=capEntityName%>.class))).thenReturn(<%=EntityName%>);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post(URI)
				.accept(MediaType.APPLICATION_JSON).content(inputInJson)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		
		String outputInJson = response.getContentAsString();
		
		assertThat(outputInJson).isEqualTo(inputInJson);
		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}
	

	@Test
	public void testGet<%=capEntityName%>ById() throws Exception {
		<%=capEntityName%> <%=EntityName%> = new <%=capEntityName%>();
		<%-utils.getAttributesData(attributes,EntityName)%>
		
		Mockito.when(service.get<%=capEntityName%>ById(1)).thenReturn(Optional.of(<%=EntityName%>));
		
		String URI = "/<%=capEntityName%>/getbyid/1";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				URI).accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		String expectedJson = this.mapToJson(<%=EntityName%>);
		String outputInJson = result.getResponse().getContentAsString();
		assertThat(outputInJson).isEqualTo(expectedJson);
	}
	

	@Test
	public void testGetAll<%=capEntityName%>() throws Exception {
		<%=capEntityName%> <%=EntityName%>1 = new <%=capEntityName%>();
		<%-utils.getAttributesData(attributes,EntityName+"1")%>
		
		<%=capEntityName%> <%=EntityName%>2 = new <%=capEntityName%>();
		<%-utils.getAttributesData(attributes,EntityName+"2")%>
		
		List<<%=capEntityName%>> <%=EntityName%>List = new ArrayList<>();
		<%=EntityName%>List.add(<%=EntityName%>1);
		<%=EntityName%>List.add(<%=EntityName%>2);
		
		Mockito.when(service.getAll<%=capEntityName%>()).thenReturn(<%=EntityName%>List);
		
		String URI = "/<%=capEntityName%>/getall";
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				URI).accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		String expectedJson = this.mapToJson(<%=EntityName%>List);
		String outputInJson = result.getResponse().getContentAsString();
		assertThat(outputInJson).isEqualTo(expectedJson);
	}


	@Test
	public void testDelete<%=capEntityName%>() {
	    doThrow(new NullPointerException()).when(service).delete<%=capEntityName%>ById(2);
	    assertThrows(
	      NullPointerException.class,
	      () -> {
	        controller.delete<%=capEntityName%>ById(2);
	      }
	    );
	}


	@Test
    public void testUpdate<%=capEntityName%>() throws Exception {
		<%=capEntityName%> <%=EntityName%> = new <%=capEntityName%>();
		<%-utils.getAttributesData(attributes,EntityName)%>
		
		String inputInJson = this.mapToJson(<%=EntityName%>);
		
		String URI = "/<%=capEntityName%>/update/1";
		
		Mockito.when(service.update<%=capEntityName%>(Mockito.any(<%=capEntityName%>.class),Mockito.anyInt())).thenReturn(<%=EntityName%>);
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.put(URI)
				.accept(MediaType.APPLICATION_JSON).content(inputInJson)
				.contentType(MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		
		String outputInJson = response.getContentAsString();
		
		assertThat(outputInJson).isEqualTo(inputInJson);
		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}


	private String mapToJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
	
}
